import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import { RadioButton } from 'react-native-paper';
import MobileNumber from './MobileNumber';
import { ScrollView } from 'react-native-gesture-handler';

class Chooselanguage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            select_language: 'hindi',
        }
        this.change_language = this.change_language.bind(this);
    }

    change_language(language) {
        this.setState({ select_language: language });
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, }}>
                <View style={{ flex: 1 }}>
                    <MobileNumber />
                </View>
                <View style={[styles.upperbox, { position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }]} >
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <Text style={styles.heading} >Choose language</Text>
                            <Text style={styles.sub_heading}>seems like you want to change language</Text>
                            <TouchableOpacity
                                onPress={() => {
                                    this.change_language('hindi');
                                }}
                                style={this.state.select_language == 'hindi' ? styles.activebutton : styles.button}>
                                <Text style={this.state.select_language == 'hindi' ? styles.buttonactivecolor : styles.buttoncolor}>Hindi</Text>
                                {
                                    this.state.select_language == 'hindi' ?
                                        <Icon
                                            name={'ios-radio-button-on'}
                                            color='#2566FF'
                                        />
                                        :
                                        <Icon
                                            name={'ios-radio-button-off'}
                                            color='#00063B'
                                        />
                                }
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.change_language('tamil');
                                }}
                                style={this.state.select_language == 'tamil' ? styles.activebutton : styles.button}>
                                <Text style={this.state.select_language == 'tamil' ? styles.buttonactivecolor : styles.buttoncolor}>Tamil</Text>
                                {
                                    this.state.select_language == 'tamil' ?
                                        <Icon
                                            name={'ios-radio-button-on'}
                                            color='#2566FF'
                                        />
                                        :
                                        <Icon
                                            name={'ios-radio-button-off'}
                                            color='#00063B'
                                        />
                                }
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.change_language('kannada');
                                }}
                                style={this.state.select_language == 'kannada' ? styles.activebutton : styles.button}>
                                <Text style={this.state.select_language == 'kannada' ? styles.buttonactivecolor : styles.buttoncolor}>Kannada</Text>
                                {
                                    this.state.select_language == 'kannada' ?
                                        <Icon
                                            name={'ios-radio-button-on'}
                                            color='#2566FF'
                                        />
                                        :
                                        <Icon
                                            name={'ios-radio-button-off'}
                                            color='#00063B'
                                        />
                                }
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.change_language('telugu');
                                }}
                                style={this.state.select_language == 'telugu' ? styles.activebutton : styles.button}>
                                <Text style={this.state.select_language == 'telugu' ? styles.buttonactivecolor : styles.buttoncolor}>Telugu</Text>
                                {
                                    this.state.select_language == 'telugu' ?
                                        <Icon
                                            name={'ios-radio-button-on'}
                                            color='#2566FF'
                                        />
                                        :
                                        <Icon
                                            name={'ios-radio-button-off'}
                                            color='#00063B'
                                        />
                                }
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.navigation.navigate('Intro');
                                }}
                                style={styles.submitbutton}>
                                <Text style={styles.submitbuttontxt}>Save changes</Text>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                </View>
            </SafeAreaView>

        );
    }
}


const styles = StyleSheet.create({
    upperbox: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#00000070",
    },
    heading: {
        color: "#040B4D",
        fontSize: 18,
        fontWeight: "700"
    },
    sub_heading: {
        color: "#00063B",
        fontSize: 18,
        fontWeight: "100",
        marginVertical: 6
    },
    container: {
        padding: 30,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    },
    activebutton: {
        backgroundColor: "#E8EFFF",
        padding: 12,
        marginVertical: 10,
        paddingHorizontal: 13,
        borderRadius: 9,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    button: {
        backgroundColor: "#F1F2F5",
        paddingHorizontal: 13,
        padding: 12,
        marginVertical: 8,
        borderRadius: 9,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    buttoncolor: {
        color: "#00063B"
    },
    buttonactivecolor: {
        color: "#2566FF",

    },
    submitbutton: {
        backgroundColor: "#2566FF",
        paddingHorizontal: 13,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    }

});

export default Chooselanguage;