import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Text, Image, ImageBackground, ScrollView, StyleSheet } from 'react-native';
import ThemeStyle from '../assets/css/ThemeStyle';

class Allowaccess extends Component {

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                <View
                    style={{
                        padding: 35,
                        backgroundColor: '#E7F5FF',
                        borderBottomLeftRadius: 25,
                        alignItems: 'center'
                    }}>
                    <Text style={{
                        color: '#040B4D',
                        fontSize: 18,
                        marginBottom: 8,
                        alignSelf: "flex-start"
                    }}>
                        Know more about us
                </Text>
                    <Text style={{
                        color: '#2566FF',
                        fontSize: 22,
                        alignSelf: "flex-start",
                        fontWeight: "700"
                    }}>
                        Allow access
                </Text>
                </View>

                <ScrollView>
                    <View style={[styles.container]}>
                        <View style={styles.content_block}>
                            <View style={[styles.image_block]}>
                                <Image style={[styles.image_css]} source={require('../assets/images/phonenew/phone_new2x.png')}></Image>
                            </View>
                            <View style={[styles.textblock]}>
                                <Text style={[styles.text_heading, { color: "#20B77E" }]}>Phone Calls permissions</Text>
                                <Text style={[styles.text_subheading]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent posuere magna a ligula convallis, et...</Text>
                            </View>
                        </View>
                        <View style={styles.content_block}>
                            <View style={[styles.image_block, { backgroundColor: "#FFEFE5" }]}>
                                <Image style={{ maxWidth: 28, height: 31 }} source={require('../assets/images/profile/profile3x.png')}></Image>
                            </View>
                            <View style={[styles.textblock]}>
                                <Text style={[styles.text_heading, { color: "#F75F00" }]}>Contact permissions</Text>
                                <Text style={[styles.text_subheading]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent posuere magna a ligula convallis, et...</Text>
                            </View>
                        </View>
                        <View style={styles.content_block}>
                            <View style={[styles.image_block, { backgroundColor: "#FFE5EE" }]}>
                                <Image style={{ maxWidth: 28, height: 25 }} source={require('../assets/images/camera/camera3x.png')}></Image>
                            </View>
                            <View style={[styles.textblock]}>
                                <Text style={[styles.text_heading, { color: "#FF085A" }]}>Camera permissions</Text>
                                <Text style={[styles.text_subheading]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent posuere magna a ligula convallis, et...</Text>
                            </View>
                        </View>
                        <View style={styles.content_block}>
                            <View style={[styles.image_block, { backgroundColor: "#F1EBFF" }]}>
                                <Image style={{ maxWidth: 28, height: 25 }} source={require('../assets/images/folder/folder3x.png')}></Image>
                            </View>
                            <View style={[styles.textblock]}>
                                <Text style={[styles.text_heading, { color: "#7340FB" }]}>Media storage permissions</Text>
                                <Text style={[styles.text_subheading]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent posuere magna a ligula convallis, et...</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate("SelectAccount");
                    }}
                    style={{
                        padding: 25,
                        marginTop: 10,
                        backgroundColor: '#2566FF',
                        borderTopLeftRadius: 15,
                        borderTopRightRadius: 15,
                        alignItems: 'center'
                    }}>
                    <Text style={[ThemeStyle.activebtntext]}>Allow access</Text>
                </TouchableOpacity>

            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    image_block: {
        backgroundColor: "#E8F8F2",
        padding: 15,
        borderRadius: 10,
        flex: 0,
        marginRight: 15,
        marginTop: 8
    },
    image_css: {
        width: 30,
        height: 30
    },
    container: {
        paddingHorizontal: 28
    },
    text_heading: {
        fontSize: 21,
        fontWeight: "700"
    },
    text_subheading: {
        fontSize: 16,
        lineHeight: 22,
        marginTop: 10,
        color: "#00063B"
    },
    textblock: {
        padding: 5,
        flex: 1
    },
    content_block: {
        flex: 1,
        flexDirection: "row",
        alignItems: 'flex-start',
        marginVertical: 18
    }
});
export default Allowaccess;
