
import React, { Component } from 'react';
import {
    SafeAreaView, TouchableOpacity, Dimensions, View, Text, StyleSheet,
    ImageBackground, ScrollView, Alert, Animated, StatusBar
} from 'react-native';
import { Icon } from 'react-native-elements';

const windowHeight = Dimensions.get('window').height - 0;
const minusHeight = 260
const finalHeight = windowHeight - minusHeight;

class TermsAndConditions extends Component {

    constructor(props) {
        super(props);
        this.state = {
            itemPosition: new Animated.Value(finalHeight),
            agree: false,
        }
        this.move_up = this.move_up.bind(this);
        this.move_down = this.move_down.bind(this);
        this.moveToNext = this.moveToNext.bind(this);
    }

    move_up() {
        Animated.timing(this.state.itemPosition, {
            toValue: 0,
            duration: 200
        }).start();
    }

    move_down() {
        Animated.timing(this.state.itemPosition, {
            toValue: finalHeight,
            duration: 100
        }).start(() => {
            setTimeout(this.moveToNext, 1000);
        });
    }

    moveToNext() {
        this.props.navigation.navigate('AppStart');
    }

    render() {
        const showDark = this.props.completePrivacyAnim;
        const down = this.props.sendDownInstruction;
        if (down) {
            this.move_down();
        }

        return (
            <View
                style={{
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                }}>
                <Animated.View
                    style={{
                        flex: 1,
                        transform: [
                            {
                                translateY: this.state.itemPosition
                            }
                        ]
                    }}>
                    <View style={{ height: 120, }}>
                        <View style={[styles.container, { opacity: down ? 0 : 1, }]}>
                            <Text numberOfLines={1} ellipsizeMode={'tail'} style={[styles.heading]} >Hey mate, before getting started</Text>
                            <Text numberOfLines={1} ellipsizeMode={'tail'} style={[styles.sub_heading]}>please agree to our T&C and privacy policy</Text>
                        </View>
                    </View>
                    <View
                        style={{
                            backgroundColor: showDark ? "#F75F00" : "#FEDFCC",
                            flex: 1,
                            borderTopLeftRadius: 20,
                            borderTopRightRadius: 20,
                        }}>

                        <TouchableOpacity
                            activeOpacity={1}
                            onPress={() => {
                                this.move_up();
                            }}
                            style={{
                                flexDirection: "row",
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                paddingHorizontal: 30,
                                height: 70,
                                flex: 0,
                            }}>
                            <View>
                                <Text
                                    style={{
                                        fontWeight: "600",
                                        color: showDark ? "white" : "#F75F00",
                                        fontSize: 16,
                                    }}>
                                    Terms and Conditions
                                </Text>
                            </View>
                            <View>
                                {
                                    this.state.agree ?
                                        <Icon
                                            name='check'
                                            type='font-awesome'
                                            color='white'
                                            size={16}
                                        />
                                        :
                                        <Icon
                                            name='circle'
                                            type='font-awesome'
                                            color='white'
                                            size={16}
                                        />
                                }
                            </View>
                        </TouchableOpacity>

                        <ScrollView
                            contentContainerStyle={{
                                paddingHorizontal: 30,
                            }}>
                            <View style={{ flex: 1, }}>
                                <Text style={{ lineHeight: 22, color: "#00063B", fontSize: 14 }}>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum end.
                                    </Text>
                            </View>
                        </ScrollView>

                        <View
                            style={{
                                flex: 0,
                            }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({
                                        agree: true,
                                    });
                                    this.props.callbackfn();
                                }}
                                style={styles.submitbutton}>
                                <Text style={styles.submitbuttontxt}>Accept terms and condition</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ height: 90 }} />

                    </View>
                </Animated.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#F75F00",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    heading: {
        color: "#040B4D",
        fontSize: 16,
        fontWeight: "bold"
    },
    sub_heading: {
        color: "#00063B",
        fontSize: 14,
        fontWeight: "100",
        marginTop: 6,
    },
    container: {
        paddingHorizontal: 30,
        paddingTop: 25,
        paddingBottom: 60,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        transform: [
            {
                translateY: 20
            }
        ]
    },
});

export default TermsAndConditions;
