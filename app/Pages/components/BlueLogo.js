import React, { Component } from 'react';
import { Image, View } from 'react-native';


class BlueLogo extends Component {

    render() {
        return (
            <View style={{ marginBottom: 20 }}>
                <Image style={{
                    height: 80,
                    width: 80,
                    resizeMode: 'contain',
                }} source={require('../../assets/images/logo-blue/logo-blue.png')} />
            </View>
        );
    }
}

export default BlueLogo;
