import React, { Component } from "react";
import { View, StyleSheet } from "react-native";

class ProfileTabTopBar extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }


    render() {

        let progress = "1%";

        if (this.props.progress == null) {
            progress = "1%";
        } else {
            progress = this.props.progress + "%";
        }


        return (
            <View>
                <View style={[styles.upperBar]}>
                    <View style={[styles.innerBar, { backgroundColor: this.props.barColor, width: progress }]} />
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    upperBar: {
        width: "100%",
        padding: 5,
        position: "relative",
        borderRadius: 10,
    },
    innerBar: {
        position: "absolute",
        padding: 6,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
    }
});

export default ProfileTabTopBar;