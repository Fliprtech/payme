import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image } from "react-native";
import React from 'react';
import { TextInput } from "react-native-paper";
import ThemeStyle from "../../assets/css/ThemeStyle";

class LoanBar extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        var percentage = (this.props.pageNumber / this.props.totalPages) * 100;
        var percentageWidth = percentage + "%";

        return (
            <View>
                <View
                    style={{
                        flexDirection: "row",
                        marginBottom: 10,
                    }}>
                    <Text style={[
                        ThemeStyle.bodyFontRegular,
                        {
                            color: "#00063B",
                            fontSize: 14,
                        }
                    ]}>Page</Text>
                    <Text
                        style={[
                            ThemeStyle.bodyFontBold,
                            {
                                fontWeight: "700",
                                color: "#00063B",
                                fontSize: 14,
                                marginLeft: 4
                            }
                        ]}>
                        {this.props.pageNumber}
                    </Text>
                    <Text
                        style={[
                            ThemeStyle.bodyFontRegular,
                            {
                                color: "#00063B",
                                fontSize: 14,
                                marginLeft: 4
                            }
                        ]}>of</Text>
                    <Text style={[
                        ThemeStyle.bodyFontBold,
                        {
                            fontWeight: "700",
                            color: "#00063B",
                            fontSize: 14,
                            marginLeft: 4
                        }
                    ]}>{this.props.totalPages}</Text>
                </View>
                <View style={styles.progressbar}>
                    <View style={{ width: "100%", backgroundColor: "#FEDFCC", padding: 2, position: "relative" }}>
                        <View style={{ position: "absolute", backgroundColor: "#F75F00", padding: 2, width: percentageWidth }}></View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    progressbar: {
        marginBottom: 20,
        paddingRight: 10
    },
});


export default LoanBar;