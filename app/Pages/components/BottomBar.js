import { Component } from "react";
import {
    View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image,
    Animated, Dimensions, StatusBar, PanResponder, Alert
} from "react-native";
import React from 'react';
import SwipeUpDown from 'react-native-swipe-up-down';
import OpctionsSidemenu from "../Pagers/OptionsSidemenu";

const windowHeight = Dimensions.get('window').height - 0;
const minusHeight = 60;
let finalHeight = windowHeight - minusHeight;


class BottomBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pan: new Animated.ValueXY({ x: 0, y: finalHeight }),
            toValueHeight: 0,
            atTop: false,
        }

        this._animatedValueX = 0;
        this._animatedValueY = finalHeight;

        this._panResponder = PanResponder.create({
            onMoveShouldSetResponderCapture: () => true,
            onMoveShouldSetPanResponderCapture: () => true,
            onPanResponderGrant: (e, gestureState) => {
                this.state.pan.setOffset({ x: this._animatedValueX, y: this._animatedValueY });
                this.state.pan.setValue({ x: 0, y: this._animatedValueY });
                // Alert.alert("Called", this._animatedValueY + "");
            },
            onPanResponderMove: Animated.event([
                null, { dx: this.state.pan.x, dy: this.state.pan.y },
            ]),
            onPanResponderRelease: (e, gestureState) => {

                if (gestureState.dy < -100) {
                    if (!this.state.atTop) {
                        Animated.spring(this.state.pan, {
                            toValue: { x: 0, y: - finalHeight }
                        }).start(() => {
                            this.state.atTop = true;
                        });
                    }
                } else if (gestureState.dy > 100) {
                    Animated.spring(this.state.pan, {
                        toValue: { x: 0, y: finalHeight }
                    }).start(() => {
                        this.state.atTop = false;
                    });
                } else {
                    Animated.spring(this.state.pan, {
                        toValue: 0
                    }).start();
                }

            }
        });
        this.setPanResponder = this.setPanResponder.bind(this);
        this.setPanResponder();
    }

    componentDidMount() {
        // Alert.alert("Called", finalHeight + "")
    }

    setPanResponder() {
        this.state.pan.x.addListener((value) => this._animatedValueX = value.value);
        this.state.pan.y.addListener((value) => this._animatedValueY = value.value);
    }


    render() {
        return (
            <Animated.View
                onLayout={(e) => {
                    let width = e.nativeEvent.layout.width;
                    let height = e.nativeEvent.layout.height;
                    let x = e.nativeEvent.layout.x;
                    let y = e.nativeEvent.layout.y;
                    // Alert.alert("Height ", height + "\n" + windowHeight);
                    // this.setState({
                    //     toValueHeight: height - 60,
                    // });
                    // Alert.alert("Called", (Dimensions.get('window').height) + "\n" + height);
                }}
                style={{
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                    flex: 1,
                    zIndex: 100,
                    transform: [
                        {
                            translateY: this.state.pan.y
                        }
                    ]
                }}>
                <View
                    style={{
                        flex: 1,
                        backgroundColor: '#FFFFFF',
                        borderTopLeftRadius: 15,
                        borderTopRightRadius: 15,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 5,
                            height: 12,
                        },
                        shadowOpacity: 0.7,
                        shadowRadius: 20,
                        elevation: 24,
                    }}>
                    <View
                        {...this._panResponder.panHandlers}
                        style={styles.bottombar}>
                        <View style={styles.greendot}></View>
                        <View style={styles.orangedot}></View>
                        <View style={styles.reddot}></View>
                        <View style={styles.bluedot}></View>
                    </View>
                    <OpctionsSidemenu {...this.props} />
                </View>
            </Animated.View>

        );
    }
}

const styles = StyleSheet.create({
    greendot: {
        padding: 3,
        backgroundColor: "#20b77e",
        paddingHorizontal: 5,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    orangedot: {
        backgroundColor: "#f75f00",
        padding: 3,
        paddingHorizontal: 5

    },
    reddot: {
        backgroundColor: "#ff085a",
        padding: 3,
        paddingHorizontal: 5

    },
    bluedot: {
        backgroundColor: "#7340fb",
        padding: 3,
        paddingHorizontal: 5,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    },
    bottombar: {
        paddingHorizontal: 25,
        paddingTop: 10,
        paddingBottom: 25,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        flexDirection: "row",
        justifyContent: "center",
        height: 60
    },
});

export default BottomBar;