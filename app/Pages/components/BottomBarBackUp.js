import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image } from "react-native";
import React from 'react';
import SwipeUpDown from 'react-native-swipe-up-down';
import OpctionsSidemenu from "../Pagers/OptionsSidemenu";


class BottomBarBackUp extends Component {

    render() {
        const Bar = (
            <View style={{ alignItems: 'stretch', justifyContent: 'flex-start' }}>
                <View
                    style={styles.bottombar}>
                    <View style={styles.greendot}></View>
                    <View style={styles.orangedot}></View>
                    <View style={styles.reddot}></View>
                    <View style={styles.bluedot}></View>
                </View>
            </View>
        );

        return (
            <SwipeUpDown
                itemMini={Bar} // Pass props component when collapsed
                itemFull={<OpctionsSidemenu />} // Pass props component when show full
                onShowMini={() => console.log('mini')}
                onShowFull={() => console.log('full')}
                onMoveDown={() => console.log('down')}
                onMoveUp={() => console.log('up')}
                swipeHeight={60}
                animation="spring"
                disablePressToShow={false} // Press item mini to show full
                style={{
                    padding: 0,
                    margin: 0,
                    // backgroundColor: 'rgba(255,255,255,0.0)',
                    backgroundColor: 'green',
                    borderTopLeftRadius: 0,
                    borderTopRightRadius: 0,
                }} // style for swipe
            />
        );
    }
}

const styles = StyleSheet.create({
    greendot: {
        padding: 3,
        backgroundColor: "#20b77e",
        paddingHorizontal: 5,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    orangedot: {
        backgroundColor: "#f75f00",
        padding: 3,
        paddingHorizontal: 5

    },
    reddot: {
        backgroundColor: "#ff085a",
        padding: 3,
        paddingHorizontal: 5

    },
    bluedot: {
        backgroundColor: "#7340fb",
        padding: 3,
        paddingHorizontal: 5,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    },
    bottombar: {
        padding: 15,
        backgroundColor: 'red',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        flexDirection: "row",
        justifyContent: "center",
        height: 60
    }
});

export default BottomBarBackUp;