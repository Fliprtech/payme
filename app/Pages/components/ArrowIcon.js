
import React, { Component } from 'react';
import { Image, } from 'react-native';

class ArrowIcon extends Component {

    state = {
        text: '',
    };

    render() {
        return (
            <Image source={require('../../assets/images/arrow_simple_1/arrow_simple_1.png')} />
        );
    }
}


export default ArrowIcon;
