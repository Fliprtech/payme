import { Component } from "react";
import {
    View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image,
    PanResponder,
    Animated,
} from "react-native";
import React from 'react';
import { TextInput } from "react-native-paper";
import ThemeStyle from "../../assets/css/ThemeStyle";

class EmiCalculatorBar extends Component {

    constructor(props) {
        super(props);
        const total_width = 26;
        this.state = {
            circle_start: 10,
            circle_position: 0,
            total_width: total_width,
            one_percentage: (100 / total_width),
            touch_position: 0,
            touch_start_position: 0,
            barWidth: 0,
            selectedAmount: 0,
            rightGap: 20,
            circle_position_percent: 0,
            repaymentAmount: 0,
            processingFee: 0,
            days: 14,
            repaymentDate: '',
        }

        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (e, gesture) => true,
            onPanResponderMove: (evt, gestureState) => {
                this.setState({
                    circle_start: 0,
                    touch_position: this.state.touch_start_position + gestureState.dx
                });
            },
            onPanResponderRelease: (e, gestureState) => {

                if (this.state.touch_position >= this.state.barWidth) {
                    this.setState({
                        touch_position: this.state.barWidth - this.state.rightGap,
                        touch_start_position: this.state.barWidth - this.state.rightGap,
                    });
                } else if (this.state.touch_position < 0) {
                    this.setState({
                        touch_position: 0,
                        touch_start_position: 0,
                    });
                } else {
                    this.setState({
                        touch_start_position: this.state.touch_position
                    });
                }
            }
        });

        this.setRepaymentDate = this.setRepaymentDate.bind(this);
        this.setRepaymentDate();
    }

    calculateRepayment() {
        // round((sanction_amount * ((roi * (1 + roi) ** tenure_months) / ((1 + roi) ** tenure_months - 1)))) * tenure_months
        let roi = ((30 * 0.1) / 100).toFixed(2);
        let tenure_months = this.state.days;
        let repaymentAnount = ((this.state.selectedAmount * ((roi * (1 + roi) * tenure_months) / ((1 + roi) * tenure_months - 1))) * tenure_months).toFixed(2);
        this.state.repaymentAmount = repaymentAnount;
    }

    claculateProcessingFee() {
        let processing_fee = this.state.selectedAmount * 10 / 100;
        this.state.processingFee = (processing_fee + processing_fee * 18 / 100).toFixed(2);
    }

    setRepaymentDate() {
        let repaymentDate = new Date();
        let newrepaymentDate = new Date(repaymentDate.getTime() + (86400000 * this.state.days));
        let repaymentDateFormate = newrepaymentDate.getDate() + "-" + (newrepaymentDate.getMonth() + 1) + "-" + newrepaymentDate.getFullYear();
        this.state.repaymentDate = repaymentDateFormate;
    }

    render() {

        var dots = [];
        var upperdots = [];

        // calculate circle position        
        if (this.state.barWidth > 0 && this.state.circle_start == 0) {
            if (this.state.selectedAmount > this.props.maxAmount) {
                this.state.circle_position = this.state.total_width;
                this.state.circle_position_percent = (this.state.circle_position * this.state.one_percentage) + "%";
                this.state.selectedAmount = this.props.maxAmount;
            } else {
                this.state.circle_position = this.state.touch_position * this.state.total_width / (this.state.barWidth - this.state.rightGap);
                this.state.circle_position_percent = (this.state.circle_position * this.state.one_percentage) + "%";
                // Calculate SelectedAmount
                this.state.selectedAmount = this.state.circle_position * (this.props.maxAmount / this.state.total_width);
            }
        }

        // Calculate Repayment
        this.calculateRepayment();

        // Calculate Processing Fee
        this.claculateProcessingFee();

        for (let i = 1; i <= this.state.total_width; i++) {
            var barcolor = "#7340FB";
            if (i > this.state.circle_position) {
                barcolor = "white";
            }
            if (i % 3 == 0) {
                dots.push(
                    <View style={[styles.upperborderdot, { backgroundColor: barcolor }]} key={i}></View>
                )
            } else {
                dots.push(
                    <View style={[styles.borderdot, { backgroundColor: barcolor }]} key={i}></View>
                )
            }
        }

        return (
            <View>
                <View style={{ flexDirection: "row", alignItems: "center", padding: 10 }}>
                    <Text>Amount Selected:</Text>
                    <Text style={{ fontSize: 22, marginHorizontal: 15, fontWeight: "bold", color: "#7340FB" }}>₹ {this.state.selectedAmount.toFixed(2)}</Text>
                </View>
                <View
                    style={{ paddingVertical: 15, paddingRight: 25, paddingLeft: 10, backgroundColor: '#F1EBFF' }}
                    {...this.panResponder.panHandlers}>
                    <View
                        onLayout={(event) => {
                            var { x, y, width, height } = event.nativeEvent.layout;
                            if (this.state.circle_start > 0) {
                                var positionPercent = (this.state.circle_start * this.state.one_percentage) + "%";
                                var touchPosition = (this.state.circle_start * (width - this.state.rightGap)) / this.state.total_width;
                                this.setState({
                                    barWidth: width,
                                    circle_position: this.state.circle_start,
                                    circle_position_percent: positionPercent,
                                    selectedAmount: this.state.circle_start * (this.props.maxAmount / this.state.total_width),
                                    touch_position: touchPosition,
                                    touch_start_position: touchPosition,
                                });
                            } else {
                                this.state.barWidth = width;
                            }
                        }}
                        style={{ position: "relative" }}>
                        <View style={{ flexDirection: "row", alignItems: 'center', justifyContent: 'space-between' }}>
                            {dots}
                        </View>
                        <Animated.View
                            style={{
                                position: "absolute", top: -8,
                                zIndex: 1000,
                                left: this.state.circle_position_percent,
                            }}>
                            <View style={[styles.barselector, {}]}>
                                <View style={{ padding: 1, height: 8, backgroundColor: 'white' }} />
                            </View>
                        </Animated.View>
                    </View>
                </View>

                <View style={{ flexDirection: "row", justifyContent: "space-between", padding: 10 }}>
                    <Text style={[ThemeStyle.headingFontMontserratBold, { fontSize: 12, color: "#00063B", }]}>₹ 0</Text>
                    <Text style={[ThemeStyle.headingFontMontserratBold, { fontSize: 12, color: "#00063B", }]}>₹ {this.props.maxAmount}</Text>
                </View>

                <View style={styles.amountdetailsblock}>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <View style={styles.txtdetailblock}>
                            <Text style={[ThemeStyle.headingFontMontserratBold, styles.amoouttxth]}>₹ {this.state.repaymentAmount}</Text>
                            <Text style={[ThemeStyle.bodyFontRegular, styles.amounttxtsh]}>Repayment</Text>
                        </View>
                        <View style={styles.txtdetailblock}>
                            <Text style={[ThemeStyle.headingFontMontserratBold, styles.amoouttxth]}>0.1%</Text>
                            <Text style={[ThemeStyle.bodyFontRegular, styles.amounttxtsh]}>Interest (per day)</Text>
                        </View>
                        <View style={styles.txtdetailblock}>
                            <Text style={[ThemeStyle.headingFontMontserratBold, styles.amoouttxth]}>₹ {this.state.processingFee}</Text>
                            <Text style={[ThemeStyle.bodyFontRegular, styles.amounttxtsh]}>Processing fee</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row", }} >
                        <View style={styles.txtdetailblock}>
                            <Text style={[ThemeStyle.headingFontMontserratBold, styles.amoouttxth]}>{this.state.repaymentDate}</Text>
                            <Text style={[ThemeStyle.bodyFontRegular, styles.amounttxtsh]}>Repayment date</Text>
                        </View>
                        <View style={[styles.txtdetailblock, { marginLeft: 25 }]}>
                            <Text style={[ThemeStyle.headingFontMontserratBold, styles.amoouttxth]}>{this.state.days} Days</Text>
                            <Text style={[ThemeStyle.bodyFontRegular, styles.amounttxtsh]}>Tenure Days</Text>
                        </View>
                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    borderdot: { backgroundColor: "#7340FB", width: 2, height: 4, },
    upperborderdot: { backgroundColor: "#7340FB", width: 2, height: 10, },
    barselector: {
        flex: 1, alignItems: 'center', justifyContent: 'center',
        width: 25, height: 25, backgroundColor: "#7340FB", borderRadius: 25,
    },
    amountdetailsblock: {
        borderTopWidth: 1,
        borderColor: "#fff",
        padding: 10
    },
    amoouttxth: { fontSize: 14, color: "#7340FB" },
    amounttxtsh: {
        fontSize: 12,
        marginTop: 2,
        color: "#00063B",
    },
    txtdetailblock: {
        paddingVertical: 8
    },
});

export default EmiCalculatorBar;