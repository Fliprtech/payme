import React, { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, Alert } from "react-native";

class PagerTopBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            percentage: 0,
        }
    }

    render() {

        if (this.props.percentage === NaN || this.props.percentage == undefined) {
            this.state.percentage = 0;
        } else {
            this.state.percentage = this.props.percentage;
        }

        const percentageWidth = this.state.percentage.toString() + "%";

        return (
            <View style={styles.progressbar}>
                <View style={{ width: "100%", backgroundColor: "#DDF0FF", padding: 2, position: "relative" }}>
                    <View style={{ position: "absolute", backgroundColor: "#2566FF", padding: 2, width: percentageWidth }}></View>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", position: 'absolute', left: 0, right: 0, top: -5 }}>
                        <View><Text> </Text></View>
                        <View><View style={this.state.percentage > 20 ? styles.bardotActive : styles.bardot} /></View>
                        <View><View style={this.state.percentage > 40 ? styles.bardotActive : styles.bardot} /></View>
                        <View><View style={this.state.percentage > 60 ? styles.bardotActive : styles.bardot} /></View>
                        <View><View style={this.state.percentage > 80 ? styles.bardotActive : styles.bardot} /></View>
                        <View><View style={this.state.percentage >= 100 ? styles.bardotActive : styles.bardot} /></View>
                    </View>
                </View>
            </View>
        );
    }
}

export default PagerTopBar;

const styles = StyleSheet.create({
    progressbar: {
        paddingVertical: 30,
        paddingHorizontal: 10,
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    bardotActive: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#2566FF"
    }
});