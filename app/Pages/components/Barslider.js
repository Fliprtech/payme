import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image } from "react-native";
import React from 'react';
import { TextInput } from "react-native-paper";

class Barslider extends Component {
    render() {

        var dots = [];
        var upperdots = [];

        for (let i = 0; i < 25; i++) {
            dots.push(
                <View style={styles.borderdot} key={i}></View>
            )
        }
        for (let i = 0; i < 7; i++) {
            upperdots.push(
                <View style={styles.upperborderdot} key={i}></View>
            )
        }

        return (
            <View>
                <View style={{ position: "relative" }}>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        {dots}
                    </View>
                    <View style={{ position: "absolute", left: 0, right: 0, top: -2 }}>
                        <View style={{ flexDirection: "row", justifyContent: "space-between" }} >
                            {upperdots}
                        </View>
                    </View>
                    <View style={styles.barselector}></View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    borderdot: { backgroundColor: "black", width: 2, height: 4, },
    upperborderdot: { backgroundColor: "black", width: 2, height: 8, },
    barselector: { width: 20, height: 20, backgroundColor: "#2664FF", borderRadius: 25, position: "absolute", top: -8, left: "35%" }
});

export default Barslider;