import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Text, StyleSheet, ImageBackground, ScrollView, Alert, StatusBar } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Intro4 from './IntroScreens/Intro4';
import TermsAndConditions from './Screens/TermsAndConditions';
import PrivacyPolicy from './Screens/PrivacyPolicy';


class GettingStarted extends Component {

    constructor(props) {
        super(props);
        this.state = {
            completePrivacyAnim: false,
            startPrivacyAnim: false,
            sendDownInstruction: false,
        }
        this.agreeTermAndCondition = this.agreeTermAndCondition.bind(this);
        this.privacyAnimationCallback = this.privacyAnimationCallback.bind(this);
        this.privacyButtonClick = this.privacyButtonClick.bind(this);
    }

    agreeTermAndCondition() {
        this.setState({
            startPrivacyAnim: true,
        });
        // Alert.alert("Agree", "Agree Button Click");
    }

    privacyAnimationCallback() {
        this.setState({
            completePrivacyAnim: true,
            startPrivacyAnim: false,
        });
    }

    privacyButtonClick() {
        this.setState({
            sendDownInstruction: true,
        })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, }}>
                <StatusBar hidden />
                <Intro4 />
                <View
                    style={{
                        position: 'absolute',
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        backgroundColor: '#000000a1'
                    }}>
                    <TermsAndConditions
                        {...this.props}
                        completePrivacyAnim={this.state.completePrivacyAnim}
                        sendDownInstruction={this.state.sendDownInstruction}
                        callbackfn={this.agreeTermAndCondition} />
                    <PrivacyPolicy
                        startPrivacyAnim={this.state.startPrivacyAnim}
                        privacyButtonClick={this.privacyButtonClick}
                        privacyAnimationCallback={this.privacyAnimationCallback} />
                </View>
            </SafeAreaView>
        );
    }
}

export default GettingStarted;
