import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { RadioButton } from 'react-native-paper';
import PurposeOfLoan from './PurposeOfLoan';
import ThemeStyle from '../../assets/css/ThemeStyle';
import OtpInputs from "react-native-otp-inputs";


class EnterOtp extends Component {

    render() {
        return (
            <SafeAreaView style={{ flex: 1, }}>
                <View style={{ flex: 1 }}>
                    <PurposeOfLoan />
                </View>
                <View style={[ThemeStyle.overFlowBox, { position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }]} >
                    <View style={[ThemeStyle.overFlowContainer]} >
                        <Text style={[ThemeStyle.overlayHeading, ThemeStyle.headingFontMontserratBold]} >Enter OTP sent to your mobile</Text>
                        <Text style={[ThemeStyle.overlaySubHeading, ThemeStyle.bodyFontRegular]}>please don not share this with others.</Text>

                        <View style={{ height: 100 }}>
                            <OtpInputs
                                inputStyles={{
                                    textAlign: 'center',
                                }}
                                inputContainerStyles={{
                                    backgroundColor: '#F1F2F5',
                                    flex: 1,
                                    marginHorizontal: 5,
                                    borderRadius: 5,
                                }}
                                handleChange={code => console.log(code)}
                                numberOfInputs={6} />
                        </View>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate("AccountForDisbursal")}
                            style={[ThemeStyle.blueBtn]}>
                            <Text style={[ThemeStyle.activebtntext]}>Verify OTP</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

export default EnterOtp;