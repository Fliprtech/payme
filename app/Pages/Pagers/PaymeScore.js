import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image } from "react-native";
import React from 'react';
import { Avatar } from 'react-native-elements';
import BottomBar from "../components/BottomBar";
import ThemeStyle from '../../assets/css/ThemeStyle';
import BlueLogo from "../components/BlueLogo";



class PaymeScore extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                <View style={styles.container}>

                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-end' }}>
                        <View style={styles.inner_block}>
                            <BlueLogo />
                            <View style={styles.fullamountblock}>
                                <View style={styles.approveamount}>
                                    <Avatar size="small" icon={{ name: 'square', type: 'font-awesome' }}
                                        overlayContainerStyle={{ backgroundColor: '#FF085A', borderRadius: 5 }} />
                                    <Text
                                        style={[
                                            ThemeStyle.headingFontMontserratBold,
                                            { color: "#FF085A", fontSize: 20, marginHorizontal: 15, }
                                        ]}>
                                        PayMe score
                                    </Text>
                                </View>
                                <View style={{ flexDirection: "row", paddingVertical: 20 }}>
                                    <View style={{ flex: 0 }}>
                                        <Image style={{ height: 160, width: 115 }} source={require('../../assets/images/gold.png')}></Image>
                                    </View>
                                    <View style={{ flex: 1, justifyContent: "center", paddingLeft: 10 }}>
                                        <Text style={{ fontSize: 14 }}>Level</Text>
                                        <Text style={{ fontSize: 21, fontWeight: "bold", color: "#FF085A", marginVertical: 5 }}>Gold member</Text>
                                        <Text style={{ fontSize: 20, fontWeight: "bold", marginBottom: 5 }}>Benefits:</Text>
                                        <Text style={{ fontSize: 12 }}>- High credit limit</Text>
                                        <Text style={{ fontSize: 12, marginVertical: 5 }}>- Lower interest rate than credit cards</Text>
                                        <Text style={{ fontSize: 12 }}>- Quick Disbursal</Text>
                                    </View>
                                </View>
                            </View>

                        </View>
                    </View>
                    <View style={{ alignItems: "center", marginVertical: 20, }}>
                        <Image source={require('../../assets/images/colorbar.png')} />
                    </View>
                </View>
                <View style={{ height: 60 }} />
                <BottomBar {...this.props} />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        flex: 1
    },

    inner_block: {

    },
    img_block: {
        marginVertical: 30
    },

    greendot: {
        padding: 3,
        backgroundColor: "#20b77e",
        paddingHorizontal: 5,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    orangedot: {
        backgroundColor: "#f75f00",
        padding: 3,
        paddingHorizontal: 5

    },
    reddot: {
        backgroundColor: "#ff085a",
        padding: 3,
        paddingHorizontal: 5

    },
    bluedot: {
        backgroundColor: "#7340fb",
        padding: 3,
        paddingHorizontal: 5,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    },
    bottombar: {
        padding: 15,
        marginTop: 10,
        backgroundColor: 'white',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        flexDirection: "row",
        justifyContent: "center"
    },
    fullamountblock: {
        backgroundColor: "#FFE5EE",
        paddingVertical: 10,
        borderRadius: 15
    },
    approveamount: {
        borderBottomWidth: 1,
        borderColor: "#fff",
        padding: 10,
        flexDirection: "row",
        alignItems: "center"
    },


});

export default PaymeScore;