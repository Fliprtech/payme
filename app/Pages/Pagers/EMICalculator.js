import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image } from "react-native";
import React from 'react';
import { Icon } from "react-native-vector-icons/Ionicons";
import { Input, Avatar } from 'react-native-elements';
import BottomBar from "../components/BottomBar";
import ThemeStyle from '../../assets/css/ThemeStyle';
import EmiCalculatorBar from "../components/EmiCalculatorBar";
import BlueLogo from "../components/BlueLogo";



class EMICalculator extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }



    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                <View style={styles.container}>

                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-end' }}>
                        <View style={styles.inner_block}>
                            <BlueLogo />
                            <View style={styles.fullamountblock}>
                                <View style={styles.approveamount}>
                                    <Avatar size="small" icon={{ name: 'square', type: 'font-awesome' }}
                                        overlayContainerStyle={{ backgroundColor: '#7340FB', borderRadius: 5 }} />
                                    <Text style={[
                                        ThemeStyle.headingFontMontserratBold,
                                        { color: "#7340FB", fontSize: 20, marginHorizontal: 15, }
                                    ]}>EMI Calculator</Text>
                                </View>

                                <View style={{}}>
                                    <EmiCalculatorBar maxAmount={4500} />
                                </View>

                            </View>

                        </View>
                    </View>
                    <View style={{ alignItems: "center", marginVertical: 20, }}>
                        <Image source={require('../../assets/images/colorbar.png')} />
                    </View>
                </View>
                <View style={{ height: 60 }} />
                <BottomBar {...this.props} />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        flex: 1
    },
    img_block: {
        marginVertical: 30
    },
    greendot: {
        padding: 3,
        backgroundColor: "#20b77e",
        paddingHorizontal: 5,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    orangedot: {
        backgroundColor: "#f75f00",
        padding: 3,
        paddingHorizontal: 5

    },
    reddot: {
        backgroundColor: "#ff085a",
        padding: 3,
        paddingHorizontal: 5,
    },
    bluedot: {
        backgroundColor: "#7340fb",
        padding: 3,
        paddingHorizontal: 5,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    },
    bottombar: {
        padding: 15,
        marginTop: 10,
        backgroundColor: 'white',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        flexDirection: "row",
        justifyContent: "center"
    },
    fullamountblock: {
        backgroundColor: "#F1EBFF",
        padding: 10,
        borderRadius: 15
    },
    approveamount: {
        borderBottomWidth: 1,
        borderColor: "#fff",
        padding: 10,
        flexDirection: "row",
        alignItems: "center"
    },

});

export default EMICalculator;