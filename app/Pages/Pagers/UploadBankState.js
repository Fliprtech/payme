import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Input, Overlay, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { RadioButton } from 'react-native-paper';
import SelectBank from './SelectBank';
import ThemeStyle from '../../assets/css/ThemeStyle';
import ImagePicker from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';


class UploadBankState extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showChooser: false,
            first_statement_add: false,
            second_statement_add: false,
            third_statement_add: false,
            option_selected: 0,
            first_statement: null,
            second_statement: null,
            third_statement: null,
            all_upload: false,
        }
        this.uploadStatement = this.uploadStatement.bind(this);
        this.uploadStateViaCamera = this.uploadStateViaCamera.bind(this);
    }

    setFileAndState() {

        let option_selected = this.state.option_selected;


        if (option_selected == 1) {
            this.setState({
                showChooser: false,
                first_statement_add: true,
                option_selected: 0,
            }, () => this.changeAllUpload())
        } else if (option_selected == 2) {
            this.setState({
                showChooser: false,
                second_statement_add: true,
                option_selected: 0,
            }, () => this.changeAllUpload())
        } else if (option_selected == 3) {
            this.setState({
                showChooser: false,
                third_statement_add: true,
                option_selected: 0,
            }, () => this.changeAllUpload())
        }
    }

    changeAllUpload() {
        let alluploaded = false;
        if (!this.state.first_statement_add) {
            alluploaded = false;
        } else if (!this.state.second_statement_add) {
            alluploaded = false;
        } else if (!this.state.third_statement_add) {
            alluploaded = false;
        } else {
            alluploaded = true;
        }
        this.setState({
            all_upload: alluploaded,
        })
    }

    async uploadStatement() {

        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
            });
            console.log('res : ' + JSON.stringify(res));
            console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size
            );
            this.setFileAndState();

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                this.setState({
                    showChooser: false,
                });
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }

    }

    uploadStateViaCamera() {

        const options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.launchCamera(options, (response) => {
            if (response.didCancel) {
                this.setState({ showChooser: false });
                console.log('User cancelled image picker');
            } else if (response.error) {
                this.setState({ showChooser: false });
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                this.setState({ showChooser: false });
                console.log('User tapped custom button: ', response.customButton);
            } else {
                this.setFileAndState();
                const source = { uri: response.uri };
            }
        });
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <View style={{ flex: 1 }}>
                    <SelectBank />
                </View>

                <View style={[styles.upperbox, { position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }]} >
                    <View>
                        <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                            <View style={styles.container} >
                                <Text style={[ThemeStyle.overlayHeading, ThemeStyle.headingFontMontserratBold]} >Upload bank statement</Text>
                                <Text style={[ThemeStyle.overlaySubHeading, ThemeStyle.bodyFontRegular]}>upload last 3 months statements</Text>

                                <View style={[ThemeStyle.uploadstatecontainer]}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                showChooser: true,
                                                option_selected: 1,
                                            });
                                        }}
                                        style={[
                                            ThemeStyle.uploadstatesingleblock,
                                            { backgroundColor: this.state.first_statement_add ? "#20B77E" : "#E8F8F2" }
                                        ]}>
                                        {
                                            this.state.first_statement_add
                                                ?
                                                <Image style={[ThemeStyle.uploadStateImage]} source={require('../../assets/images/g_verified/g_verified.png')} />
                                                :
                                                <Image style={[ThemeStyle.uploadStateImage]} source={require('../../assets/cloud_up_green/cloud_up_green2x.png')} />
                                        }

                                        <Text style={[
                                            ThemeStyle.uploadStateText,
                                            { color: this.state.first_statement_add ? "white" : '#20B77E' }
                                        ]}>
                                            Month 1st
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                showChooser: true,
                                                option_selected: 2,
                                            });
                                        }}
                                        style={[
                                            ThemeStyle.uploadstatesingleblock,
                                            { backgroundColor: this.state.second_statement_add ? '#F75F00' : '#FFEFE5' }
                                        ]}>

                                        {
                                            this.state.second_statement_add
                                                ?
                                                <Image style={[ThemeStyle.uploadStateImage]} source={require('../../assets/images/o_verified/o_verified.png')} />
                                                :
                                                <Image style={[ThemeStyle.uploadStateImage]} source={require('../../assets/images/cloud_up_orange/cloud_up_orange2x.png')} />
                                        }

                                        <Text style={[
                                            ThemeStyle.uploadStateText,
                                            { color: this.state.second_statement_add ? 'white' : '#F75F00' }
                                        ]}>
                                            Month 2nd
                                            </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                showChooser: true,
                                                option_selected: 3,
                                            });
                                        }}
                                        style={[
                                            ThemeStyle.uploadstatesingleblock,
                                            { backgroundColor: this.state.third_statement_add ? "#FF085A" : '#FFE5EE' }
                                        ]}>
                                        {
                                            this.state.third_statement_add
                                                ?
                                                <Image style={[ThemeStyle.uploadStateImage]} source={require('../../assets/images/p_verified/p_verified.png')} />
                                                :
                                                <Image style={[ThemeStyle.uploadStateImage]} source={require('../../assets/images/cloud_up_pink/cloud_up_pink2x.png')} />
                                        }

                                        <Text style={[
                                            ThemeStyle.uploadStateText,
                                            { color: this.state.third_statement_add ? "white" : '#FF085A' }
                                        ]}>
                                            Month 3rd
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.input_block}>
                                    <Input
                                        inputContainerStyle={{
                                            borderBottomWidth: 0,
                                            backgroundColor: "#F1F2F5",
                                            borderRadius: 10
                                        }}
                                        containerStyle={{
                                            padding: 0,
                                            width: '100%',
                                            alignItems: 'stretch',
                                            paddingHorizontal: 0,
                                        }}
                                        inputStyle={{
                                            fontSize: 13,
                                            fontWeight: "500",
                                            color: "black",
                                            marginLeft: 14,
                                            paddingVertical: 15
                                        }}
                                        secureTextEntry={true}
                                        placeholderTextColor="#c5c6d3"
                                        placeholder='Enter your password'
                                        leftIcon={
                                            <Image
                                                style={{
                                                    width: 16,
                                                    height: 16,
                                                }} source={require('../../assets/images/lock_1/lock2.png')} />
                                        } />
                                </View>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.state.all_upload ?
                                            // this.props.navigation.navigate("EnterDocPassword")
                                            this.props.navigation.navigate("EnterPassword")
                                            :
                                            Alert.alert('OOPS!', "Please Upload Statement")
                                    }}
                                    style={styles.submitbutton}>
                                    <Text style={[ThemeStyle.activebtntext]}>{this.state.all_upload ? "Upload all statements" : "Select and upload"}</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <Overlay
                    // width={'auto'}
                    height={'auto'}
                    isVisible={this.state.showChooser}>
                    <View>
                        <Text
                            style={{
                                fontSize: 18, fontWeight: '700',
                                paddingHorizontal: 10,
                                paddingVertical: 15
                            }}>
                            Choose Option
                    </Text>
                        <View>
                            <TouchableOpacity
                                onPress={() => {
                                    this.uploadStateViaCamera();
                                }}
                                style={{
                                    paddingHorizontal: 20,
                                    paddingVertical: 15,
                                    fontSize: 16,
                                    backgroundColor: '#f0f0f0',
                                    marginVertical: 10,
                                    marginHorizontal: 10,
                                }}>
                                <Text>
                                    Camera
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.uploadStatement();
                                }}
                                style={{
                                    paddingHorizontal: 20,
                                    paddingVertical: 15,
                                    fontSize: 16,
                                    backgroundColor: '#f0f0f0',
                                    marginVertical: 10,
                                    marginHorizontal: 10,
                                }}>
                                <Text>
                                    Storage
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ alignItems: 'center', paddingHorizontal: 10, paddingVertical: 20 }}>
                            <Button
                                title="Close"
                                type="solid"
                                raised={true}
                                containerStyle={{
                                }}
                                buttonStyle={{
                                    paddingHorizontal: 30,
                                }}
                                style={{
                                    paddingHorizontal: 20,
                                }}
                                onPress={() => {
                                    this.setState({
                                        showChooser: false,
                                        option_selected: 0,
                                    });
                                }} />
                        </View>
                    </View>
                </Overlay>

            </SafeAreaView>

        );
    }
}


const styles = StyleSheet.create({
    upperbox: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#00000070",
    },
    container: {
        padding: 30,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    activebutton: {
        backgroundColor: "#E8EFFF",
        padding: 12,
        marginVertical: 10,
        paddingHorizontal: 13,
        borderRadius: 9,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    button: {
        backgroundColor: "#F1F2F5",
        paddingHorizontal: 13,
        padding: 12,
        marginVertical: 8,
        borderRadius: 9,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    buttoncolor: {
        color: "#00063B"
    },
    buttonactivecolor: {
        color: "#2566FF",

    },
    submitbutton: {
        backgroundColor: "#2566FF",
        paddingHorizontal: 13,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
    },
    input_block: {
        marginVertical: 8
    }

});

export default UploadBankState;