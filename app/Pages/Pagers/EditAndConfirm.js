
import React, { Component } from 'react';
import {
    SafeAreaView, TouchableOpacity, View, Image, Text, TextInput,
    StyleSheet, ImageBackground, ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';
import AsyncStorage from '@react-native-community/async-storage';

class EditAndConfirm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            accountname: '',
            accountnumber: '',
            bankname: '',
            ifsccode: '',
            bankDetail: null,
        }
    }


    componentDidMount() {
        let bankDetail = this.props.navigation.state.params.bankdetail;
        this.setState({
            accountname: bankDetail.accountHolderName,
            accountnumber: bankDetail.accountNumber,
            bankname: bankDetail.bankName,
            ifsccode: bankDetail.accountIFSC,
            bankDetail: bankDetail,
        });
    }

    async editConfirm() {
        this.state.bankDetail.accountHolderName = this.state.accountname;
        this.state.bankDetail.accountNumber = this.state.accountnumber;
        this.state.bankDetail.bankName = this.state.bankname;
        this.state.bankDetail.accountIFSC = this.state.ifsccode;
        await AsyncStorage.setItem("accountDetail", JSON.stringify(this.state.bankDetail));
        this.props.navigation.navigate("LoanDirbursed", { bankdetail: this.state.bankDetail });
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={styles.container}>
                    <PagerTopBar percentage={98} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <BlueLogo />
                                <View
                                    style={{
                                        marginBottom: 20,
                                        marginRight: 20,
                                        backgroundColor: '#EEF8FF',
                                        padding: 20,
                                        borderRadius: 15,
                                    }}>
                                    <View>
                                        <Text style={[ThemeStyle.inputLavelText]}>Name on account</Text>
                                        <View style={{
                                            backgroundColor: '#ffffff',
                                            borderRadius: 10,
                                            padding: 4,
                                            marginBottom: 20

                                        }}>
                                            <View>
                                                <TextInput style={{
                                                    color: 'black',
                                                    fontSize: 14,
                                                    fontWeight: 'bold'
                                                }}
                                                    placeholder="Enter name here."
                                                    value={this.state.accountname}
                                                    onChangeText={accountname => this.setState({ accountname })} /></View>
                                        </View>
                                    </View>
                                    <View>
                                        <Text style={[ThemeStyle.inputLavelText]}>Account number</Text>
                                        <View style={{
                                            backgroundColor: '#ffffff',
                                            borderRadius: 10,
                                            padding: 4,
                                            marginBottom: 20

                                        }}>
                                            <View>
                                                <TextInput style={{
                                                    color: 'black',
                                                    fontSize: 14,
                                                    fontWeight: 'bold'
                                                }}
                                                    placeholder="Enter account number here."
                                                    value={this.state.accountnumber}
                                                    onChangeText={accountnumber => this.setState({ accountnumber })} /></View>
                                        </View>
                                    </View>

                                    <View style={{
                                        flexDirection: "row",
                                        justifyContent: 'space-between',
                                    }}>

                                        <View style={{
                                            flex: 1,
                                            paddingRight: 5
                                        }}>
                                            <Text style={[ThemeStyle.inputLavelText]}>Bank Name</Text>
                                            <View style={{
                                                backgroundColor: '#ffffff',
                                                borderRadius: 10,
                                                marginBottom: 20,
                                                padding: 4,
                                            }}>
                                                <TextInput style={{
                                                    fontWeight: 'bold'
                                                }}
                                                    placeholder="Bank Name"
                                                    value={this.state.bankname}
                                                    onChangeText={bankname => this.setState({ bankname })} />
                                            </View>
                                        </View>
                                        <View style={{
                                            flex: 1,
                                            paddingLeft: 5
                                        }}>
                                            <Text style={[ThemeStyle.inputLavelText]}>IFSC Code</Text>
                                            <View style={{
                                                backgroundColor: '#ffffff',
                                                borderRadius: 10,
                                                marginBottom: 20,
                                                padding: 4,
                                            }}>
                                                <TextInput style={{
                                                    fontWeight: 'bold'
                                                }}
                                                    placeholder="IFSC code"
                                                    value={this.state.ifsccode}
                                                    onChangeText={ifsccode => this.setState({ ifsccode })} />
                                            </View>
                                        </View>
                                    </View>
                                </View>


                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'space-between',
                                    }}>
                                    <TouchableOpacity
                                        style={{
                                            flex: 1,
                                            marginRight: 5
                                        }}
                                        onPress={() => this.editConfirm()}>
                                        <View>
                                            <View style={{ padding: 18, backgroundColor: '#2566FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                                <Text style={[ThemeStyle.activebtntext]}>Disburse amount</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{
                                        flex: 1
                                    }}
                                        onPress={() => this.editConfirm()}>
                                        <View>
                                            <View style={{ padding: 18, borderColor: "#2566FF", borderWidth: 1, borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                                <Text style={[ThemeStyle.borderbtntext]}>Not now</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>


                                </View>
                                <View style={{ height: 20 }} />
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 60 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#20B77E",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    progressbar: {
        marginVertical: 20
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    container: {
        flex: 1, marginHorizontal: 25
    }
});

export default EditAndConfirm;
