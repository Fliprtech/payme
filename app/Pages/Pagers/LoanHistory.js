import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image } from "react-native";
import React from 'react';
import { Avatar } from 'react-native-elements';
import BottomBar from "../components/BottomBar";
import BlueLogo from "../components/BlueLogo";
import ThemeStyle from '../../assets/css/ThemeStyle';


class LoanHistory extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                <View style={styles.container}>

                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-end' }}>
                        <View style={styles.inner_block}>
                            <BlueLogo />
                            <View style={styles.fullamountblock}>

                                <View style={styles.approveamount}>
                                    <Avatar size="small" icon={{ name: 'square', type: 'font-awesome' }}
                                        overlayContainerStyle={{ backgroundColor: '#F75F00', borderRadius: 5 }} />
                                    <Text
                                        style={[
                                            ThemeStyle.headingFontMontserratBold,
                                            { color: "#F75F00", fontSize: 20, marginHorizontal: 15, }
                                        ]}>
                                        Loan history
                                    </Text>
                                </View>

                                <View style={{ marginHorizontal: 15, marginVertical: 5 }}>
                                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", backgroundColor: "#FEDFCC", padding: 10, borderRadius: 10, marginVertical: 10 }}>
                                        <View>
                                            <Text style={{ fontSize: 12 }}>Credited</Text>
                                            <Text style={{ fontSize: 14, marginVertical: 5 }}>08 NOV 2019, 02:00 AM</Text>
                                        </View>
                                        <Text style={{ fontSize: 14, marginVertical: 5, fontWeight: "bold", color: "#20B77E" }}>+  ₹ 4500</Text>
                                    </View>
                                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", backgroundColor: "#FEDFCC", padding: 10, borderRadius: 10, marginVertical: 10 }}>
                                        <View>
                                            <Text style={{ fontSize: 12 }}>Credited</Text>
                                            <Text style={{ fontSize: 14, marginVertical: 5 }}>08 NOV 2019, 02:00 AM</Text>
                                        </View>
                                        <Text style={{ fontSize: 14, marginVertical: 5, fontWeight: "bold", color: "#20B77E" }}>+  ₹ 4500</Text>
                                    </View>
                                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", backgroundColor: "#FEDFCC", padding: 10, borderRadius: 10, marginVertical: 10 }}>
                                        <View>
                                            <Text style={{ fontSize: 12 }}>Credited</Text>
                                            <Text style={{ fontSize: 14, marginVertical: 5 }}>08 NOV 2019, 02:00 AM</Text>
                                        </View>
                                        <Text style={{ fontSize: 14, marginVertical: 5, fontWeight: "bold", color: "#FF0808" }}>-   ₹ 1250</Text>
                                    </View>
                                </View>
                            </View>

                        </View>
                    </View>
                    <View style={{ alignItems: "center", marginVertical: 20, }}>
                        <Image source={require('../../assets/images/colorbar.png')} />
                    </View>
                </View>
                <View style={{ height: 60 }} />
                <BottomBar {...this.props} />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        flex: 1
    },

    inner_block: {

    },
    img_block: {
        marginVertical: 30
    },

    greendot: {
        padding: 3,
        backgroundColor: "#20b77e",
        paddingHorizontal: 5,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    orangedot: {
        backgroundColor: "#f75f00",
        padding: 3,
        paddingHorizontal: 5

    },
    reddot: {
        backgroundColor: "#ff085a",
        padding: 3,
        paddingHorizontal: 5

    },
    bluedot: {
        backgroundColor: "#7340fb",
        padding: 3,
        paddingHorizontal: 5,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    },
    bottombar: {
        padding: 15,
        marginTop: 10,
        backgroundColor: 'white',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        flexDirection: "row",
        justifyContent: "center"
    },
    fullamountblock: {
        backgroundColor: "#FFEFE5",
        paddingVertical: 10,
        borderRadius: 15
    },
    approveamount: {
        borderBottomWidth: 1,
        borderColor: "#fff",
        padding: 10,
        flexDirection: "row",
        alignItems: "center"
    },


});

export default LoanHistory;