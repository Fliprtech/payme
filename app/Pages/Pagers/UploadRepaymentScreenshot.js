import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, Alert } from 'react-native'
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler'
import ThemeStyle from '../../assets/css/ThemeStyle'
import ImagePicker from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';

export class UploadRepaymentScreenshot extends Component {

    constructor(props) {
        super(props);
        this.state = {
            option_selected: 0,
            all_upload: false,
            first_doc_add: false,
            second_doc_add: false,
        }
    }

    setFileAndState() {
        let option_selected = this.state.option_selected;
        if (option_selected == 1) {
            this.setState({
                first_doc_add: true,
                second_doc_add: false,
                option_selected: 0,
                all_upload: true,
            })
        } else if (option_selected == 2) {
            this.setState({
                first_doc_add: false,
                second_doc_add: true,
                option_selected: 0,
                all_upload: true,
            })
        }
    }


    async uploadStatement() {

        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
            });
            console.log('res : ' + JSON.stringify(res));
            console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size
            );
            this.setFileAndState();

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }

    }

    uploadStateViaCamera() {

        const options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.launchCamera(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                this.setFileAndState();
                const source = { uri: response.uri };
            }
        });
    }


    render() {
        return (
            <View style={[styles.upperbox]} >
                <View style={styles.container} >

                    <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                        <Text style={[ThemeStyle.overlayHeading, ThemeStyle.headingFontMontserratBold]}>
                            {"Upload repayment screenshot"}
                        </Text>
                        <Text style={[ThemeStyle.overlaySubHeading, ThemeStyle.bodyFontRegular]}>
                            {"upload screenshot of the payment you've done"}
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                flex: 1,
                                marginBottom: 10,
                            }}>
                            <View style={{
                                backgroundColor: this.state.first_doc_add ? "#20B77E" : '#E8F8F2',
                                borderRadius: 10,
                                flex: 1,
                                marginRight: 5,
                            }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({
                                            option_selected: 1,
                                        }, () => this.uploadStatement());
                                    }}
                                    style={{
                                        marginRight: 5,
                                        paddingHorizontal: 10,
                                        paddingVertical: 20,
                                    }}>
                                    <View style={{ flex: 1 }}>
                                        {
                                            this.state.first_doc_add ?
                                                <Image source={require('../../assets/images/g_verified/g_verified.png')} />
                                                :
                                                <Image source={require('../../assets/images/cloud_up_green/cloud_up.png')} />
                                        }
                                        <Text style={[
                                            styles.btnText, ThemeStyle.bodyFontSemiBold,
                                            { color: this.state.first_doc_add ? "white" : '#20B77E' }
                                        ]}>
                                            {"Choose from library"}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <View style={{
                                backgroundColor: this.state.second_doc_add ? '#20B77E' : "white",
                                borderRadius: 10,
                                flex: 1,
                                marginLeft: 5,
                                borderWidth: 1,
                                borderColor: "#20B77E",
                            }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({
                                            option_selected: 2,
                                        }, () => this.uploadStateViaCamera());
                                    }}
                                    style={{
                                        paddingHorizontal: 20,
                                        paddingVertical: 15,
                                    }}>
                                    <View>
                                        {
                                            this.state.second_doc_add ?
                                                <Image source={require('../../assets/images/g_verified/g_verified.png')} />
                                                :
                                                <Image source={require('../../assets/images/cam_4_green/cam_4.png')} />
                                        }
                                        <Text style={[
                                            styles.btnText, ThemeStyle.bodyFontSemiBold,
                                            { color: this.state.second_doc_add ? "white" : '#20B77E' }
                                        ]}>
                                            {"Take a picture"}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <TouchableOpacity
                            onPress={() => {
                                this.state.all_upload ?
                                    this.props.navigation.navigate("AmountPaid")
                                    :
                                    Alert.alert('OOPS!', "Please Upload Document")
                            }}
                            style={[ThemeStyle.blueBtn]}>
                            <Text style={[ThemeStyle.activebtntext]}>Upload</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    upperbox: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#00000070",
        position: "absolute", top: 0,
        left: 0, right: 0, bottom: 0,
        zIndex: 10000,
    },
    container: {
        padding: 30,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    },
    btnText: {
        color: '#ffffff',
        marginTop: 10,
        fontSize: 14,
    }
});

export default UploadRepaymentScreenshot

