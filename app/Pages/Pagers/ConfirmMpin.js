import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image } from "react-native";
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Avatar } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";


class ConfirmMpin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            counting: 0,
            confirmMpin: false,
        }
        this.numberClick = this.numberClick.bind(this);
        this.deleteAll = this.deleteAll.bind(this);
        this.backPress = this.backPress.bind(this);
    }

    numberClick(number) {
        if (this.state.counting <= 6) {
            this.setState({
                counting: this.state.counting + 1,
            });
        }
    }
    deleteAll() {
        this.setState({
            counting: 0,
        });
    }
    backPress() {
        if (this.state.counting > 0) {
            this.setState({
                counting: this.state.counting - 1,
            });
        }
    }

    render() {

        const number_size = 40;

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>

                <View style={styles.container}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}>
                        <View style={{ flex: 0 }}>
                            <View style={styles.backicon}>
                                <Icon
                                    name='chevron-left'
                                    color='black'
                                    size={20}
                                />
                            </View>
                            <View style={styles.txtblock}>
                                <Text style={styles.pheading}>Confirm your MPIN</Text>
                                <Text style={styles.psubheading}>please enter your MPIN again to confirm your pin that will work on your next login</Text>
                            </View>
                        </View>
                        <View style={{ flex: 1, justifyContent: "center" }}>
                            <View style={{ marginHorizontal: 20 }}>
                                <View style={styles.fillpinblock}>
                                    <View style={this.state.counting >= 1 ? styles.filldot : styles.notfilldot}></View>
                                    <View style={this.state.counting >= 2 ? styles.filldot : styles.notfilldot}></View>
                                    <View style={this.state.counting >= 3 ? styles.filldot : styles.notfilldot}></View>
                                    <View style={this.state.counting >= 4 ? styles.filldot : styles.notfilldot}></View>
                                    <View style={this.state.counting >= 5 ? styles.filldot : styles.notfilldot}></View>
                                    <View style={this.state.counting >= 6 ? styles.filldot : styles.notfilldot}></View>
                                </View>
                                <View style={styles.numberpad}>
                                    <View style={styles.row}>
                                        <Avatar
                                            size={number_size}
                                            rounded
                                            title="1"
                                            titleStyle={[styles.numbetTextStyle]}
                                            overlayContainerStyle={{ backgroundColor: '#EFEFEF', }}
                                            onPress={() => this.numberClick(1)}
                                            containerStyle={[styles.avtarContainer]}
                                        />
                                        <Avatar
                                            size={number_size}
                                            rounded
                                            title="2"
                                            titleStyle={[styles.numbetTextStyle]}
                                            overlayContainerStyle={{ backgroundColor: '#EFEFEF' }}
                                            onPress={() => this.numberClick(2)}
                                            containerStyle={[styles.avtarContainer]}
                                        />
                                        <Avatar
                                            size={number_size}
                                            rounded
                                            title="3"
                                            titleStyle={[styles.numbetTextStyle]}
                                            overlayContainerStyle={{ backgroundColor: '#EFEFEF' }}
                                            onPress={() => this.numberClick(3)}
                                            containerStyle={[styles.avtarContainer]}
                                        />
                                    </View>
                                    <View style={styles.row}>
                                        <Avatar
                                            size={number_size}
                                            rounded
                                            title="4"
                                            titleStyle={[styles.numbetTextStyle]}
                                            overlayContainerStyle={{ backgroundColor: '#EFEFEF' }}
                                            onPress={() => this.numberClick(4)}
                                            containerStyle={[styles.avtarContainer]}
                                        />
                                        <Avatar
                                            size={number_size}
                                            rounded
                                            title="5"
                                            titleStyle={[styles.numbetTextStyle]}
                                            overlayContainerStyle={{ backgroundColor: '#EFEFEF' }}
                                            onPress={() => this.numberClick(5)}
                                            containerStyle={[styles.avtarContainer]}
                                        />
                                        <Avatar
                                            size={number_size}
                                            rounded
                                            title="6"
                                            titleStyle={[styles.numbetTextStyle]}
                                            overlayContainerStyle={{ backgroundColor: '#EFEFEF' }}
                                            onPress={() => this.numberClick(6)}
                                            containerStyle={[styles.avtarContainer]}
                                        />
                                    </View>
                                    <View style={styles.row}>
                                        <Avatar
                                            size={number_size}
                                            rounded
                                            title="7"
                                            titleStyle={[styles.numbetTextStyle]}
                                            overlayContainerStyle={{ backgroundColor: '#EFEFEF' }}
                                            onPress={() => this.numberClick(7)}
                                            containerStyle={[styles.avtarContainer]}
                                            containerStyle={[styles.avtarContainer]}
                                        />
                                        <Avatar
                                            size={number_size}
                                            rounded
                                            title="8"
                                            titleStyle={[styles.numbetTextStyle]}
                                            overlayContainerStyle={{ backgroundColor: '#EFEFEF' }}
                                            onPress={() => this.numberClick(8)}
                                            containerStyle={[styles.avtarContainer]}
                                        />
                                        <Avatar
                                            size={number_size}
                                            rounded
                                            title="9"
                                            titleStyle={[styles.numbetTextStyle]}
                                            overlayContainerStyle={{ backgroundColor: '#EFEFEF' }}
                                            onPress={() => this.numberClick(9)}
                                            containerStyle={[styles.avtarContainer]}
                                        />
                                    </View>
                                    <View style={styles.row}>
                                        <Avatar
                                            size={number_size}
                                            rounded
                                            icon={{ name: 'chevron-left', size: 16, type: 'font-awesome', color: '#00063B', }}
                                            titleStyle={[styles.numbetTextStyle]}
                                            overlayContainerStyle={{ backgroundColor: '#EFEFEF' }}
                                            onPress={() => this.backPress()}
                                            containerStyle={[styles.avtarContainer]}
                                        />
                                        <Avatar
                                            size={number_size}
                                            rounded
                                            title="0"
                                            titleStyle={[styles.numbetTextStyle]}
                                            overlayContainerStyle={{ backgroundColor: '#EFEFEF' }}
                                            onPress={() => this.numberClick(0)}
                                            containerStyle={[styles.avtarContainer]}
                                        />
                                        <Avatar
                                            size={number_size}
                                            rounded
                                            icon={{ name: 'trash', size: 16, type: 'font-awesome', color: '#00063B', }}
                                            titleStyle={[styles.numbetTextStyle]}
                                            overlayContainerStyle={{ backgroundColor: '#EFEFEF', }}
                                            onPress={() => this.deleteAll()}
                                            containerStyle={[styles.avtarContainer]}
                                        />
                                    </View>

                                </View>
                            </View>

                        </View>
                    </ScrollView>
                </View>

                {
                    this.state.counting >= 6 ?
                        <View style={{ flex: 0, justifyContent: "flex-end" }}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.goBack()}
                                style={{
                                    padding: 25,
                                    backgroundColor: '#2566FF',
                                    borderTopLeftRadius: 15,
                                    borderTopRightRadius: 15,
                                    alignItems: 'center'
                                }}>
                                <Text style={{ fontWeight: "700", color: "white", fontSize: 18 }}>Save MPIN</Text>
                            </TouchableOpacity>
                        </View>
                        :
                        <View />
                }

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 20,
        flex: 1
    },
    backicon: {
        paddingVertical: 20
    },
    txtblock: {

    },
    pheading: {
        color: "#2566FF",
        fontSize: 26
    },
    psubheading: {
        fontSize: 18,
        fontWeight: "400",
        lineHeight: 26,
        marginTop: 20,
        color: '#00063B'
    },
    fillpinblock: {
        flexDirection: "row",
        justifyContent: 'center',
        marginVertical: 15
    },
    numbetTextStyle: {
        color: "#00063B",
        fontWeight: "700",
    },
    filldot: {
        width: 20,
        height: 20,
        backgroundColor: "#00063B",
        borderRadius: 25,
        marginHorizontal: 6
    },
    notfilldot: {
        width: 20,
        height: 20,
        backgroundColor: "#EFEFEF",
        borderRadius: 25,
        marginHorizontal: 6
    },
    row: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginVertical: 10,
    },
    numberpad: {
        marginVertical: 20
    },
    avtarContainer: {
        padding: 10,
        width: 60,
        height: 60,
        borderRadius: 60,
        backgroundColor: '#EFEFEF'
    }
});

export default ConfirmMpin;