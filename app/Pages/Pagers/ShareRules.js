import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native";
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ThemeStyle from "../../assets/css/ThemeStyle";
import ShareAndEarnPopUp from "./ShareAndEarnPopUp";

class ShareRules extends Component {

    constructor(props) {
        super(props)

        this.state = {
            shareBox: false,
        }
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
                <View style={styles.topslider}>
                    <View style={[styles.sliderTextblock, { alignContent: 'center', }]}>
                        <View style={{ paddingVertical: 25, paddingHorizontal: 10, }}>
                            <Text style={[ThemeStyle.bodyFontRegular, { fontSize: 18, color: "#00063B" }]}>
                                {"to learn about coins see"}
                            </Text>
                            <Text style={[ThemeStyle.headingFontMontserratBold, { fontSize: 22, color: "#040B4D", }]}>
                                {"Rules for reference"}
                            </Text>
                        </View>
                    </View>
                </View>
                <ScrollView>
                    <View style={[styles.container]}>
                        <View style={{ marginVertical: 20 }}>
                            <View style={styles.row} >
                                <TouchableOpacity
                                    activeOpacity={0.7}
                                    onPress={() => this.props.navigation.navigate("Repayment")} style={styles.contentbox}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#20B77E", }]}>
                                        {"Registeration"}
                                    </Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>
                                        {"your account will be credited by 1000 coins once your register with the app"}
                                    </Text>
                                </TouchableOpacity>
                                <View style={[styles.contentbox, { backgroundColor: "#FFEFE5" }]}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#F75F00", }]}>
                                        {"Membership"}
                                    </Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>
                                        {"becom a memeber and get 2550 coins in your account."}
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.row} >
                                <View style={[styles.contentbox, { backgroundColor: "#FFE5EE" }]}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#FF085A", }]}>
                                        {"Documents"}
                                    </Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>
                                        {"apply for loan and uplaod all documents and get 1000 coins."}
                                    </Text>
                                </View>
                                <View style={[styles.contentbox, { backgroundColor: "#F1EBFF" }]}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#7340FB", }]}>
                                        {"Payments"}
                                    </Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>
                                        {"pay your lona emi on time and get extra 300 coins to your account."}
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.row} >
                                <TouchableOpacity
                                    activeOpacity={0.7}
                                    onPress={() => this.props.navigation.navigate("Repayment")} style={styles.contentbox}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#20B77E", }]}>
                                        {"Registeration"}
                                    </Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>
                                        {"your account will be credited by 1000 coins once your register with the app"}
                                    </Text>
                                </TouchableOpacity>
                                <View style={[styles.contentbox, { backgroundColor: "#FFEFE5" }]}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#F75F00", }]}>
                                        {"Membership"}
                                    </Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>
                                        {"becom a memeber and get 2550 coins in your account."}
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.row} >
                                <View style={[styles.contentbox, { backgroundColor: "#FFE5EE" }]}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#FF085A", }]}>
                                        {"Documents"}
                                    </Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>
                                        {"apply for loan and uplaod all documents and get 1000 coins."}
                                    </Text>
                                </View>
                                <View style={[styles.contentbox, { backgroundColor: "#F1EBFF" }]}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#7340FB", }]}>
                                        {"Payments"}
                                    </Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>
                                        {"pay your lona emi on time and get extra 300 coins to your account."}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>

                <View style={{ flexDirection: "row", margin: 15 }}>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}
                        style={[styles.submitbutton, { flex: 0, backgroundColor: "white", borderWidth: 1, borderColor: "#2566FF" }]}>
                        <Icon
                            name='chevron-left'
                            color='#2566FF'
                            size={15} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.setState({ shareBox: true })}
                        style={[styles.submitbutton, { alignSelf: "stretch" }]}>
                        <Text style={[ThemeStyle.activebtntext]}>
                            {"Share now"}
                        </Text>
                    </TouchableOpacity>
                </View>

                {
                    this.state.shareBox && <ShareAndEarnPopUp close={() => {
                        this.setState({
                            shareBox: false,
                        });
                    }} {...this.props} />
                }

            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 20,
        flex: 1,
    },
    topslider: {
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: "#DFE8FE",
        alignItems: "center",
        borderBottomLeftRadius: 25,
    },
    sliderTextblock: {
        paddingHorizontal: 20,
    },
    row: {
        flexDirection: "row",
        justifyContent: "space-between",
        flex: 1,
        marginVertical: 10
    },
    contentbox: {
        flex: 1,
        backgroundColor: "#E8F8F2",
        padding: 15,
        marginHorizontal: 5,
        borderRadius: 15
    },
    submitbutton: {
        backgroundColor: "#2566FF",
        paddingHorizontal: 18,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        flex: 1,
        marginHorizontal: 8
    },
    heading: {
        fontSize: 18,
    },
    subHeading: {
        fontSize: 14,
        lineHeight: 22,
        marginVertical: 8,
        color: "#00063B"
    }


});

export default ShareRules;