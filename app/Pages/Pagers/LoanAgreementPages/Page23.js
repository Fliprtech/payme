
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ThemeStyle from '../../../assets/css/ThemeStyle';

class Page23 extends Component {

    render() {
        return (
            <View style={{
                alignItems: 'center',
                margin: 15
            }}>
                <View style={{
                    margin: 15
                }}>
                    <Text style={[ThemeStyle.aggrementText]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In mollis lorem suscipit, pretium leo nec, auctor nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nunc at fringilla magna. Curabitur ut ante sollicitudin, dignissim enim laoreet, ultricies dolor. In lobortis efficitur odio, sed scelerisque felis ultrices nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ornare efficitur euismod. Pellentesque venenatis, quam vel euismod bibendum, purus lectus consectetur metus, id luctus magna sem nec justo. Sed ut ornare nibh. Integer nulla metus, fermentum quis nulla id, eleifend iaculis enim. Mauris id interdum odio. Aenean tortor enim, consequat vitae placerat id, laoreet non ligula. Shailesh Bakhsi</Text>
                    <Text style={[ThemeStyle.aggrementTextHeading]}>Shilesh Bakhsi</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

});

export default Page23;
