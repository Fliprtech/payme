
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ThemeStyle from '../../../assets/css/ThemeStyle';

class Page2 extends Component {

    render() {
        return (
            <View style={{
                alignItems: 'center',
                margin: 15
            }}>
                <Text style={[ThemeStyle.aggrementTextHeading]}>Loan Agreement page 2</Text>
                <Text style={[ThemeStyle.aggrementText]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ante ante, elementum eu eleifend et, cursus sit amet velit. Aenean ut velit eros.</Text>
                <Text style={[ThemeStyle.aggrementText]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ante ante, elementum eu eleifend et, cursus sit amet velit. Aenean ut velit eros. Nam elementum nisl posuere nibh volutpat, vitae semper leo eleifend.</Text>
                <Text style={[ThemeStyle.aggrementText]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ante ante, elementum eu eleifend et, cursus sit amet velit. Aenean ut velit eros. Nam elementum nisl posuere nibh volutpat, vitae semper leo eleifend. Vivamus nulla odio, tempor nec scelerisque id, consectetur vitae lectus. Proin vel tincidunt leo. Mauris sagittis sollicitudin est ac maximus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi pharetra elementum turpis at convallis. Integer rutrum rhoncus fermentum. Mauris varius, orci non feugiat rhoncus, ante velit convallis est, sit amet congue arcu magna eget justo. Etiam dictum metus ac felis consectetur, ac gravida ex consectetur. Etiam metus diam, placerat quis velit vel, pharetra tempus felis. Praesent commodo, nunc sit amet tempor fringilla, erat dui lobortis urna, sed ultrices arcu leo et urna. Aliquam vehicula tempus semper.</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({

});

export default Page2;
