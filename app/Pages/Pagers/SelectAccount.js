import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image } from "react-native";
import React from 'react';
import { Icon } from "react-native-vector-icons/Ionicons";
import { Input } from 'react-native-elements';
import PagerTopBar from "../components/PagerTopBar";
import BottomBar from "../components/BottomBar";
import ThemeStyle from '../../assets/css/ThemeStyle';
import BlueLogo from "../components/BlueLogo";

class SelectAccount extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
        this.next_screen = this.next_screen.bind(this);
    }

    next_screen() {
        this.props.navigation.navigate('ReferalCode');
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                <View style={styles.container}>
                    <PagerTopBar percentage={5} />
                    <View
                        style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'flex-end',
                        }}>
                        <View style={styles.inner_block}>
                            <BlueLogo />
                            <View style={[ThemeStyle.text_block]}>
                                <Text style={[ThemeStyle.processText]}>
                                    Select an account below to sign in with.
                                </Text>
                            </View>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between", marginBottom: 20 }}>
                        <TouchableOpacity
                            onPress={this.next_screen}
                            style={styles.google_block}>
                            <Image style={{ width: 18, height: 18 }} source={require('../../assets/images/google-plus/google-plus.png')}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.next_screen}
                            style={styles.fb_block}>
                            <Image style={{ width: 10, height: 18 }} source={require('../../assets/images/facebook-logo/facebook-logo.png')}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.next_screen}
                            style={styles.linkin_block}>
                            <Image style={{ width: 18, height: 15 }} source={require('../../assets/images/linkedin-letters/linkedin-letters.png')}></Image>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ height: 60 }} />
                <BottomBar {...this.props} />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        flex: 1
    },
    progressbar: {
        marginVertical: 20
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    page_txt: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#00063B",
        lineHeight: 25,
    },
    google_block: {
        backgroundColor: "#FC5350",
        paddingHorizontal: 35,
        paddingVertical: 20,
        borderRadius: 10,
        alignItems: "center",
    },
    fb_block: {
        backgroundColor: "#385898",
        paddingHorizontal: 40,
        paddingVertical: 20,
        borderRadius: 10,
        alignItems: "center",

    },
    linkin_block: {
        backgroundColor: "#2977C9",
        paddingHorizontal: 35,
        paddingVertical: 20,
        borderRadius: 10,
        alignItems: "center",
    },
    greendot: {
        padding: 3,
        backgroundColor: "#20b77e",
        paddingHorizontal: 5,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    orangedot: {
        backgroundColor: "#f75f00",
        padding: 3,
        paddingHorizontal: 5

    },
    reddot: {
        backgroundColor: "#ff085a",
        padding: 3,
        paddingHorizontal: 5

    },
    bluedot: {
        backgroundColor: "#7340fb",
        padding: 3,
        paddingHorizontal: 5,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    },
    bottombar: {
        padding: 15,
        marginTop: 10,
        backgroundColor: 'white',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        flexDirection: "row",
        justifyContent: "center"
    }

});

export default SelectAccount;