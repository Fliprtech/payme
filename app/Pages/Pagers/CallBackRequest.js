import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Input, Overlay } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Rating, AirbnbRating } from 'react-native-elements';
import Repayment from './Repayment';
import ThemeStyle from '../../assets/css/ThemeStyle';

class CallBackRequest extends Component {

    constructor(props) {
        super(props);
        this.state = {
            reasionShow: false,
            reasonText: "",
            callShow: false,
            callText: "",
        }
    }

    selectReason(value) {
        this.setState({
            reasionShow: false,
            reasonText: value,
        });
    }

    selectTime(value) {
        this.setState({
            callShow: false,
            callText: value,
        });
    }

    render() {

        let reasonList = [
            {
                "reason": "Reason 1"
            },
            {
                "reason": "Reason 2"
            },
            {
                "reason": "Reason 3"
            },
            {
                "reason": "Reason 4"
            },
            {
                "reason": "Reason 5"
            },
        ]

        let callList = [
            {
                "time": "07:00AM - 09:00AM"
            },
            {
                "time": "09:00AM - 01:00PM"
            },
            {
                "time": "01:00PM - 05:00PM"
            },
            {
                "time": "05:00PM - 08:00PM"
            },
            {
                "time": "08:00PM - 10:00PM"
            },
        ]

        return (
            <View style={[styles.upperbox, { position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }]} >

                <Overlay isVisible={this.state.reasionShow} height={'auto'}>
                    <View>
                        <Text style={[styles.popupHeading, ThemeStyle.headingFontMontserratBold]}>Select Reason</Text>
                        {
                            reasonList.map((item, index) => (
                                <TouchableOpacity
                                    key={index}
                                    onPress={() => this.selectReason(item.reason)}>
                                    <Text style={[styles.popupItem, ThemeStyle.bodyFontSemiBold]}>{item.reason}</Text>
                                </TouchableOpacity>
                            ))
                        }
                        <View style={{ paddingVertical: 5 }} />
                    </View>
                </Overlay>

                <Overlay isVisible={this.state.callShow} height={'auto'}>
                    <View>
                        <Text style={[styles.popupHeading, ThemeStyle.headingFontMontserratBold]}>Select Time For Call</Text>
                        {
                            callList.map((item, index) => (
                                <TouchableOpacity
                                    key={index}
                                    onPress={() => this.selectTime(item.time)}>
                                    <Text style={[styles.popupItem, ThemeStyle.bodyFontSemiBold]}>{item.time}</Text>
                                </TouchableOpacity>
                            ))
                        }
                        <View style={{ paddingVertical: 5 }} />
                    </View>
                </Overlay>

                <View style={styles.container} >
                    <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'always'}>
                        <Text style={styles.heading} >Enter your mobile number</Text>
                        <Text style={styles.sub_heading}>we will call you back as soon as possible.</Text>
                        <View style={styles.input_block}>
                            <TouchableOpacity
                                onPress={() => this.setState({ reasionShow: true })}
                                activeOpacity={0.8}>
                                <Input
                                    inputContainerStyle={{
                                        borderBottomWidth: 0,
                                        backgroundColor: "#F1F2F5",
                                        borderRadius: 10,
                                        paddingVertical: 5,
                                        marginBottom: 10,
                                    }}
                                    editable={false}
                                    containerStyle={{
                                        padding: 0,
                                        width: '100%',
                                        alignItems: 'stretch',
                                        paddingHorizontal: 0,
                                    }}
                                    value={this.state.reasonText}
                                    inputStyle={[ThemeStyle.inerInputStyle]}
                                    placeholderTextColor="#c5c6d3"
                                    placeholder='select reason for call'
                                    rightIcon={
                                        <Icon
                                            name='chevron-down'
                                            size={14}
                                            color='black'
                                            style={{ marginRight: 15, }} />
                                    } />
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => this.setState({ callShow: true })}
                                activeOpacity={0.8}>
                                <Input
                                    inputContainerStyle={{
                                        borderBottomWidth: 0,
                                        backgroundColor: "#F1F2F5",
                                        borderRadius: 10,
                                        paddingVertical: 5,
                                        marginBottom: 10,
                                    }}
                                    editable={false}
                                    value={this.state.callText}
                                    containerStyle={{
                                        padding: 0,
                                        width: '100%',
                                        alignItems: 'stretch',
                                        paddingHorizontal: 0,
                                    }}
                                    inputStyle={[ThemeStyle.inerInputStyle]}
                                    placeholderTextColor="#c5c6d3"
                                    placeholder='select time of your call'
                                    rightIcon={
                                        <Icon
                                            name='chevron-down'
                                            size={14}
                                            color='black'
                                            style={{ marginRight: 15, }} />
                                    } />
                            </TouchableOpacity>
                            <Input
                                inputContainerStyle={{
                                    borderBottomWidth: 0,
                                    backgroundColor: "#F1F2F5",
                                    borderRadius: 10,
                                    paddingVertical: 5,
                                }}
                                containerStyle={{
                                    padding: 0,
                                    width: '100%',
                                    alignItems: 'stretch',
                                    paddingHorizontal: 0,
                                }}
                                textAlignVertical={'top'}
                                inputStyle={[ThemeStyle.inerInputStyle,]}
                                multiline={true}
                                numberOfLines={5}
                                placeholderTextColor="#c5c6d3"
                                placeholder='write your valuable here.' />
                        </View>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.submitCallBack();
                            }}
                            style={styles.submitbutton}>
                            <Text style={styles.submitbuttontxt}>Request callback</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>

            </View>

        );
    }
}


const styles = StyleSheet.create({
    upperbox: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#00000070",
    },
    heading: {
        color: "#040B4D",
        fontSize: 20,
        fontWeight: "700"
    },
    sub_heading: {
        color: "#00063B",
        fontSize: 16,
        fontWeight: "100",
        marginVertical: 6,
        marginBottom: 15
    },
    container: {
        padding: 30,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    },
    activebutton: {
        backgroundColor: "#E8EFFF",
        padding: 12,
        marginVertical: 10,
        paddingHorizontal: 13,
        borderRadius: 9,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    button: {
        backgroundColor: "#F1F2F5",
        paddingHorizontal: 13,
        padding: 12,
        marginVertical: 8,
        borderRadius: 9,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    buttoncolor: {
        color: "#00063B"
    },
    buttonactivecolor: {
        color: "#2566FF",

    },
    submitbutton: {
        backgroundColor: "#2566FF",
        paddingVertical: 20,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    input_block: {
        marginVertical: 8
    },
    popupHeading: {
        paddingVertical: 10,
        paddingHorizontal: 15,
        color: "#040B4D",
        fontSize: 16,
    },
    popupItem: {
        paddingVertical: 10,
        paddingHorizontal: 15,
        color: "#040B4D",
        fontSize: 14,
    }


});

export default CallBackRequest;