import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { RadioButton } from 'react-native-paper';
import OptionForMandateAndSign from './OptionForMandateAndSign';
import ThemeStyle from '../../assets/css/ThemeStyle';

class Esign extends Component {


    render() {

        return (
            <SafeAreaView style={{ flex: 1, }}>
                <View style={{ flex: 1 }}>
                    <OptionForMandateAndSign />
                </View>
                <View style={[styles.upperbox, { position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }]} >
                    <View style={styles.container} >
                        <Text style={[ThemeStyle.overlayHeading,ThemeStyle.headingFontMontserratBold]} >Digital Signature</Text>
                        <Text style={[ThemeStyle.overlaySubHeading,ThemeStyle.bodyFontRegular]}>read the complete loan agreement and acknowledge</Text>
                        <View style={styles.signatureblock}>
                            <Text style={[ThemeStyle.bodyFontRegular,{ color: "#00063B", fontSize: 13 }]}>you need to accept all the pages to proceed</Text>
                            <View style={styles.progressbar}>
                                <View style={{ borderRadius: 5, width: "100%", backgroundColor: "#fee8d9", padding: 2, position: "relative", }}>
                                    <View style={{ borderRadius: 5, position: "absolute", backgroundColor: "#F75F00", padding: 2, width: "13%" }} />
                                </View>
                            </View>
                        </View>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate("LoanAgreement1")}
                            style={styles.submitbutton}>
                            <Text style={[ThemeStyle.activebtntext]}>Continue</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}


const styles = StyleSheet.create({
    upperbox: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#00000070",
    },    
    container: {
        padding: 30,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    },

    submitbutton: {
        backgroundColor: "#2566FF",
        paddingHorizontal: 13,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    progressbar: {
        marginVertical: 8
    },
    signatureblock: {
        backgroundColor: "#fff7f2",
        padding: 20,
        marginVertical: 10,
        paddingVertical: 30,
        borderRadius: 15
    }

});

export default Esign;