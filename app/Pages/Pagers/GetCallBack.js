import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { RadioButton } from 'react-native-paper';
import Repayment from './Repayment';


class GetCallBack extends Component {

    render() {
        return (
            <View style={[styles.upperbox, { position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }]} >
                <View style={styles.container} >
                    <Text style={styles.heading} >Have you got answer?</Text>
                    <Text style={styles.sub_heading}>to the problem you're facing.</Text>
                    <View style={{ flexDirection: "row", }}>
                        <TouchableOpacity
                            onPress={() => this.props.positiveCallBack()}
                            style={[styles.submitbutton]}>
                            <Text style={styles.submitbuttontxt}>Yes, I got</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.negativeCallBack()}
                            style={[styles.submitbutton, { flex: 1, backgroundColor: "white", borderWidth: 1, borderColor: "#2566FF" }]}>
                            <Text style={[styles.submitbuttontxt, { color: "#2566FF" }]}>No, I didn't</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View >
        );
    }
}


const styles = StyleSheet.create({
    upperbox: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#00000070",
    },
    heading: {
        color: "#040B4D",
        fontSize: 20,
        fontWeight: "700"
    },
    sub_heading: {
        color: "#00063B",
        fontSize: 16,
        fontWeight: "100",
        marginVertical: 6,
        marginBottom: 15
    },
    container: {
        padding: 30,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    },
    submitbutton: {
        backgroundColor: "#2566FF",
        paddingHorizontal: 10,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        flex: 1,
        marginHorizontal: 5
    },
    submitbuttontxt: {
        color: "white",
        fontSize: 16,
        textAlign: "center"
    },



});

export default GetCallBack;