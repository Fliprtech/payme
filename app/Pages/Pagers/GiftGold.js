import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native";
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Input } from "react-native-elements";
import ThemeStyle from "../../assets/css/ThemeStyle";

class GiftGold extends Component {


    render() {

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>

                <View style={styles.topslider}>
                    <View style={[styles.sliderTextblock, { marginVertical: 15 }]}>
                        <Text style={[ThemeStyle.bodyFontSemiBold, ThemeStyle.profileHeadingTopText]}>Know more about us </Text>
                        <Text style={[ThemeStyle.headingFontMontserratBold, ThemeStyle.profileHeading, { color: "#F75F00" }]}>Gift Gold</Text>
                    </View>
                </View>


                <ScrollView>
                    <View style={[styles.container]}>
                        <Text style={{ textAlign: "right", fontSize: 15, paddingVertical: 20, }}>Gold history</Text>
                        <View style={{ backgroundColor: "#FFEFE5", padding: 5, paddingVertical: 25, marginTop: 30, borderRadius: 12 }}>
                            <View >
                                <Input
                                    leftIconContainerStyle={{
                                    }}
                                    inputContainerStyle={{
                                        borderBottomWidth: 0,
                                        backgroundColor: "white",
                                        borderRadius: 8
                                    }}
                                    inputStyle={{
                                        fontSize: 16,
                                        fontWeight: "700",
                                        color: "white",
                                        marginLeft: 10,
                                    }}
                                    label="Mobile Number"
                                    labelStyle={[
                                        {
                                            color: "#00063B",
                                            fontSize: 18,
                                            marginVertical: 15,
                                        },
                                        ThemeStyle.headingFontMontserratBold,
                                    ]}
                                    placeholder='enter mobile here'
                                    placeholderTextColor='#dddee5'
                                    leftIcon={
                                        <Icon
                                            name='phone-volume'
                                            size={20}
                                            color='black' />
                                    } />
                            </View>
                            <View >
                                <Input
                                    leftIconContainerStyle={{
                                    }}
                                    inputContainerStyle={{
                                        borderBottomWidth: 0,
                                        backgroundColor: "white",
                                        borderRadius: 8
                                    }}
                                    inputStyle={{
                                        fontSize: 16,
                                        fontWeight: "700",
                                        color: "white",
                                        marginLeft: 10,
                                    }}
                                    label="Enter amount"
                                    labelStyle={{
                                        color: "#00063B",
                                        fontSize: 18,
                                        marginVertical: 15
                                    }}
                                    placeholder='enter amount here'
                                    placeholderTextColor='#dddee5'
                                    leftIcon={
                                        <Icon
                                            name='rupee-sign'
                                            size={20}
                                            color='black'
                                        />
                                    } />
                            </View>
                        </View>
                    </View>
                </ScrollView>

                <View style={{ flexDirection: "row", margin: 15, }} >
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={[styles.submitbutton, { flex: 0, backgroundColor: "white", borderWidth: 1, borderColor: "#2566FF" }]}>
                        <Icon
                            name='chevron-left'
                            color='#2566FF'
                            size={15}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate("GetDelivery")}
                        style={[styles.submitbutton, { alignSelf: "stretch", backgroundColor: "#F75F00" }]}>
                        <Text style={[ThemeStyle.activebtntext]}>Gift Gold</Text>
                    </TouchableOpacity>

                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 20,
        flex: 1,
    },
    topslider: {
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: "#FFEFE5",
        borderBottomLeftRadius: 30,
        marginBottom: 10
    },
    sliderTextblock: {
        padding: 30,
    },
    row: {
        flexDirection: "row",
        marginVertical: 10,
    },
    submitbutton: {
        paddingHorizontal: 18,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        flex: 1,
        marginHorizontal: 8
    },
});

export default GiftGold;