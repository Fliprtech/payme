import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native";
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import GetCallBack from "./GetCallBack";
import CallBackRequest from "./CallBackRequest";
import ThemeStyle from "../../assets/css/ThemeStyle";

class Repayment extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showOption: false,
            showForm: false,
        }
        this.openOption = this.openOption.bind(this);
        this.positiveCallBack = this.positiveCallBack.bind(this);
        this.negativeCallBack = this.negativeCallBack.bind(this);
        this.submitCallBack = this.submitCallBack.bind(this);
    }

    openOption() {
        this.setState({
            showOption: true,
        })
    }

    positiveCallBack() {
        this.setState({
            showOption: false,
        })
    }

    negativeCallBack() {
        this.setState({
            showOption: false,
            showForm: true,
        })
    }
    submitCallBack() {
        this.setState({
            showOption: false,
            showForm: false,
        })
    }

    render() {

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
                <View style={[styles.topslider]}>
                    <View style={[styles.slidericonblock, { position: 'absolute' }]}>
                        <TouchableOpacity
                            style={{ padding: 20, }}
                            onPress={() => this.props.navigation.goBack()}>
                            <Icon
                                name='chevron-left'
                                color='#20B77E'
                                size={18} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.sliderTextblock}>
                        <Text style={[ThemeStyle.bodyFontSemiBold, { fontSize: 16, color: '#040B4D' }]}>resolve your query about</Text>
                        <Text style={[ThemeStyle.headingFontMontserratBold, { fontSize: 22, color: "#20B77E", lineHeight: 45 }]}>Repayment</Text>
                    </View>
                </View>

                <ScrollView>
                    <View style={styles.container}>
                        <View style={[styles.DetailsBlock, { marginTop: 30 }]}>
                            <View style={styles.boxheader}>
                                <View><Text style={[ThemeStyle.bodyFontSemiBold, styles.textSmallText]}>ID: 6167878707</Text></View>
                                <View><Text style={[ThemeStyle.bodyFontSemiBold, styles.textSmallText]}>10 Oct 10:49PM</Text></View>
                            </View>

                            <View style={styles.boxcontent}>
                                <Text style={[ThemeStyle.bodyFontSemiBold, ThemeStyle.textSubHeading]}>Transferred to ac no. 0014212199875</Text>
                                <Text style={{ fontSize: 16, color: "#FF0808", fontWeight: "bold" }}>- ₹ 4500</Text>
                            </View>

                            <TouchableOpacity
                                onPress={() => this.openOption()}
                                style={styles.choosebtn}>
                                <Text style={[ThemeStyle.bodyFontSemiBold, styles.btnText]}>Choose</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.DetailsBlock}>
                            <View style={styles.boxheader}>
                                <View><Text style={[ThemeStyle.bodyFontSemiBold, styles.textSmallText]}>ID: 6167878707</Text></View>
                                <View><Text style={[ThemeStyle.bodyFontSemiBold, styles.textSmallText]}>10 Oct 10:49PM</Text></View>
                            </View>

                            <View style={styles.boxcontent}>
                                <Text style={[ThemeStyle.bodyFontSemiBold, ThemeStyle.textSubHeading]}>Transferred to ac no. 0014212199875</Text>
                                <Text style={{ fontSize: 16, color: "#20B77E", fontWeight: "bold" }}>₹ 4500</Text>
                            </View>

                            <TouchableOpacity
                                onPress={() => this.openOption()}
                                style={styles.choosebtn}>
                                <Text style={[ThemeStyle.bodyFontSemiBold, styles.btnText]}>Choose</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.DetailsBlock]}>
                            <View style={styles.boxheader}>
                                <View><Text style={[ThemeStyle.bodyFontSemiBold, styles.textSmallText]}>ID: 6167878707</Text></View>
                                <View><Text style={[ThemeStyle.bodyFontSemiBold, styles.textSmallText]}>10 Oct 10:49PM</Text></View>
                            </View>

                            <View style={styles.boxcontent}>
                                <Text style={[ThemeStyle.bodyFontSemiBold, ThemeStyle.textSubHeading]}>Transferred to ac no. 0014212199875</Text>
                                <Text style={{ fontSize: 16, color: "#FF0808", fontWeight: "bold" }}>- ₹ 4500</Text>
                            </View>

                            <TouchableOpacity
                                onPress={() => this.openOption()}
                                style={styles.choosebtn}>
                                <Text style={[ThemeStyle.bodyFontSemiBold, styles.btnText]}>Choose</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.DetailsBlock}>
                            <View style={styles.boxheader}>
                                <View><Text style={[ThemeStyle.bodyFontSemiBold, styles.textSmallText]}>ID: 6167878707</Text></View>
                                <View><Text style={[ThemeStyle.bodyFontSemiBold, styles.textSmallText]}>10 Oct 10:49PM</Text></View>
                            </View>

                            <View style={styles.boxcontent}>
                                <Text style={[ThemeStyle.bodyFontSemiBold, ThemeStyle.textSubHeading]}>Transferred to ac no. 0014212199875</Text>
                                <Text style={{ fontSize: 16, color: "#20B77E", fontWeight: "bold" }}>₹ 4500</Text>
                            </View>

                            <TouchableOpacity
                                onPress={() => this.openOption()}
                                style={styles.choosebtn}>
                                <Text style={[ThemeStyle.bodyFontSemiBold, styles.btnText]}>Choose</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.DetailsBlock]}>
                            <View style={styles.boxheader}>
                                <View><Text style={[ThemeStyle.bodyFontSemiBold, styles.textSmallText]}>ID: 6167878707</Text></View>
                                <View><Text style={[ThemeStyle.bodyFontSemiBold, styles.textSmallText]}>10 Oct 10:49PM</Text></View>
                            </View>

                            <View style={styles.boxcontent}>
                                <Text style={[ThemeStyle.bodyFontSemiBold, ThemeStyle.textSubHeading]}>Transferred to ac no. 0014212199875</Text>
                                <Text style={{ fontSize: 16, color: "#FF0808", fontWeight: "bold" }}>- ₹ 4500</Text>
                            </View>

                            <TouchableOpacity
                                onPress={() => this.openOption()}
                                style={styles.choosebtn}>
                                <Text style={[ThemeStyle.bodyFontSemiBold, styles.btnText]}>Choose</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.DetailsBlock}>
                            <View style={styles.boxheader}>
                                <View><Text style={[ThemeStyle.bodyFontSemiBold, styles.textSmallText]}>ID: 6167878707</Text></View>
                                <View><Text style={[ThemeStyle.bodyFontSemiBold, styles.textSmallText]}>10 Oct 10:49PM</Text></View>
                            </View>

                            <View style={styles.boxcontent}>
                                <Text style={[ThemeStyle.bodyFontSemiBold, ThemeStyle.textSubHeading]}>Transferred to ac no. 0014212199875</Text>
                                <Text style={{ fontSize: 16, color: "#20B77E", fontWeight: "bold" }}>₹ 4500</Text>
                            </View>

                            <TouchableOpacity
                                onPress={() => this.openOption()}
                                style={styles.choosebtn}>
                                <Text style={[ThemeStyle.bodyFontSemiBold, styles.btnText]}>Choose</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>

                {
                    this.state.showOption ? <GetCallBack positiveCallBack={this.positiveCallBack} negativeCallBack={this.negativeCallBack} /> : <View />
                }
                {
                    this.state.showForm ? <CallBackRequest submitCallBack={this.submitCallBack} /> : <View />
                }

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    topslider: {
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: "#E8F8F2",
        borderBottomLeftRadius: 25,
        padding: 20
    },
    sliderTextblock: {
        flex: 1,
        alignItems: "center",
        paddingVertical: 50
    },
    slidericonblock: {
        flex: 0,
        alignSelf: "flex-end"
    },
    container: {
        marginHorizontal: 20
    },
    DetailsBlock: {
        backgroundColor: "#E8F8F2",
        marginVertical: 10,
        borderRadius: 10
    },
    boxheader: {
        flexDirection: "row",
        justifyContent: "space-between",
        borderBottomColor: "#fff",
        borderBottomWidth: 1,
        padding: 10,
        paddingVertical: 20
    },
    boxcontent: {
        flexDirection: "row",
        padding: 10,
        justifyContent: "space-between"
    },
    choosebtn: {
        padding: 15,
        backgroundColor: "#D2F1E5",
        margin: 10,
        borderRadius: 8
    },
    textSmallText: {
        fontSize: 12,
        color: "#00063B",
    },
    textSubHeading: {
        fontSize: 16,
        color: "#00063B",
    },
    btnText: {
        color: "#20B77E",
        textAlign: "center",
        fontSize: 14,
    }
});

export default Repayment;