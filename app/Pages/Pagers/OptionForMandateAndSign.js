
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView, Button, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';
import ImagePicker from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
import { Overlay } from 'react-native-elements';

class OptionForMandateAndSign extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showChooser: false,
            option_selected: 0,
            all_upload: false,
            first_doc_add: false,
            second_doc_add: false,
        }
    }

    setFileAndState() {
        let option_selected = this.state.option_selected;
        if (option_selected == 1) {
            this.setState({
                showChooser: false,
                first_doc_add: true,
                option_selected: 0,
            }, () => this.changeAllUpload())
        } else if (option_selected == 2) {
            this.setState({
                showChooser: false,
                second_doc_add: true,
                option_selected: 0,
            }, () => this.changeAllUpload())
        }
    }

    changeAllUpload() {
        let alluploaded = false;
        if (!this.state.first_doc_add) {
            alluploaded = false;
        } else if (!this.state.second_doc_add) {
            alluploaded = false;
        } else {
            alluploaded = true;
        }
        this.setState({
            all_upload: alluploaded,
        })
    }

    async uploadStatement() {

        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
            });
            console.log('res : ' + JSON.stringify(res));
            console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size
            );
            this.setFileAndState();

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                this.setState({
                    showChooser: false,
                });
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }

    }

    uploadStateViaCamera() {

        const options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.launchCamera(options, (response) => {
            if (response.didCancel) {
                this.setState({ showChooser: false });
                console.log('User cancelled image picker');
            } else if (response.error) {
                this.setState({ showChooser: false });
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                this.setState({ showChooser: false });
                console.log('User tapped custom button: ', response.customButton);
            } else {
                this.setFileAndState();
                const source = { uri: response.uri };
            }
        });
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <Overlay
                    // width={'auto'}
                    height={'auto'}
                    isVisible={this.state.showChooser}>
                    <View>
                        <Text
                            style={{
                                fontSize: 18, fontWeight: '700',
                                paddingHorizontal: 10,
                                paddingVertical: 15
                            }}>
                            Choose Option
                    </Text>
                        <View>
                            <TouchableOpacity
                                onPress={() => {
                                    this.uploadStateViaCamera();
                                }}
                                style={{
                                    paddingHorizontal: 20,
                                    paddingVertical: 15,
                                    fontSize: 16,
                                    backgroundColor: '#f0f0f0',
                                    marginVertical: 10,
                                    marginHorizontal: 10,
                                }}>
                                <Text>
                                    Camera
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.uploadStatement();
                                }}
                                style={{
                                    paddingHorizontal: 20,
                                    paddingVertical: 15,
                                    fontSize: 16,
                                    backgroundColor: '#f0f0f0',
                                    marginVertical: 10,
                                    marginHorizontal: 10,
                                }}>
                                <Text>
                                    Storage
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ alignItems: 'center', paddingHorizontal: 10, paddingVertical: 20 }}>
                            <Button
                                title="Close"
                                type="solid"
                                raised={true}
                                containerStyle={{
                                }}
                                buttonStyle={{
                                    paddingHorizontal: 30,
                                }}
                                style={{
                                    paddingHorizontal: 20,
                                }}
                                onPress={() => {
                                    this.setState({
                                        showChooser: false,
                                        option_selected: 0,
                                    });
                                }} />
                        </View>
                    </View>
                </Overlay>
                <View style={styles.container}>
                    <PagerTopBar percentage={85} />

                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <View>

                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <View
                                    style={{
                                        justifyContent: "flex-end",
                                        flex: 1,
                                        flexDirection: "column",
                                    }}>

                                    <BlueLogo />

                                    <View style={[ThemeStyle.text_block]}>
                                        <Text style={[ThemeStyle.processText]}> Great, thanks for that.</Text>
                                    </View>
                                    <View style={[ThemeStyle.text_block]}>
                                        <Text style={[ThemeStyle.processText]}>Your documents are validated and to proceed further we need you to go through our E-Sign and E-Mandate.</Text>
                                    </View>

                                    <View style={{
                                        flexDirection: 'column',
                                    }}>
                                        <View style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            marginBottom: 10,
                                        }}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setState({
                                                        showChooser: true,
                                                        option_selected: 1,
                                                    });
                                                }}
                                                style={{
                                                    flex: 1,
                                                    marginRight: 5,
                                                    paddingLeft: 20,
                                                    paddingVertical: 15,
                                                    backgroundColor: this.state.first_doc_add ? "#20B77E" : '#E8F8F2',
                                                    borderRadius: 10,
                                                }}>
                                                {
                                                    this.state.first_doc_add ?
                                                        <Image source={require('../../assets/images/g_verified/g_verified.png')} />
                                                        :
                                                        <Image source={require('../../assets/images/news_2/news_2.png')} />
                                                }
                                                <Text style={[
                                                    styles.btnText, ThemeStyle.bodyFontSemiBold,
                                                    { color: this.state.first_doc_add ? "white" : '#20B77E' }
                                                ]}>
                                                    E-Mandate
                                                </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setState({
                                                        showChooser: true,
                                                        option_selected: 2,
                                                    });
                                                }}
                                                style={{
                                                    flex: 1,
                                                    paddingLeft: 20,
                                                    paddingVertical: 15,
                                                    backgroundColor: this.state.second_doc_add ? '#F75F00' : "#FFEFE5",
                                                    borderRadius: 10,
                                                }}>
                                                {
                                                    this.state.second_doc_add ?
                                                        <Image source={require('../../assets/images/o_verified/o_verified.png')} />
                                                        :
                                                        <Image source={require('../../assets/images/pen_2/pen_2.png')} />
                                                }
                                                <Text style={[
                                                    styles.btnText, ThemeStyle.bodyFontSemiBold,
                                                    { color: this.state.second_doc_add ? "white" : '#F75F00' }
                                                ]}>
                                                    E-Signature
                                                </Text>
                                            </TouchableOpacity>

                                        </View>

                                    </View>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.state.all_upload ?
                                                this.props.navigation.navigate("PhysicalNach")
                                                :
                                                Alert.alert('OOPS!', "Please Upload Document")
                                        }}
                                        style={{
                                        }}>
                                        <View style={{ paddingVertical: 20, paddingHorizontal: 15, width: "100%", borderColor: "#2566FF", borderWidth: 1.5, borderRadius: 10, }}>
                                            <Text style={[ThemeStyle.borderbtntext]}>Send physical documents</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#20B77E",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    progressbar: {
        marginVertical: 20
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    container: {
        flex: 1, marginHorizontal: 25
    },
    btnText: {
        color: '#ffffff',
        marginTop: 10,
        fontSize: 14,
    }
});

export default OptionForMandateAndSign;
