import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';

class SelectEMILoanOption extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={[ThemeStyle.progressContainer]}>
                    <PagerTopBar percentage={90} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <View style={{
                                    justifyContent: "flex-end",
                                    flex: 1,
                                    flexDirection: "column",
                                }}>
                                    <BlueLogo />

                                    <View style={[ThemeStyle.text_block]}>
                                        <Text style={[ThemeStyle.processText]}>
                                            Please select from the option below to proceed furhter.
                                        </Text>
                                    </View>

                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            alignItems: 'stretch',
                                            flexWrap: 'wrap',
                                        }}>
                                        <TouchableOpacity
                                            onPress={() => this.props.navigation.navigate("ShortTermLoan")}
                                            style={[ThemeStyle.blueBtn, { flex: 1, marginRight: 5, }]}>
                                            <Image
                                                style={{ marginBottom: 13, }}
                                                source={require('../../assets/images/Path_6761/Path_6761.png')} />
                                            <Text style={[ThemeStyle.activebtntext, { textAlign: 'left' }]}>Short term loan</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                            onPress={() => this.props.navigation.navigate("SelectAmount")}
                                            style={[ThemeStyle.whiteBtn, { flex: 1, marginLeft: 5, }]}>
                                            <Image
                                                style={{ marginBottom: 10, }}
                                                source={require('../../assets/images/emi_loan/emi_loan.png')} />
                                            <Text style={[ThemeStyle.borderbtntext, { textAlign: 'left' }]}>EMI Loan</Text>
                                        </TouchableOpacity>

                                    </View>

                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

export default SelectEMILoanOption;
