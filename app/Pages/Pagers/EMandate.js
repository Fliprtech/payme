import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { RadioButton } from 'react-native-paper';
import OptionForMandateAndSign from './OptionForMandateAndSign';
import ThemeStyle from '../../assets/css/ThemeStyle';


class EMandate extends Component {

    render() {

        return (
            <SafeAreaView style={{ flex: 1, }}>
                <View style={{ flex: 1 }}>
                    <OptionForMandateAndSign />
                </View>

                <View style={[styles.upperbox, { position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }]} >
                    <View style={styles.container} >
                        <Text style={[ThemeStyle.overlayHeading,ThemeStyle.headingFontMontserratBold]} >Digital Mandate</Text>
                        <Text style={[ThemeStyle.overlaySubHeading,ThemeStyle.bodyFontRegular]}>information is secured as per standard encryption</Text>
                        <View style={styles.alldatesblock}>
                            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                <View style={[styles.datebox]}>
                                    <Text style={[styles.dateheading,ThemeStyle.bodyFontBold]}>13-09-2019</Text>
                                    <Text style={[styles.datesubheading,ThemeStyle.bodyFontRegular]}>start date</Text>
                                </View>
                                <View style={[styles.datebox]}>
                                    <Text style={[styles.dateheading,ThemeStyle.bodyFontBold, { color: "#F75F00" }]}>12-09-2020</Text>
                                    <Text style={[styles.datesubheading,ThemeStyle.bodyFontRegular]}>end date</Text>
                                </View>
                                <View style={[styles.datebox]}>
                                    <Text style={[styles.dateheading,ThemeStyle.bodyFontBold, { color: "#FF085A" }]}>Weekly</Text>
                                    <Text style={[styles.datesubheading,ThemeStyle.bodyFontRegular]}>frequency</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: "row" }}>
                                <View style={[styles.datebox]}>
                                    <Text style={[styles.dateheading,ThemeStyle.bodyFontBold, { color: "#7340FB" }]}>Variable</Text>
                                    <Text style={[styles.datesubheading,ThemeStyle.bodyFontRegular]}>amount type</Text>
                                </View>
                                <View style={[styles.datebox,ThemeStyle.bodyFontBold, { marginLeft: 35 }]}>
                                    <Text style={[styles.dateheading, { color: "#2566FF" }]}>137500</Text>
                                    <Text style={[styles.datesubheading,ThemeStyle.bodyFontRegular]}>max debit amount</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.input_block}>
                            <Input
                                inputContainerStyle={{
                                    borderBottomWidth: 0,
                                    backgroundColor: "#F1F2F5",
                                    borderRadius: 10
                                }}
                                containerStyle={{
                                    padding: 0,
                                    width: '100%',
                                    alignItems: 'stretch',
                                    paddingHorizontal: 0,
                                }}
                                inputStyle={{
                                    fontSize: 15,
                                    fontWeight: "700",
                                    color: "black",
                                    marginLeft: 15,
                                    padding: 10,
                                    paddingVertical: 15
                                }}
                                placeholderTextColor="#c5c6d3"
                                placeholder='e.g. HDFC Bank'/>
                        </View>

                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate("ESign")}
                            style={styles.submitbutton}>
                            <Text style={[ThemeStyle.activebtntext]}>Pay ₹ 1.00</Text>
                        </TouchableOpacity>
                    </View>

                </View>

            </SafeAreaView>

        );
    }
    
}


const styles = StyleSheet.create({
    upperbox: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#00000070",
    },   
    container: {
        padding: 30,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    },

    submitbutton: {
        backgroundColor: "#2566FF",
        paddingHorizontal: 13,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
    },    
    input_block: {
        marginVertical: 8
    },
    datebox: {
        padding: 8
    },
    dateheading: {
        color: "#20B77E",
        fontSize: 16,
        fontWeight: "700"
    },
    datesubheading: {
        fontSize: 14,
        lineHeight: 30,
        color: "#00063B"
    },
    alldatesblock: {
        marginVertical: 5
    }

});

export default EMandate;