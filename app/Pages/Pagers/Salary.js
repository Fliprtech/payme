
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';

class Salary extends Component {


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={styles.container}>
                    <PagerTopBar percentage={25} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <BlueLogo />

                                <View style={[ThemeStyle.text_block]}>
                                    <Text style={[ThemeStyle.processText]}>So, how do you get your salary?</Text>
                                </View>

                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',

                                }}>
                                    <TouchableOpacity style={styles.activebutton} onPress={() => this.props.navigation.navigate("CompanyName")}>
                                        <Text style={[ThemeStyle.activebtntext]}>NEFT/ IMPS</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.borderbutton} onPress={() => this.props.navigation.navigate("CompanyName")}>
                                        <Text style={[ThemeStyle.borderbtntext]}>Cheque/ Cash</Text>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    activebutton: {
        backgroundColor: "#2664FF",
        borderRadius: 10,
        flex: 1,
        paddingVertical: 20,
        paddingHorizontal: 5,
        marginRight: 5,
        borderWidth: 1.2,
        borderColor: "#2664FF",
    },
    borderbutton: {
        borderColor: "#2664FF",
        borderRadius: 10,
        paddingVertical: 20,
        paddingHorizontal: 5,
        borderWidth: 1.2,
        flex: 1,
        marginLeft: 5
    },
    activebtntext: {
        color: "white",
        textAlign: "center"
    },
    borderbtntext: {
        color: "#2664FF",
        textAlign: "center"
    },
    container: {
        flex: 1, marginHorizontal: 25
    }
});

export default Salary;
