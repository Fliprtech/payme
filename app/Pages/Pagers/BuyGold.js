import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native";
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Input } from "react-native-elements";
import ThemeStyle from "../../assets/css/ThemeStyle";

class BuyGold extends Component {

    constructor(props) {
        super(props);
        this.state = {
            select: 0,
        }
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{}}>
                        <View style={styles.topslider}>
                            <View style={styles.sliderTextblock}>
                                <Text style={[ThemeStyle.bodyFontSemiBold, ThemeStyle.profileHeadingTopText]}>Know more about us </Text>
                                <View height={5} />
                                <Text style={[ThemeStyle.headingFontMontserratBold, ThemeStyle.profileHeading, { color: "#1C99FF" }]}>PayMe Gold</Text>
                                <View height={15} />
                            </View>
                        </View>
                        <View style={[styles.allGoldBlock, { bottom: -45, position: 'absolute', zIndex: 100 }]}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <View style={{}}>
                                    <View style={styles.row} >
                                        <TouchableOpacity
                                            onPress={() => this.props.navigation.navigate("SellMeGold")}
                                            style={[styles.contentbox, {
                                                backgroundColor: "#20B77E",
                                            }]}>
                                            <Image style={{ width: 30, height: 30, }} source={require('../../assets/images/diamond_2/diamond_2.png')}></Image>
                                            <Text style={[ThemeStyle.headingFontMontserratBold, styles.cardSmallHeading]}>Sell Gold</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => this.props.navigation.navigate("GiftGold")}
                                            style={[styles.contentbox, {
                                                backgroundColor: "#F75F00",
                                            }]}>
                                            <Image style={{ width: 30, height: 30, }} source={require('../../assets/images/gift_2/gift_2.png')}></Image>
                                            <Text style={[ThemeStyle.headingFontMontserratBold, styles.cardSmallHeading]}>Gift Gold</Text>
                                        </TouchableOpacity>
                                        <View
                                            style={[styles.contentbox, {
                                                backgroundColor: "#FF085A",
                                            }]}>
                                            <Image style={{ width: 30, height: 30, }} source={require('../../assets/images/gamble_chck/gamble_chck.png')}></Image>
                                            <Text style={[ThemeStyle.headingFontMontserratBold, styles.cardSmallHeading]}>Gold Coins</Text>
                                        </View>
                                        <View
                                            style={[styles.contentbox, {
                                                backgroundColor: "#7340FB",
                                            }]}>
                                            <Image style={{ width: 30, height: 30, }} source={require('../../assets/images/box/box.png')}></Image>
                                            <Text style={[ThemeStyle.headingFontMontserratBold, styles.cardSmallHeading]}>Get delivery</Text>
                                        </View>
                                    </View>
                                </View>
                            </ScrollView>
                        </View>
                    </View>

                    <View style={[styles.container]}>
                        <View style={{ marginTop: 90, paddingHorizontal: 5, }}>
                            <Text style={[ThemeStyle.bodyFontSemiBold, { fontSize: 14, color: '#040B4D' }]}>Select from the option to go further.</Text>
                        </View>
                        <View style={styles.brandlogos}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <TouchableOpacity
                                    onPress={() => this.setState({ select: 1 })}
                                    style={[styles.logoblock, this.state.select == 1 ? { borderColor: "#2566FF", borderWidth: 2 } : {}]}>
                                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Image style={{ width: 15, height: 15 }} source={require('../../assets/images/kalyan_logo/kalyan_logo.png')}></Image>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.card2heading]}>Tanishq</Text>
                                    </View>
                                    <Text style={[ThemeStyle.bodyFontExtraBlack, styles.cardPrice]}>₹ 3960.94/g</Text>
                                    <Text style={[ThemeStyle.bodyFontSemiBold, styles.cardSubheading]}>24K 99.99% Purity</Text>
                                    {
                                        this.state.select == 1 ?
                                            <View style={styles.activeblockicon}>
                                                <Icon
                                                    name='check'
                                                    color='#fff'
                                                    size={10}
                                                />
                                            </View>
                                            :
                                            <View />
                                    }
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.setState({ select: 2 })}
                                    style={[styles.logoblock, this.state.select == 2 ? { borderColor: "#2566FF", borderWidth: 2 } : {}]}>
                                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Image style={{ width: 15, height: 15 }} source={require('../../assets/images/kalyan_logo/kalyan_logo.png')}></Image>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.card2heading]}>Kalyan Jewellers</Text>
                                    </View>
                                    <Text style={[ThemeStyle.bodyFontExtraBlack, styles.cardPrice]}>₹ 3960.94/g</Text>
                                    <Text style={[ThemeStyle.bodyFontSemiBold, styles.cardSubheading]}>24K 99.99% Purity</Text>
                                    {
                                        this.state.select == 2 ?
                                            <View style={styles.activeblockicon}>
                                                <Icon
                                                    name='check'
                                                    color='#fff'
                                                    size={10}
                                                />
                                            </View>
                                            :
                                            <View />
                                    }
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.setState({ select: 3 })}
                                    style={[styles.logoblock, this.state.select == 3 ? { borderColor: "#2566FF", borderWidth: 2 } : {}]}>
                                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Image style={{ width: 15, height: 15 }} source={require('../../assets/images/bitcoin/bitcoin.png')}></Image>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.card2heading]}>Malabaar</Text>
                                    </View>
                                    <Text style={[ThemeStyle.bodyFontExtraBlack, styles.cardPrice]}>₹ 3960.94/g</Text>
                                    <Text style={[ThemeStyle.bodyFontSemiBold, styles.cardSubheading]}>24K 99.99% Purity</Text>
                                    {
                                        this.state.select == 3 ?
                                            <View style={styles.activeblockicon}>
                                                <Icon
                                                    name='check'
                                                    color='#fff'
                                                    size={10}
                                                />
                                            </View>
                                            :
                                            <View />
                                    }
                                </TouchableOpacity>

                            </ScrollView>
                        </View>

                        <View height={10} />
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <View style={styles.buyamoutblock}>
                                <View style={styles.singleblock}>
                                    <View style={styles.amountblockhead}>
                                        <Text style={[ThemeStyle.headingFontMontserratBold, styles.largeCardHeading]}>Buy in amount</Text>
                                    </View>
                                    <View style={styles.amountinputblock}>
                                        <Input
                                            inputContainerStyle={{
                                                borderBottomWidth: 0,
                                                padding: 10
                                            }}
                                            inputStyle={{
                                                fontSize: 30,
                                                fontWeight: "700",
                                                color: "black",
                                                marginLeft: 5,
                                                padding: 10,
                                            }}
                                            placeholderTextColor="#bfe0d4"
                                            placeholder='enter amount'
                                            leftIcon={
                                                <Image
                                                    style={{ width: 25, height: 25, resizeMode: 'contain' }}
                                                    source={require('../../assets/images/rupee-indian/rupee-indian.png')} />
                                            }
                                            leftIconContainerStyle={{
                                                marginLeft: 0,
                                            }}
                                        />
                                    </View>
                                    <View style={styles.boxgstcontent}>
                                        <Text style={[ThemeStyle.bodyFontSemiBold, { fontSize: 14 }]}>inclusive 3% GST Extra</Text>
                                    </View>
                                </View>
                                <View style={[styles.singleblock, { backgroundColor: "#FFEFE5" }]}>
                                    <View style={styles.amountblockhead}>
                                        <Text style={[ThemeStyle.headingFontMontserratBold, styles.largeCardHeading]}>Buy in grams</Text>
                                    </View>
                                    <View style={styles.amountinputblock}>
                                        <Input
                                            inputContainerStyle={{
                                                borderBottomWidth: 0,
                                                padding: 10
                                            }}
                                            inputStyle={{
                                                fontSize: 30,
                                                fontWeight: "700",
                                                color: "black",
                                                marginLeft: 5,
                                                padding: 10,
                                            }}
                                            placeholderTextColor="#e3cfc3"
                                            placeholder='enter grams'
                                            leftIcon={
                                                // <Image source={require('../../assets/images/rupee-indian/rupee-indian.png')}></Image>
                                                <Text style={{ fontSize: 26, fontWeight: 'bold' }}>gm</Text>
                                            }
                                            leftIconContainerStyle={{
                                                marginLeft: 0,
                                            }}
                                        />
                                    </View>
                                    <View style={styles.boxgstcontent}>
                                        <Text style={[ThemeStyle.bodyFontSemiBold, { fontSize: 14 }]}>inclusive 3% GST Extra</Text>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>

                    <View style={{ flexDirection: "row", margin: 15, }} >
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                            style={[styles.submitbutton, { flex: 0, backgroundColor: "white", borderWidth: 1, borderColor: "#2566FF" }]}>
                            <Icon
                                name='chevron-left'
                                color='#2566FF'
                                size={15}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate("GetDelivery")}
                            style={[styles.submitbutton, { alignSelf: "stretch" }]}>
                            <Text style={[ThemeStyle.activebtntext]}>Buy Gold</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 20,
        flex: 1,
    },
    topslider: {
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: "#E7F5FF",
        borderBottomLeftRadius: 25,
        paddingVertical: 40,
    },
    sliderTextblock: {
        padding: 30,
        paddingBottom: 40
    },
    row: {
        flexDirection: "row",
    },
    contentbox: {
        marginHorizontal: 5,
        borderRadius: 15,
        width: 110,
        paddingHorizontal: 15,
        paddingVertical: 15,
        height: 100,
        justifyContent: 'space-between'
    },
    submitbutton: {
        backgroundColor: "#2566FF",
        paddingHorizontal: 18,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        flex: 1,
        marginHorizontal: 8
    },
    allGoldBlock: {
        paddingHorizontal: 20,
    },
    brandlogos: {
        marginVertical: 20
    },
    buyamoutblock: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    singleAmountblock: {
        backgroundColor: "#E8F8F2",
        width: 150
    },
    buyamoutblock: {
        flexDirection: "row",

    },
    amountblockhead: {
        padding: 20,
        borderBottomColor: "white",
        borderBottomWidth: 1,

    },
    singleblock: {
        backgroundColor: "#E8F8F2",
        borderRadius: 20,
        marginRight: 15,
        width: 270
    },
    amountinputblock: {

    },
    boxgstcontent: {
        paddingHorizontal: 20,
        paddingBottom: 25,
        paddingTop: 10
    },
    activeblockicon: {
        backgroundColor: "#2566FF",
        borderTopRightRadius: 10,
        padding: 5,
        position: "absolute",
        right: 0,
        borderBottomLeftRadius: 12
    },
    logoblock: {
        backgroundColor: "#E7F5FF",
        padding: 15,
        paddingHorizontal: 25,
        borderRadius: 15,
        marginHorizontal: 5,
    },
    cardSmallHeading: {
        fontSize: 13,
        fontWeight: "bold",
        marginTop: 15,
        color: "#FFFFFF",
    },
    card2heading: {
        marginLeft: 5,
        fontSize: 12,
    },
    cardPrice: {
        fontSize: 22,
        color: "#2566FF",
        marginVertical: 10,
    },
    cardSubheading: {
        color: "#00063B",
        fontSize: 12,
    },
    largeCardHeading: {
        fontSize: 18,
        color: '#00063B',
    }

});

export default BuyGold;