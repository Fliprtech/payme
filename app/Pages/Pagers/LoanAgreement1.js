
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import BottomBar from '../components/BottomBar';
import LoanBar from '../components/LoanBar';
import Page1 from './LoanAgreementPages/Page1';
import Page2 from './LoanAgreementPages/Page2';
import Page23 from './LoanAgreementPages/Page23';
import Signature from './Signature';
import ThemeStyle from '../../assets/css/ThemeStyle';

class LoanAgreement1 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pageNumber: 1,
            totalPages: 23,
            _pageScroller: null,
            showSignature: false,
        }
    }

    render() {

        var page;
        switch (this.state.pageNumber) {
            case 1:
                page = (<Page1 />);
                break;
            case 2:
                page = (<Page2 />);
                break;
            case 3:
                page = (<Page1 />);
                break;
            case 4:
                page = (<Page1 />);
                break;
            case 5:
                page = (<Page1 />);
                break;
            case 6:
                page = (<Page1 />);
                break;
            case 7:
                page = (<Page1 />);
                break;
            case 8:
                page = (<Page1 />);
                break;
            case 9:
                page = (<Page1 />);
                break;
            case 10:
                page = (<Page1 />);
                break;
            case 11:
                page = (<Page1 />);
                break;
            case 12:
                page = (<Page1 />);
                break;
            case 13:
                page = (<Page1 />);
                break;
            case 14:
                page = (<Page1 />);
                break;
            case 15:
                page = (<Page1 />);
                break;
            case 16:
                page = (<Page1 />);
                break;
            case 17:
                page = (<Page1 />);
                break;
            case 18:
                page = (<Page1 />);
                break;
            case 19:
                page = (<Page1 />);
                break;
            case 20:
                page = (<Page1 />);
                break;
            case 21:
                page = (<Page1 />);
                break;
            case 22:
                page = (<Page1 />);
                break;
            case 23:
                page = (<Page23 />);
                break;
            default:
                page = (<View />);
                break;
        }

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#FFF7F2", }}>
                <View
                    style={{
                        flex: 1,
                        margin: 20
                    }}>
                    <View style={{
                        marginBottom: 10
                    }}>
                        <Text style={[
                            ThemeStyle.headingFontMontserratBold,{
                            fontWeight: "700",
                            color: "#00063B",
                            fontSize: 18,
                        }]}>Digital Signature</Text>
                    </View>
                    <LoanBar pageNumber={this.state.pageNumber} totalPages={this.state.totalPages} />
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        ref={ref => this.state._pageScroller = ref}
                        style={{
                            backgroundColor: "#ffffff",
                            marginLeft: 10,
                            marginRight: 10,
                            borderRadius: 15
                        }}>
                        {page}
                    </ScrollView>
                    {
                        this.state.pageNumber >= this.state.totalPages ?
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({
                                        showSignature: true,
                                    });
                                }}
                                style={{ marginTop: 20 }}>
                                <View style={{ alignItems: 'center', paddingHorizontal: 10, paddingRight: 30, paddingVertical: 20, backgroundColor: '#F75F00', borderRadius: 10, }}>
                                    <Text style={{ color: '#ffffff' }}>Accept all and sign</Text>
                                </View>
                            </TouchableOpacity> :
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginTop: 20

                                }}>
                                <TouchableOpacity
                                    style={{ marginHorizontal: 5, alignItems: 'center', paddingVertical: 20, borderColor: '#F75F00', borderWidth: 1.5, borderRadius: 10, flex: 1 }}
                                    onPress={() => {
                                        if (this.state.pageNumber > 1) {
                                            this.setState({
                                                pageNumber: this.state.pageNumber - 1,
                                            });
                                            this.state._pageScroller.scrollTo({
                                                x: 0
                                            });
                                        }
                                    }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                        <View style={{
                                            marginHorizontal: 5
                                        }}>
                                            <Icon
                                                reverse
                                                name='long-arrow-alt-left'
                                                size={16}
                                                color='#F75F00' />
                                        </View>
                                        <Text style={{ color: '#F75F00', marginHorizontal: 10 }}>Previous</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={{ marginHorizontal: 5, paddingVertical: 20, alignItems: 'center', backgroundColor: '#F75F00', borderRadius: 10, flex: 1 }}
                                    onPress={() => {
                                        if (this.state.pageNumber < this.state.totalPages) {
                                            this.setState({
                                                pageNumber: this.state.pageNumber + 1,
                                            });
                                            this.state._pageScroller.scrollTo({
                                                x: 0
                                            });
                                        }
                                    }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={{ color: '#ffffff', marginHorizontal: 20 }}>Accept</Text>
                                        <View style={{
                                            marginHorizontal: 5
                                        }}>
                                            <Icon
                                                reverse
                                                name='long-arrow-alt-right'
                                                size={16}
                                                color='#ffffff' />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                    }
                </View>
                {
                    this.state.showSignature ? < Signature {...this.props} /> : <View />
                }
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#F75F00",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
});

export default LoanAgreement1;
