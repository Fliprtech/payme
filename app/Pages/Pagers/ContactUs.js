import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native";
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ThemeStyle from "../../assets/css/ThemeStyle";

class ContactUs extends Component {
    render() {

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
                <View style={styles.topslider}>
                    <View style={[styles.sliderTextblock, { alignContent: 'center' }]}>
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <Text style={[ThemeStyle.bodyFontRegular, { fontSize: 18, color: "#00063B" }]}>we are here</Text>
                            <Text style={[ThemeStyle.headingFontMontserratBold, { fontSize: 24, color: "#F75F00", lineHeight: 40 }]}>Contact Us</Text>
                        </View>
                    </View>
                    <View style={[styles.sliderimageblock, { flex: 0, }]}>
                        <Image style={{ width: 180, height: 120, }} source={require('../../assets/images/support2/support2.png')}></Image>
                    </View>
                </View>
                <ScrollView>
                    <View style={[styles.container]}>
                        <View style={{ marginVertical: 25 }}>
                            <View style={styles.row} >
                                <TouchableOpacity
                                    activeOpacity={0.7}
                                    onPress={() => this.props.navigation.navigate("Repayment")} style={styles.contentbox}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#20B77E", }]}>Repayment</Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. convallis.</Text>
                                </TouchableOpacity>
                                <View style={[styles.contentbox, { backgroundColor: "#FFEFE5" }]}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#F75F00", }]}>Refund</Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. convallis.</Text>
                                </View>
                            </View>
                            <View style={styles.row} >
                                <View style={[styles.contentbox, { backgroundColor: "#FFE5EE" }]}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#FF085A", }]}>E nach</Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. convallis.</Text>
                                </View>
                                <View style={[styles.contentbox, { backgroundColor: "#F1EBFF" }]}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#7340FB", }]}>E mandate</Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. convallis.</Text>
                                </View>
                            </View>
                            <View style={styles.row} >
                                <TouchableOpacity
                                    activeOpacity={0.7}
                                    onPress={() => this.props.navigation.navigate("Repayment")} style={styles.contentbox}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#20B77E", }]}>Repayment</Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. convallis.</Text>
                                </TouchableOpacity>
                                <View style={[styles.contentbox, { backgroundColor: "#FFEFE5" }]}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#F75F00", }]}>Refund</Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. convallis.</Text>
                                </View>
                            </View>
                            <View style={styles.row} >
                                <View style={[styles.contentbox, { backgroundColor: "#FFE5EE" }]}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#FF085A", }]}>E nach</Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. convallis.</Text>
                                </View>
                                <View style={[styles.contentbox, { backgroundColor: "#F1EBFF" }]}>
                                    <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading, { color: "#7340FB", }]}>E mandate</Text>
                                    <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. convallis.</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>

                <View style={{ flexDirection: "row", margin: 15 }}>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}
                        style={[styles.submitbutton, { flex: 0, backgroundColor: "white", borderWidth: 1, borderColor: "#2566FF" }]}>
                        <Icon
                            name='chevron-left'
                            color='#2566FF'
                            size={15} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate("Repayment")}
                        style={[styles.submitbutton, { alignSelf: "stretch" }]}>
                        <Text style={[ThemeStyle.activebtntext]}>Raise ticket</Text>
                    </TouchableOpacity>

                </View>
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 20,
        flex: 1,
    },
    topslider: {
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: "#FEDFCC",
        alignItems: "center",
        borderBottomLeftRadius: 25,
    },
    sliderTextblock: {
        paddingHorizontal: 20,
    },
    row: {
        flexDirection: "row",
        justifyContent: "space-between",
        flex: 1,
        marginVertical: 10
    },
    contentbox: {
        flex: 1,
        backgroundColor: "#E8F8F2",
        padding: 15,
        marginHorizontal: 5,
        borderRadius: 15
    },
    submitbutton: {
        backgroundColor: "#2566FF",
        paddingHorizontal: 18,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        flex: 1,
        marginHorizontal: 8
    },
    heading: {
        fontSize: 18,
    },
    subHeading: {
        fontSize: 14,
        lineHeight: 22,
        marginVertical: 8
    }


});

export default ContactUs;