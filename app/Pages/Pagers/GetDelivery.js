import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native";
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Input } from "react-native-elements";
import ThemeStyle from "../../assets/css/ThemeStyle";

class GetDelivery extends Component {

    constructor(props) {
        super(props);
        this.state = {
            select: 0,
        }
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>

                <ScrollView showsVerticalScrollIndicator={false}>

                    <View style={styles.topslider}>
                        <View style={[styles.sliderTextblock, { marginVertical: 15 }]}>
                            <Text style={[ThemeStyle.bodyFontSemiBold, ThemeStyle.profileHeadingTopText]}>your 24k gold delivered doorsteps </Text>
                            <Text style={[ThemeStyle.headingFontMontserratBold, ThemeStyle.profileHeading, { color: "#7340FB" }]}>Get Delivery</Text>
                        </View>
                    </View>

                    <View style={styles.container}>
                        <View style={{ marginVertical: 20 }}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('GoldHowitworks')}
                                style={{ backgroundColor: "#F1EBFF", padding: 20, borderRadius: 10 }}>
                                <Text style={{ textAlign: "center", fontSize: 16, fontWeight: "bold", color: "#7340FB" }}>How it works</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ marginVertical: 15, paddingHorizontal: 5, }}>
                            <Text style={[ThemeStyle.bodyFontSemiBold, { fontSize: 16, color: '#040B4D' }]}>Select the seller to proceed</Text>
                        </View>
                        <View style={styles.brandlogos}>
                            <View style={{ flexDirection: "row", marginVertical: 10, justifyContent: "space-between" }}>
                                <TouchableOpacity
                                    onPress={() => this.setState({ select: 1 })}
                                    style={[styles.logoblock, this.state.select == 1 ? { borderColor: "#7340FB", borderWidth: 2 } : {}]}>
                                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Image style={{ width: 15, height: 15 }} source={require('../../assets/images/kalyan_logo/kalyan_logo.png')}></Image>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.card2heading]}>Tanishq</Text>
                                    </View>
                                    <Text style={[ThemeStyle.bodyFontExtraBlack, styles.cardPrice]}>₹ 3960.94/g</Text>
                                    <Text style={[ThemeStyle.bodyFontSemiBold, styles.cardSubheading]}>24K 99.99% Purity</Text>
                                    {
                                        this.state.select == 1 ?
                                            <View style={styles.activeblockicon}>
                                                <Icon
                                                    name='check'
                                                    color='#fff'
                                                    size={10}
                                                />
                                            </View>
                                            :
                                            <View />
                                    }

                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.setState({ select: 2 })}
                                    style={[styles.logoblock, this.state.select == 2 ? { borderColor: "#7340FB", borderWidth: 2 } : {}]}>
                                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Image style={{ width: 15, height: 15 }} source={require('../../assets/images/kalyan_logo/kalyan_logo.png')}></Image>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.card2heading]}>Kalyan Jewellers</Text>
                                    </View>
                                    <Text style={[ThemeStyle.bodyFontExtraBlack, styles.cardPrice]}>₹ 3960.94/g</Text>
                                    <Text style={[ThemeStyle.bodyFontSemiBold, styles.cardSubheading]}>24K 99.99% Purity</Text>
                                    {
                                        this.state.select == 2 ?
                                            <View style={styles.activeblockicon}>
                                                <Icon
                                                    name='check'
                                                    color='#fff'
                                                    size={10}
                                                />
                                            </View>
                                            :
                                            <View />
                                    }
                                </TouchableOpacity>
                            </View>

                            <View style={{ flexDirection: "row", marginVertical: 10, justifyContent: "space-between" }}>
                                <TouchableOpacity
                                    onPress={() => this.setState({ select: 3 })}
                                    style={[styles.logoblock, this.state.select == 3 ? { borderColor: "#7340FB", borderWidth: 2 } : {}]}>
                                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Image style={{ width: 15, height: 15 }} source={require('../../assets/images/kalyan_logo/kalyan_logo.png')}></Image>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.card2heading]}>Tanishq</Text>
                                    </View>
                                    <Text style={[ThemeStyle.bodyFontExtraBlack, styles.cardPrice]}>₹ 3960.94/g</Text>
                                    <Text style={[ThemeStyle.bodyFontSemiBold, styles.cardSubheading]}>24K 99.99% Purity</Text>
                                    {
                                        this.state.select == 3 ?
                                            <View style={styles.activeblockicon}>
                                                <Icon
                                                    name='check'
                                                    color='#fff'
                                                    size={10}
                                                />
                                            </View>
                                            :
                                            <View />
                                    }

                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.setState({ select: 4 })}
                                    style={[styles.logoblock, this.state.select == 4 ? { borderColor: "#7340FB", borderWidth: 2 } : {}]}>
                                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Image style={{ width: 15, height: 15 }} source={require('../../assets/images/kalyan_logo/kalyan_logo.png')}></Image>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.card2heading]}>Kalyan Jewellers</Text>
                                    </View>
                                    <Text style={[ThemeStyle.bodyFontExtraBlack, styles.cardPrice]}>₹ 3960.94/g</Text>
                                    <Text style={[ThemeStyle.bodyFontSemiBold, styles.cardSubheading]}>24K 99.99% Purity</Text>
                                    {
                                        this.state.select == 4 ?
                                            <View style={styles.activeblockicon}>
                                                <Icon
                                                    name='check'
                                                    color='#fff'
                                                    size={10}
                                                />
                                            </View>
                                            :
                                            <View />
                                    }

                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", margin: 15, }} >
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                            style={[styles.submitbutton, { flex: 0, backgroundColor: "white", borderWidth: 1, borderColor: "#2566FF" }]}>
                            <Icon
                                name='chevron-left'
                                color='#2566FF'
                                size={15}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate("Jewellers")}
                            style={[styles.submitbutton, { alignSelf: "stretch" }]}>
                            <Text style={[ThemeStyle.activebtntext]}>Proceed</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 20,
    },
    topslider: {
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: "#F1EBFF",
        borderBottomLeftRadius: 30,
        marginBottom: 10
    },
    sliderTextblock: {
        padding: 30,
    },
    activeblockicon: {
        backgroundColor: "#7340FB",
        borderTopRightRadius: 10,
        padding: 5,
        position: "absolute",
        right: 0,
        borderBottomLeftRadius: 12
    },
    logoblock: {
        backgroundColor: "#F1EBFF",
        padding: 15,
        borderRadius: 15,
        marginHorizontal: 5,
        flex: 1
    },
    submitbutton: {
        backgroundColor: "#7340FB",
        paddingHorizontal: 18,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        flex: 1,
        marginHorizontal: 8
    },
    card2heading: {
        marginLeft: 5,
        fontSize: 12,
    },
    cardPrice: {
        fontSize: 22,
        color: "#7340FB",
        marginVertical: 10,
    },
    cardSubheading: {
        color: "#00063B",
        fontSize: 12,
    },

});

export default GetDelivery;