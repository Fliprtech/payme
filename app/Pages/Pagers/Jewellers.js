import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native";
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Avatar } from "react-native-elements";
import ThemeStyle from "../../assets/css/ThemeStyle";

class Jewellers extends Component {

    constructor(props) {
        super(props);
        this.state = {
            list: [],
            cardCount: 0,
            selectedItem: [],
            showCartView: false,
        }
    }

    render() {

        this.state.list = [
            {
                name: "0.5g Lotus round coin making charges",
                price: '10.2',
            },
            {
                name: "0.5g Lotus round coin making charges",
                price: '10.2',
            },
            {
                name: "0.5g Lotus round coin making charges",
                price: '10.2',
            },
            {
                name: "0.5g Lotus round coin making charges",
                price: '10.2',
            },
            {
                name: "0.5g Lotus round coin making charges",
                price: '10.2',
            },
            {
                name: "0.5g Lotus round coin making charges",
                price: '10.2',
            },
            {
                name: "0.5g Lotus round coin making charges",
                price: '10.2',
            },
            {
                name: "0.5g Lotus round coin making charges",
                price: '10.2',
            },
            {
                name: "0.5g Lotus round coin making charges",
                price: '10.2',
            },
            {
                name: "0.5g Lotus round coin making charges",
                price: '10.2',
            },
        ];

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>

                <ScrollView style={{ flex: 1, bottom: 0 }} showsVerticalScrollIndicator={false}>

                    <View style={styles.topslider}>
                        <View style={[styles.sliderTextblock, { paddingVertical: 50 }]}>
                            <Text style={[ThemeStyle.bodyFontSemiBold, ThemeStyle.profileHeadingTopText]}>your gold subscription </Text>
                            <Text style={[ThemeStyle.headingFontMontserratBold, ThemeStyle.profileHeading, { color: "#7340FB" }]}>Kalyan Jewellers</Text>
                        </View>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 0 }}>
                            <TouchableOpacity
                                style={{ padding: 10 }}
                                onPress={() => this.props.navigation.goBack()}>
                                <Icon
                                    name='chevron-left'
                                    color='#7340FB'
                                    size={18} />
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{ padding: 10 }}>
                                <Icon
                                    name='heart'
                                    color='#7340FB'
                                    size={18} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.container}>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>

                            {
                                this.state.list.map((item, index) => (
                                    <View style={{ width: '50%', marginBottom: 20 }} key={index}>
                                        <View style={styles.singleproductblock}>
                                            {
                                                (index + 1) % 2 == 0 ?
                                                    <Image style={{ width: "100%", borderRadius: 10 }} source={require('../../assets/images/gold-biscuit/gold-biscuit.png')}></Image>
                                                    :
                                                    <Image style={{ width: "100%", borderRadius: 10 }} source={require('../../assets/images/gold_coin/gold_coin.png')}></Image>
                                            }
                                            <View style={{ marginHorizontal: 5 }}>
                                                <Text style={{ fontSize: 12, lineHeight: 18, marginTop: 10 }}>0.5g Lotus round coin making charges</Text>
                                                <Text style={{ fontSize: 13, fontWeight: "bold", color: "#7340FB", marginVertical: 5 }}>₹ 295</Text>
                                            </View>
                                        </View>
                                    </View>
                                ))
                            }
                        </View>

                        <View style={{ height: 130 }} />

                    </View>
                </ScrollView>


                <View style={[styles.cartbar, { flex: 0, position: 'absolute', bottom: 0, left: 0, right: 0 }]}>

                    <View
                        style={{
                            height: 20, borderBottomRightRadius: 20, borderBottomLeftRadius: 20,
                            backgroundColor: "rgba(255, 255, 255, 1)", paddingHorizontal: 10,
                        }} />

                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                showCartView: !this.state.showCartView,
                            });
                        }}
                        style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                            alignItems: "center",
                            padding: 20,
                            backgroundColor: "#7340FB",
                        }}>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <Text style={{ fontSize: 20, color: "#fff" }}>Cart</Text>
                            {
                                this.state.showCartView ?
                                    <View />
                                    :
                                    <Avatar
                                        rounded
                                        source={require('../../assets/images/cartsmallbiscuit/cartsmallbiscuit.png')}
                                        containerStyle={{ marginHorizontal: 15, }} />
                            }
                            {
                                this.state.showCartView ?
                                    <View />
                                    :
                                    <Avatar
                                        rounded
                                        source={require('../../assets/images/cartsmallcoin/cartsmallcoin.png')} />
                            }
                        </View>
                        <View>
                            {
                                this.state.showCartView ?
                                    <View />
                                    :
                                    <Avatar
                                        overlayContainerStyle={{ backgroundColor: 'white' }}
                                        size="small"
                                        rounded
                                        title="2"
                                        titleStyle={{ color: "#7340FB", fontSize: 16, fontWeight: "bold" }}
                                        activeOpacity={0.7} />

                            }

                        </View>
                    </TouchableOpacity>

                    {
                        this.state.showCartView ?
                            <View style={{ marginHorizontal: 10, padding: 10 }}>
                                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", }}>
                                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Avatar
                                            rounded
                                            source={require('../../assets/images/cartsmallbiscuit/cartsmallbiscuit.png')}
                                        />
                                        <Text style={{ fontSize: 16, color: "#fff", fontWeight: "bold", marginHorizontal: 20 }}>0.5g Lotus round coin</Text>
                                    </View>
                                    <Text style={{ fontSize: 18, color: "#ffffffb3", }}>₹ 295</Text>

                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginVertical: 10 }}>
                                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Avatar
                                            rounded
                                            source={require('../../assets/images/cartsmallbiscuit/cartsmallbiscuit.png')}
                                        />
                                        <Text style={{ fontSize: 16, color: "#fff", fontWeight: "bold", marginHorizontal: 20 }}>5g Lotus react coin</Text>
                                    </View>
                                    <Text style={{ fontSize: 18, color: "#ffffffb3", }}>₹ 620</Text>

                                </View>

                                <Text style={{ fontSize: 14, color: "#ffffff96", marginVertical: 15, textAlign: "center" }}>All orders above ₹2500 qualify for free delivery</Text>

                                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginVertical: 10 }}>
                                    <Text style={{ fontSize: 28, color: "#fff", }}>Total:</Text>
                                    <Text style={{ fontSize: 28, color: "#fff", fontWeight: "bold", }}>₹ 915</Text>
                                </View>
                                <TouchableOpacity style={{ padding: 20, backgroundColor: "#fff", borderRadius: 10, marginVertical: 15 }}>
                                    <Text style={{ fontSize: 14, color: "#7340FB", textAlign: "center", fontWeight: "bold" }}>Proceed</Text>
                                </TouchableOpacity>
                            </View>
                            :
                            <View />

                    }

                </View>

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    topslider: {
        backgroundColor: "#F1EBFF",
        borderBottomLeftRadius: 25,
        padding: 20,
        marginBottom: 20
    },
    sliderTextblock: {
        flex: 1,
        alignItems: "center",
        paddingVertical: 50
    },
    container: {
        marginHorizontal: 20
    },
    singleproductblock: { flex: 1, backgroundColor: "#E3D9FE", paddingHorizontal: 5, paddingTop: 3, marginHorizontal: 8, paddingBottom: 5, borderRadius: 10 },
    cartbar: {
        backgroundColor: "#7340FB",
    }
});

export default Jewellers;