
import React, { Component } from 'react';
import {
    SafeAreaView, TouchableOpacity, View, Image, Text, TextInput,
    StyleSheet, ImageBackground, ScrollView, Alert
} from 'react-native';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';
import DocumentPicker from 'react-native-document-picker';
import { Overlay, ListItem, Button, Icon } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';

class OptionForBank extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showChooser: false,
        }
        this.uploadStatement = this.uploadStatement.bind(this);
        this.uploadStateViaCamera = this.uploadStateViaCamera.bind(this);
    }

    async uploadStatement() {

        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
            });
            console.log('res : ' + JSON.stringify(res));
            console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size
            );
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }

    }

    uploadStateViaCamera() {

        const options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.launchCamera(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };
            }
        });
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={styles.container}>
                    <PagerTopBar percentage={27} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>

                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <BlueLogo />
                                <View
                                    style={[ThemeStyle.text_block]}>
                                    <Text style={[ThemeStyle.processText]}>Please provide us with your bank statement.</Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'flex-start',
                                        flexWrap: 'wrap'
                                    }}>
                                    <TouchableOpacity
                                        onPress={() => this.props.navigation.navigate("SelectBank")}
                                        style={{
                                            marginRight: 5
                                        }}>
                                        <View style={{ paddingHorizontal: 10, paddingVertical: 15, width: 150, backgroundColor: '#2566FF', borderRadius: 10, }}>
                                            <Image
                                                style={{ width: 23, height: 23 }}
                                                source={require('../../assets/images/login_with_bank/login_with_bank.png')} />
                                            <Text style={[ThemeStyle.bodyFontSemiBold, { color: '#FFFFFF', marginTop: 10, }]}>Login with bank</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => this.props.navigation.navigate("UploadBankState")}
                                        // onPress={() => {
                                        //     this.setState({
                                        //         showChooser: true,
                                        //     });
                                        // }}
                                        style={{
                                            marginLeft: 5
                                        }}>
                                        <View style={{ paddingHorizontal: 10, paddingVertical: 14, width: 150, borderColor: "#2566FF", borderWidth: 1.5, borderRadius: 10, }}>
                                            <Image
                                                style={{ width: 23, height: 23 }}
                                                source={require('../../assets/images/cloud_up/cloud_up.png')} />
                                            <Text style={[ThemeStyle.bodyFontSemiBold, { color: '#2566FF', marginTop: 10 }]}>Upload Statement</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
                <Overlay
                    // width={'auto'}
                    height={'auto'}
                    isVisible={this.state.showChooser}>
                    <View>
                        <Text
                            style={{
                                fontSize: 18, fontWeight: '700',
                                paddingHorizontal: 10,
                                paddingVertical: 15
                            }}>
                            Choose Option
                    </Text>
                        <View>
                            <TouchableOpacity
                                onPress={() => {
                                    this.uploadStateViaCamera();
                                }}
                                style={{
                                    paddingHorizontal: 20,
                                    paddingVertical: 15,
                                    fontSize: 16,
                                    backgroundColor: '#f0f0f0',
                                    marginVertical: 10,
                                    marginHorizontal: 10,
                                }}>
                                <Text>
                                    Camera
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.uploadStatement();
                                }}
                                style={{
                                    paddingHorizontal: 20,
                                    paddingVertical: 15,
                                    fontSize: 16,
                                    backgroundColor: '#f0f0f0',
                                    marginVertical: 10,
                                    marginHorizontal: 10,
                                }}>
                                <Text>
                                    Storage
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ alignItems: 'center', paddingHorizontal: 10, paddingVertical: 20 }}>
                            <Button
                                title="Close"
                                type="solid"
                                raised={true}
                                containerStyle={{
                                }}
                                buttonStyle={{
                                    paddingHorizontal: 30,
                                }}
                                style={{
                                    paddingHorizontal: 20,
                                }}
                                onPress={() => {
                                    this.setState({
                                        showChooser: false,
                                    });
                                }} />
                        </View>
                    </View>
                </Overlay>
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, marginHorizontal: 25
    }
});

export default OptionForBank;
