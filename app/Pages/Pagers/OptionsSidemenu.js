
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import BottomBar from '../components/BottomBar';
import ThemeStyle from '../../assets/css/ThemeStyle';

class OpctionsSidemenu extends Component {

    state = {
        text: '',
    };

    render() {
        return (
            <View
                style={{
                    flex: 1,
                }}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{
                        marginHorizontal: 20
                    }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            marginTop: 10,
                            flex: 0,
                        }}>
                        <TouchableOpacity
                            style={{}}
                            onPress={() => this.props.navigation.navigate("Profile")}>
                            <Image source={require('../../assets/images/NoPath/NoPath.png')} style={{
                                width: 70, height: 70
                            }}>
                            </Image>
                        </TouchableOpacity>
                        <View
                            style={{
                                flexDirection: "column",
                                justifyContent: 'center',
                                paddingHorizontal: 10,
                                flex: 1,
                            }}>
                            <Text style={[
                                ThemeStyle.bodyFontRegular,
                                {
                                    color: '#8D8D8D',
                                    fontSize: 12
                                }
                            ]}>Shailesh.bakshi@gmail.com</Text>
                            <Text style={[
                                ThemeStyle.headingFontMontserratBold,
                                {
                                    color: '#00063B',
                                    fontSize: 16
                                }
                            ]}>Shailesh Bakshi</Text>
                        </View>
                        <TouchableOpacity style={{
                            flex: 0
                        }}>
                            <View style={{
                            }}>
                                <View style={{ width: 70, height: 70, backgroundColor: '#FBE7C2', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                    <Icon
                                        reverse
                                        name='ios-sunny'
                                        type='ionicon'
                                        size={40}
                                        color='#F09900' />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: '#F1F2F5',
                        borderRadius: 10,
                        marginTop: 15,
                        paddingLeft: 15,
                        paddingVertical: 5

                    }}>
                        <View
                            style={{
                                flex: 0
                            }}>
                            <Icon
                                reverse
                                name='ios-search'
                                type='ionicon'
                                size={18}
                                color='#000000' />
                        </View>
                        <View style={{
                            flex: 1
                        }}>
                            <TextInput style={{
                                color: 'black',
                                fontSize: 14,
                                fontWeight: 'bold',
                                marginLeft: 10
                            }}
                                placeholder="search for anything here"
                                value={this.state.text}
                                onChangeText={text => this.setState({ text })} />
                        </View>
                    </View>

                    <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate("ReferAndEarn");
                    }}>
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: "space-between",
                                backgroundColor: '#DFE8FE',
                                borderRadius: 10,
                                marginTop: 20,
                                paddingLeft: 20,

                            }}>
                            <View
                                style={{
                                    flexDirection: 'column',
                                    justifyContent: "space-between",
                                }}>
                                <Text style={[styles.cardHeading, ThemeStyle.headingFontMontserratBold, { color: '#00063B', }]}>Refer and Earn</Text>
                                <Text style={[ThemeStyle.bodyFontRegular, styles.cardSubHeading]}>refer to you friend and win</Text>
                                <Text style={{
                                    backgroundColor: 'white',
                                    fontWeight: "bold",
                                    fontSize: 8,
                                    marginBottom: 25,
                                    paddingVertical: 7,
                                    marginRight: 80,
                                    textAlign: 'center',
                                    borderRadius: 10
                                }}>Refer now</Text>
                            </View>
                            <Image source={require('../../assets/images/referandearn/referandearn.png')} style={{
                                width: 150,
                                height: 120
                            }}></Image>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("BuyGold")}>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: "space-between",
                            backgroundColor: '#C6E5FF',
                            borderRadius: 10,
                            marginTop: 20,
                            paddingLeft: 20,

                        }}>
                            <View style={{
                                flexDirection: 'column',
                                justifyContent: "space-between",
                            }}>
                                <Text style={[ThemeStyle.headingFontMontserratBold, styles.cardHeading, { color: "#1C99FF", }]}>Buy Gold</Text>
                                <Text style={[ThemeStyle.bodyFontRegular, styles.cardSubHeading]}>gold from our partner</Text>
                                <Text style={{
                                    backgroundColor: 'white',
                                    fontWeight: "bold",
                                    fontSize: 8,
                                    marginBottom: 25,
                                    paddingVertical: 7,
                                    marginRight: 45,
                                    textAlign: 'center',
                                    borderRadius: 10
                                }}>Buy now</Text>
                            </View>
                            <Image source={require('../../assets/images/gold/gold.png')} style={{
                                height: 110,
                                width: 180
                            }}></Image>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: "space-between",
                            backgroundColor: '#FFCDDE',
                            borderRadius: 10,
                            marginTop: 20,
                            paddingLeft: 20,

                        }}>

                            <View style={{
                                flexDirection: 'column',
                                justifyContent: 'center'
                            }}>
                                <Text style={[ThemeStyle.headingFontMontserratBold, styles.cardHeading, { color: '#FF085A', }]}>Sign Out</Text>
                                <Text style={[ThemeStyle.bodyFontRegular, styles.cardSubHeading]}>exit from the app.</Text>
                            </View>
                            <Image source={require('../../assets/images/signout/signout.png')} style={{
                                width: 180,
                                height: 100
                            }}></Image>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("ContactUs")}>
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: "space-between",
                                backgroundColor: '#FEDFCC',
                                borderRadius: 10,
                                marginTop: 20,
                                paddingLeft: 20,
                            }}>

                            <View
                                style={{
                                    flexDirection: 'column',
                                    justifyContent: 'center'
                                }}>
                                <Text style={[ThemeStyle.headingFontMontserratBold, styles.cardHeading, { color: '#F75F00', }]}>Contact Us</Text>
                                <Text style={[ThemeStyle.bodyFontRegular, styles.cardSubHeading]}>we are here.</Text>
                            </View>
                            <Image
                                source={require('../../assets/images/support/support.png')} style={{
                                    width: 180,
                                    height: 100
                                }} />
                        </View>
                    </TouchableOpacity>

                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginVertical: 20,
                            marginBottom: 40
                        }}>
                        <TouchableOpacity
                            style={{
                                flex: 1,
                                marginRight: 5
                            }}>
                            <View style={{
                                flexDirection: 'column',
                                backgroundColor: '#E3D9FE',
                                paddingVertical: 30,
                                paddingHorizontal: 15,
                                borderRadius: 10
                            }}>
                                <Text
                                    style={[
                                        ThemeStyle.headingFontMontserratBold,
                                        {
                                            color: '#7340FB',
                                            fontSize: 12,
                                        }
                                    ]}>Terms & Conditions</Text>
                                <Text
                                    style={[
                                        ThemeStyle.bodyFontRegular,
                                        {
                                            color: '#00063B',
                                            fontSize: 11,
                                        }
                                    ]}>read our conditions.</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{
                                flex: 1
                            }}>
                            <View
                                style={{
                                    flexDirection: 'column',
                                    backgroundColor: '#D2F1E5',
                                    paddingVertical: 30,
                                    paddingHorizontal: 15,
                                    borderRadius: 10
                                }}>
                                <Text
                                    style={[
                                        ThemeStyle.headingFontMontserratBold,
                                        {
                                            color: '#20B77E',
                                            fontSize: 12,
                                        }
                                    ]}>Privacy Policy</Text>
                                <Text
                                    style={[
                                        ThemeStyle.bodyFontRegular,
                                        {
                                            color: '#00063B',
                                            fontSize: 11,
                                        }
                                    ]}>read our privacy policy.</Text>
                            </View>
                        </TouchableOpacity>


                    </View>

                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#20B77E",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    }, greendot: {
        padding: 3,
        backgroundColor: "#20b77e",
        paddingHorizontal: 7,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    orangedot: {
        backgroundColor: "#f75f00",
        padding: 3,
        paddingHorizontal: 7

    },
    reddot: {
        backgroundColor: "#ff085a",
        padding: 3,
        paddingHorizontal: 7

    },
    bluedot: {
        backgroundColor: "#7340fb",
        padding: 3,
        paddingHorizontal: 7,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    },
    cardHeading: {
        fontSize: 16,
        marginBottom: 0,
        marginTop: 15
    },
    cardSubHeading: {
        color: '#00063B',
        fontSize: 12,
        marginBottom: 10,
        marginTop: 3,
    },
});

export default OpctionsSidemenu;
