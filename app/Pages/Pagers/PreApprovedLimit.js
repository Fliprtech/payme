
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';

class PreApprovedLimit extends Component {


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={[ThemeStyle.progressContainer]}>
                    <PagerTopBar percentage={90} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <View style={{
                                    justifyContent: "flex-end",
                                    flex: 1,
                                    flexDirection: "column",
                                }}>
                                    <BlueLogo />
                                    <View style={[ThemeStyle.text_block]}>
                                        <Text style={[ThemeStyle.processText, { fontSize: 24, marginBottom: 5, }]}>{'\u20B9'} 4500</Text>
                                        <Text style={[ThemeStyle.processText]}>your approved limit</Text>
                                    </View>
                                    <View style={[ThemeStyle.text_block]}>
                                        <Text style={[ThemeStyle.processText]}>Congratulations!You can now apply for loan and get money immediately.</Text>
                                    </View>

                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'space-between',
                                        }}>
                                        <TouchableOpacity
                                            // onPress={() => this.props.navigation.navigate("SelectAmount")}
                                            onPress={() => this.props.navigation.navigate("SelectEMILoanOption")}
                                            style={[ThemeStyle.blueBtn, { flexGrow: 1, marginRight: 5, }]}>
                                            <Text style={[ThemeStyle.activebtntext]}>Go ahead</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                            onPress={() => {
                                                Alert.alert("OOPS!", "We don't have screen for this. Please provide screen number in XD")
                                            }}
                                            style={[ThemeStyle.whiteBtn, { flexGrow: 1, marginLeft: 5, }]}>
                                            <Text style={[ThemeStyle.borderbtntext]}>Need more</Text>
                                        </TouchableOpacity>
                                    </View>

                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

export default PreApprovedLimit;
