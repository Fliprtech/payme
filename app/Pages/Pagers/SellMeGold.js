import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native";
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Input } from "react-native-elements";
import ThemeStyle from "../../assets/css/ThemeStyle";

class SellMeGold extends Component {

    constructor(props) {
        super(props);
        this.state = {
            select: 0,
        }
    }

    render() {

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>

                <View style={styles.topslider}>
                    <View style={[styles.sliderTextblock, { marginVertical: 25 }]}>
                        <Text style={[ThemeStyle.bodyFontSemiBold, ThemeStyle.profileHeadingTopText]}>Know more about us </Text>
                        <Text style={[ThemeStyle.headingFontMontserratBold, ThemeStyle.profileHeading, { color: "#20B77E" }]}>Sell PayMe Gold</Text>
                    </View>
                </View>

                <ScrollView>
                    <View style={[styles.container]}>

                        <View style={{ marginVertical: 25 }}>
                            <Text style={[ThemeStyle.headingFontMontserratBold, { fontSize: 18, color: '#00063B', marginVertical: 10 }]}>Gold locker balance</Text>
                            <View style={{ flexDirection: "row" }}>
                                <Text style={{ fontSize: 12 }}>Live Sell Price</Text>
                                <Text style={{ fontSize: 14, fontWeight: "bold", marginHorizontal: 7, color: "#20B77E" }}>₹ 3960.94/g</Text>
                            </View>
                        </View>

                        <View style={{ marginTop: 10 }}>
                            <Text style={[ThemeStyle.bodyFontSemiBold, { fontSize: 14, color: '#040B4D' }]}>Select from the option to go further.</Text>
                        </View>

                        <View style={styles.brandlogos}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <TouchableOpacity
                                    onPress={() => this.setState({ select: 1 })}
                                    style={[styles.logoblock, this.state.select == 1 ? { borderColor: "#20B77E", borderWidth: 2 } : {}]}>
                                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Image style={{ width: 15, height: 15 }} source={require('../../assets/images/kalyan_logo/kalyan_logo.png')}></Image>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.card2heading]}>Tanishq</Text>
                                    </View>
                                    <Text style={[ThemeStyle.bodyFontExtraBlack, styles.cardPrice]}>₹ 3960.94/g</Text>
                                    <Text style={[ThemeStyle.bodyFontSemiBold, styles.cardSubheading]}>24K 99.99% Purity</Text>
                                    {
                                        this.state.select == 1 ?
                                            <View style={styles.activeblockicon}>
                                                <Icon
                                                    name='check'
                                                    color='#fff'
                                                    size={10}
                                                />
                                            </View>
                                            :
                                            <View />
                                    }

                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.setState({ select: 2 })}
                                    style={[styles.logoblock, this.state.select == 2 ? { borderColor: "#20B77E", borderWidth: 2 } : {}]}>
                                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Image style={{ width: 15, height: 15 }} source={require('../../assets/images/kalyan_logo/kalyan_logo.png')}></Image>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.card2heading]}>Kalyan Jewellers</Text>
                                    </View>
                                    <Text style={[ThemeStyle.bodyFontExtraBlack, styles.cardPrice]}>₹ 3960.94/g</Text>
                                    <Text style={[ThemeStyle.bodyFontSemiBold, styles.cardSubheading]}>24K 99.99% Purity</Text>
                                    {
                                        this.state.select == 2 ?
                                            <View style={styles.activeblockicon}>
                                                <Icon
                                                    name='check'
                                                    color='#fff'
                                                    size={10}
                                                />
                                            </View>
                                            :
                                            <View />
                                    }
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.setState({ select: 3 })}
                                    style={[styles.logoblock, this.state.select == 3 ? { borderColor: "#20B77E", borderWidth: 2 } : {}]}>
                                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Image style={{ width: 15, height: 15 }} source={require('../../assets/images/bitcoin/bitcoin.png')}></Image>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.card2heading]}>Malabaar</Text>
                                    </View>
                                    <Text style={[ThemeStyle.bodyFontExtraBlack, styles.cardPrice]}>₹ 3960.94/g</Text>
                                    <Text style={[ThemeStyle.bodyFontSemiBold, styles.cardSubheading]}>24K 99.99% Purity</Text>
                                    {
                                        this.state.select == 3 ?
                                            <View style={styles.activeblockicon}>
                                                <Icon
                                                    name='check'
                                                    color='#fff'
                                                    size={10}
                                                />
                                            </View>
                                            :
                                            <View />
                                    }
                                </TouchableOpacity>

                            </ScrollView>
                        </View>

                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <View style={styles.buyamoutblock}>
                                <View style={styles.singleblock}>
                                    <View style={styles.amountblockhead}>
                                        <Text style={[ThemeStyle.headingFontMontserratBold, styles.largeCardHeading]}>Sell in amount</Text>
                                    </View>
                                    <View style={styles.amountinputblock}>
                                        <Input
                                            inputContainerStyle={{
                                                borderBottomWidth: 0,
                                                padding: 10
                                            }}
                                            inputStyle={{
                                                fontSize: 28,
                                                fontWeight: "700",
                                                marginLeft: 5,
                                            }}
                                            placeholderTextColor="#ccb8ca"
                                            placeholder='enter amount'
                                            leftIcon={
                                                <Image
                                                    style={{ width: 25, height: 25, resizeMode: 'contain' }}
                                                    source={require('../../assets/images/rupee-indian/rupee-indian.png')} />
                                            }
                                            leftIconContainerStyle={{
                                                marginLeft: 0,
                                            }}
                                        />
                                    </View>

                                </View>
                                <View style={[styles.singleblock, { backgroundColor: "#F1EBFF" }]}>
                                    <View style={styles.amountblockhead}>
                                        <Text style={[ThemeStyle.headingFontMontserratBold, styles.largeCardHeading]}>Sell in grams</Text>
                                    </View>
                                    <View style={styles.amountinputblock}>
                                        <Input
                                            inputContainerStyle={{
                                                borderBottomWidth: 0,
                                                padding: 10
                                            }}
                                            inputStyle={{
                                                fontSize: 28,
                                                fontWeight: "700",
                                                marginLeft: 5,
                                            }}
                                            placeholderTextColor="#c9c5de"
                                            placeholder='enter grams'
                                            leftIcon={

                                                <Text style={{ fontSize: 26, fontWeight: 'bold' }}>gm</Text>
                                            }
                                            leftIconContainerStyle={{
                                                marginLeft: 0,
                                            }}
                                        />
                                    </View>

                                </View>

                            </View>

                        </ScrollView>


                    </View>
                </ScrollView>

                <View style={{ flexDirection: "row", margin: 15, }} >
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={[styles.submitbutton, { flex: 0, backgroundColor: "white", borderWidth: 1, borderColor: "#2566FF" }]}>
                        <Icon
                            name='chevron-left'
                            color='#2566FF'
                            size={15} />
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.submitbutton, { alignSelf: "stretch", backgroundColor: "#20B77E" }]}>
                        <Text style={[ThemeStyle.activebtntext]}>Sell Gold</Text>
                    </TouchableOpacity>

                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 20,
        flex: 1,
    },
    topslider: {
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: "#E8F8F2",
        borderBottomLeftRadius: 30,
    },
    sliderTextblock: {
        padding: 30,
    },
    row: {
        flexDirection: "row",
        marginVertical: 10,
    },
    contentbox: {
        flex: 0,
        marginHorizontal: 5,
        borderRadius: 15,
        width: 100,
        padding: 10
    },
    submitbutton: {
        backgroundColor: "#2566FF",
        paddingHorizontal: 18,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        flex: 1,
        marginHorizontal: 8
    },
    brandlogos: {
        marginVertical: 30
    },
    buyamoutblock: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    singleAmountblock: {
        backgroundColor: "#E8F8F2",
        width: 150
    },
    buyamoutblock: {
        flexDirection: "row",
        marginVertical: 15
    },
    amountblockhead: {
        padding: 20,
        borderBottomColor: "white",
        borderBottomWidth: 1,

    },
    singleblock: {
        backgroundColor: "#FFE5EE",
        borderRadius: 10,
        marginHorizontal: 5,
        width: 270
    },
    amountinputblock: {

    },
    boxgstcontent: {
        paddingHorizontal: 20,
        paddingBottom: 25,
        paddingTop: 10
    },
    activeblockicon: {
        backgroundColor: "#20B77E",
        borderTopRightRadius: 10,
        padding: 5,
        position: "absolute",
        right: 0,
        borderBottomLeftRadius: 12
    },
    logoblock: {
        backgroundColor: "#E8F8F2",
        padding: 15,
        paddingHorizontal: 25,
        borderRadius: 15,
        marginHorizontal: 5,
    },
    cardSmallHeading: {
        fontSize: 13,
        fontWeight: "bold",
        marginTop: 15,
        color: "#FFFFFF",
    },
    card2heading: {
        marginLeft: 5,
        fontSize: 12,
    },
    cardPrice: {
        fontSize: 22,
        color: "#20B77E",
        marginVertical: 10,
    },
    cardSubheading: {
        color: "#00063B",
        fontSize: 12,
    },
    largeCardHeading: {
        fontSize: 18,
        color: '#00063B',
    }

});

export default SellMeGold;