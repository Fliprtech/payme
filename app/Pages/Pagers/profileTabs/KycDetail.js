import React, { Component } from "react";
import {
    SafeAreaView, TouchableOpacity, View, Image, Text, TextInput,
    StyleSheet, ImageBackground, ScrollView, Animated
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ThemeStyle from "../../../assets/css/ThemeStyle";
import ProfileTabTopBar from "../../components/ProfileTabTopBar";

class KycDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstname: '',
            lastname: '',
            fathersname: '',
            dob: '',
            itemPosition: 0,
            show: false,
            education: ''
        }
        this.move_down = this.move_down.bind(this);
    }

    move_down() {
        Animated.timing(this.state.itemPosition, {
            toValue: 100,
            duration: 100
        }).start(() => {

        });
    }

    render() {

        return (
            <View style={{}}>
                <TouchableOpacity
                    onPress={() => {
                        this.setState({
                            show: !this.state.show,
                        })
                    }}
                    style={{ marginTop: 20 }}>
                    <View style={{ backgroundColor: '#F1EBFF', borderRadius: 10, paddingBottom: 0 }}>
                        <ProfileTabTopBar {...this.props} barColor={'#7340FB'} progress={30} />
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                paddingHorizontal: 20
                            }}>
                            <View
                                style={{
                                    flexDirection: "row",
                                    paddingVertical: 20
                                }}>
                                <Image
                                    source={require('../../../assets/images/layers_2/layers_2.png')}
                                    style={[ThemeStyle.profileTabIcon]} />
                                <Text style={[ThemeStyle.profileHeadings,]}>
                                    KYC details
                                </Text>
                            </View>
                            {
                                this.state.show ?
                                    <Icon
                                        reverse
                                        name='minus'
                                        type='font-awesome'
                                        size={15}
                                        color='#000000' />
                                    :
                                    <Icon
                                        reverse
                                        name='plus'
                                        type='font-awesome'
                                        size={15}
                                        color='#000000' />
                            }

                        </View>
                    </View>
                </TouchableOpacity>

                {
                    this.state.show ?
                        <View style={{ backgroundColor: '#F1EBFF', }}>
                            <View style={{
                                borderRadius: 10,
                                paddingHorizontal: 15,
                                paddingVertical: 10,
                                borderTopWidth: 2,
                                borderTopColor: 'white',
                            }}>
                                <View style={{ height: 10 }} />
                                <View>
                                    <Text style={{
                                        color: '#000000',
                                        fontSize: 14,
                                        alignSelf: "flex-start",
                                        marginBottom: 10,

                                    }}>PAN Card</Text>

                                    <View style={{
                                        backgroundColor: '#ffffff',
                                        borderRadius: 10,
                                        padding: 4,
                                        marginBottom: 20

                                    }}>
                                        <View>
                                            <TextInput style={[styles.textInput]}
                                                placeholderTextColor={"#c2c2c2"}
                                                placeholder="enter your PAN number here"
                                                value={this.state.pancard}
                                                onChangeText={pancard => this.setState({ pancard })} /></View>
                                    </View>
                                </View>
                                <Text style={{
                                    color: '#000000',
                                    fontSize: 14,
                                    alignSelf: "flex-start",
                                    marginBottom: 10,
                                }}>Education</Text>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        marginBottom: 10,
                                        backgroundColor: '#E3D9FE',
                                        borderRadius: 5,
                                    }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                education: 'under_graduate',
                                            });
                                        }}
                                        style={[styles.educationBlock]}>
                                        <View>
                                            <Text style={[styles.educationBlockText, { color: this.state.education == 'under_graduate' ? "#7340FB" : "#00063B" }]}>Under Graduate</Text>
                                            <View style={[this.state.education == 'under_graduate' ? styles.actibeTab : styles.inactiveTab]} />
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                education: 'graduate',
                                            });
                                        }}
                                        style={[styles.educationBlock]}>
                                        <View>
                                            <Text style={[styles.educationBlockText, { color: this.state.education == 'graduate' ? "#7340FB" : "#00063B" }]}>Graduate</Text>
                                            <View style={[this.state.education == 'graduate' ? styles.actibeTab : styles.inactiveTab]} />
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                education: 'post_graduate',
                                            });
                                        }}
                                        style={[styles.educationBlock]}>
                                        <View>
                                            <Text style={[styles.educationBlockText, { color: this.state.education == 'post_graduate' ? "#7340FB" : "#00063B" }]}>Post Graduate</Text>
                                            <View style={[this.state.education == 'post_graduate' ? styles.actibeTab : styles.inactiveTab]} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <View>
                                    <Text style={{
                                        color: '#000000',
                                        fontSize: 14,
                                        alignSelf: "flex-start",
                                        marginBottom: 10,

                                    }}>Reference name</Text>

                                    <View style={{
                                        backgroundColor: '#ffffff',
                                        borderRadius: 10,
                                        padding: 4,
                                        marginBottom: 20

                                    }}>
                                        <View>
                                            <TextInput style={[styles.textInput]}
                                                placeholderTextColor={"#c2c2c2"}
                                                placeholder="enter your reference name here"
                                                value={this.state.referencename}
                                                onChangeText={referencename => this.setState({ referencename })} /></View>
                                    </View>
                                </View>
                                <View>
                                    <Text style={{
                                        color: '#000000',
                                        fontSize: 14,
                                        alignSelf: "flex-start",
                                        marginBottom: 10,

                                    }}>Reference phone</Text>

                                    <View style={{
                                        backgroundColor: '#ffffff',
                                        borderRadius: 10,
                                        padding: 4,
                                        marginBottom: 20

                                    }}>
                                        <View>
                                            <TextInput style={[styles.textInput]}
                                                placeholderTextColor={"#c2c2c2"}
                                                placeholder="enter your reference phone"
                                                value={this.state.referencephone}
                                                keyboardType="number-pad"
                                                onChangeText={referencephone => this.setState({ referencephone })} /></View>
                                    </View>
                                </View>

                            </View>
                            <TouchableOpacity style={{ marginTop: 0 }}>
                                <View style={{ alignItems: 'center', paddingRight: 30, paddingVertical: 20, backgroundColor: '#7340FB', borderBottomLeftRadius: 10, borderBottomEndRadius: 10 }}>

                                    <Text style={{ color: '#ffffff' }}>Save changes</Text>

                                </View>
                            </TouchableOpacity>
                        </View>
                        :
                        <View />

                }

            </View>
        );
    }

}

const styles = StyleSheet.create({
    actibeTab: {
        paddingVertical: 2,
        backgroundColor: '#7340FB',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    inactiveTab: {
        paddingVertical: 2,
        backgroundColor: 'rgba(0,0,0,0)',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    educationBlock: {
        paddingVertical: 0,
        flexGrow: 1,
        alignItems: 'stretch',
        paddingHorizontal: 10,
    },
    educationBlockText: {
        color: '#20B77E',
        textAlign: 'center',
        marginTop: 3,
        paddingVertical: 15,
        fontFamily: 'Nunito-Bold',
        fontSize: 13,
    },
    textInput: {
        color: '#00063B',
        fontSize: 14,
        fontWeight: 'bold',
    },
});

export default KycDetail;