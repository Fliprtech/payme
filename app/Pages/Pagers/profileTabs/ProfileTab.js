import React, { Component } from "react";
import {
    SafeAreaView, TouchableOpacity, View, Image, Text, TextInput,
    StyleSheet, ImageBackground, ScrollView, Animated
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ThemeStyle from "../../../assets/css/ThemeStyle";
import ProfileTabTopBar from "../../components/ProfileTabTopBar";

class ProfileTab extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstname: '',
            lastname: '',
            fathersname: '',
            dob: '',
            itemPosition: 0,
            show: false,
            gender: ''
        }
        this.move_down = this.move_down.bind(this);
    }

    move_down() {
        Animated.timing(this.state.itemPosition, {
            toValue: 100,
            duration: 100
        }).start(() => {

        });
    }

    render() {

        return (
            <View style={{}}>
                <TouchableOpacity
                    onPress={() => {
                        this.setState({
                            show: !this.state.show,
                        })
                    }}
                    style={{ marginTop: 20 }}>
                    <View style={{ backgroundColor: '#E8F8F2', borderRadius: 10, paddingBottom: 0 }}>
                        <ProfileTabTopBar {...this.props} barColor={'#20B77E'} progress={70} />
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                paddingHorizontal: 20
                            }}>
                            <View
                                style={{
                                    flexDirection: "row",
                                    paddingVertical: 20
                                }}>
                                <Image
                                    source={require('../../../assets/images/profile_details/profile_details.png')}
                                    style={[ThemeStyle.profileTabIcon]} />
                                <Text style={[ThemeStyle.profileHeadings,]}>Profile details</Text>
                            </View>
                            {
                                this.state.show ?
                                    <Icon
                                        reverse
                                        name='minus'
                                        type='font-awesome'
                                        size={15}
                                        color='#000000' />
                                    :
                                    <Icon
                                        reverse
                                        name='plus'
                                        type='font-awesome'
                                        size={15}
                                        color='#000000' />

                            }

                        </View>
                    </View>
                </TouchableOpacity>

                {
                    this.state.show ?
                        <View style={{ backgroundColor: '#E8F8F2', }}>
                            <View
                                style={{
                                    borderRadius: 10,
                                    paddingHorizontal: 15,
                                    paddingVertical: 10,
                                    borderTopWidth: 2,
                                    borderTopColor: 'white'
                                }}>
                                <View style={{ height: 10 }} />
                                <View>
                                    <Text style={{
                                        color: '#000000',
                                        fontSize: 14,
                                        alignSelf: "flex-start",
                                        marginBottom: 10,

                                    }}>First name</Text>

                                    <View style={{
                                        backgroundColor: '#ffffff',
                                        borderRadius: 10,
                                        padding: 4,
                                        marginBottom: 20

                                    }}>
                                        <View>
                                            <TextInput style={[styles.textInput]}
                                                placeholderTextColor={"#c2c2c2"}
                                                placeholder="enter your first name here"
                                                value={this.state.firstname}
                                                onChangeText={firstname => this.setState({ firstname })} />
                                        </View>
                                    </View>
                                </View>
                                <View>
                                    <Text style={{
                                        color: '#000000',
                                        fontSize: 14,
                                        alignSelf: "flex-start",
                                        marginBottom: 10,

                                    }}>Last name</Text>

                                    <View style={{
                                        backgroundColor: '#ffffff',
                                        borderRadius: 10,
                                        padding: 4,
                                        marginBottom: 20

                                    }}>
                                        <View>
                                            <TextInput style={[styles.textInput]}
                                                placeholder="enter your last name here"
                                                placeholderTextColor={"#c2c2c2"}
                                                value={this.state.lastname}
                                                onChangeText={lastname => this.setState({ lastname })} />
                                        </View>
                                    </View>
                                </View>
                                <View>
                                    <Text style={{
                                        color: '#000000',
                                        fontSize: 14,
                                        alignSelf: "flex-start",
                                        marginBottom: 10,

                                    }}>Father's name</Text>

                                    <View style={{
                                        backgroundColor: '#ffffff',
                                        borderRadius: 10,
                                        padding: 4,
                                        marginBottom: 20

                                    }}>
                                        <View>
                                            <TextInput style={[styles.textInput]}
                                                placeholder="enter your father's name here"
                                                placeholderTextColor={"#c2c2c2"}
                                                value={this.state.fathersname}
                                                onChangeText={fathersname => this.setState({ fathersname })} />
                                        </View>
                                    </View>
                                </View>
                                <View>
                                    <Text style={{
                                        color: '#000000',
                                        fontSize: 14,
                                        alignSelf: "flex-start",
                                        marginBottom: 10,

                                    }}>Date of birth</Text>

                                    <View style={{
                                        backgroundColor: '#ffffff',
                                        borderRadius: 10,
                                        padding: 4,
                                        marginBottom: 20

                                    }}>
                                        <View>
                                            <TextInput style={[styles.textInput]}
                                                placeholder="select your date of birth"
                                                placeholderTextColor={"#c2c2c2"}
                                                value={this.state.dob}
                                                keyboardType="number-pad"
                                                onChangeText={dob => this.setState({ dob })} />
                                        </View>
                                    </View>
                                </View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        marginBottom: 10,
                                        backgroundColor: '#D2F1E5',
                                        borderRadius: 5,
                                    }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                gender: 'male',
                                            });
                                        }}
                                        style={[styles.genderBlock]}>
                                        <View>
                                            <Text style={[styles.genderText, { color: this.state.gender == 'male' ? "#20B77E" : "#00063B" }]}>male</Text>
                                            <View style={[this.state.gender == 'male' ? styles.activeGender : styles.inactiveGender]} />
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                gender: 'female',
                                            });
                                        }}
                                        style={[
                                            styles.genderBlock,
                                            {
                                                borderLeftColor: 'white',
                                                borderLeftWidth: 1,
                                                borderRightColor: 'white',
                                                borderRightWidth: 1,
                                            }
                                        ]}>
                                        <View>
                                            <Text style={[styles.genderText, { color: this.state.gender == 'female' ? "#20B77E" : "#00063B" }]}>female</Text>
                                            <View style={[this.state.gender == 'female' ? styles.activeGender : styles.inactiveGender]} />
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                gender: 'other',
                                            });
                                        }}
                                        style={[styles.genderBlock]}>
                                        <View>
                                            <Text style={[styles.genderText, { color: this.state.gender == 'other' ? "#20B77E" : "#00063B" }]}>others</Text>
                                            <View style={[this.state.gender == 'other' ? styles.activeGender : styles.inactiveGender]} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <TouchableOpacity style={{ marginTop: 0 }}>
                                <View style={{ alignItems: 'center', paddingRight: 30, paddingVertical: 20, backgroundColor: '#20B77E', borderBottomLeftRadius: 10, borderBottomEndRadius: 10 }}>
                                    <Text style={{ color: '#ffffff' }}>Save changes</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        :
                        <View />
                }

            </View>
        );
    }

}

const styles = StyleSheet.create({
    activeGender: {
        paddingVertical: 2,
        backgroundColor: '#20B77E',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    inactiveGender: {
        paddingVertical: 2,
        backgroundColor: 'rgba(0,0,0,0)',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    genderBlock: {
        paddingVertical: 0,
        flexGrow: 1,
        alignItems: 'stretch',
        paddingHorizontal: 10,
    },
    genderText: {
        color: '#20B77E',
        textAlign: 'center',
        marginTop: 3,
        paddingVertical: 15,
        fontFamily: 'Nunito-Bold',
        fontSize: 14,
    },
    textInput: {
        color: '#00063B',
        fontSize: 14,
        fontWeight: 'bold'
    },
});

export default ProfileTab;