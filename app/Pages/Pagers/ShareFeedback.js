import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { RadioButton } from 'react-native-paper';
import LoanDirbursed from './LoanDirbursed';
import { Rating, AirbnbRating } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import ThemeStyle from '../../assets/css/ThemeStyle';

class ShareFeedback extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ratting: 0,
        }
        this.changeRating = this.changeRating.bind(this);
    }

    changeRating(value) {
        this.setState({
            ratting: value,
        });
    }


    render() {

        return (
            <SafeAreaView style={{ flex: 1, }}>
                <View style={{ flex: 1 }}>
                    <LoanDirbursed />
                </View>

                <View style={[styles.upperbox, { position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }]} >
                    <View>
                        <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                            <View style={styles.container} >
                                <Text style={[ThemeStyle.overlayHeading, ThemeStyle.headingFontMontserratBold]} >Share your valuable feedback!</Text>
                                <Text style={[ThemeStyle.overlaySubHeading, ThemeStyle.bodyFontRegular]}>that will help us improve.</Text>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        paddingBottom: 10,
                                    }}>
                                    <TouchableOpacity onPress={() => this.changeRating(1)}>

                                        {
                                            this.state.ratting >= 1 ?
                                                <Image style={[styles.rattingStar]} source={require('../../assets/images/star_1/star_2.png')} />
                                                :
                                                <Image style={[styles.rattingStar]} source={require('../../assets/images/star_2/star_2.png')} />
                                        }

                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.changeRating(2)}>
                                        {
                                            this.state.ratting >= 2 ?
                                                <Image style={[styles.rattingStar]} source={require('../../assets/images/star_1/star_2.png')} />
                                                :
                                                <Image style={[styles.rattingStar]} source={require('../../assets/images/star_2/star_2.png')} />
                                        }
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.changeRating(3)}>
                                        {
                                            this.state.ratting >= 3 ?
                                                <Image style={[styles.rattingStar]} source={require('../../assets/images/star_1/star_2.png')} />
                                                :
                                                <Image style={[styles.rattingStar]} source={require('../../assets/images/star_2/star_2.png')} />
                                        }
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.changeRating(4)}>
                                        {
                                            this.state.ratting >= 4 ?
                                                <Image style={[styles.rattingStar]} source={require('../../assets/images/star_1/star_2.png')} />
                                                :
                                                <Image style={[styles.rattingStar]} source={require('../../assets/images/star_2/star_2.png')} />
                                        }
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.changeRating(5)}>
                                        {
                                            this.state.ratting >= 5 ?
                                                <Image style={[styles.rattingStar]} source={require('../../assets/images/star_1/star_2.png')} />
                                                :
                                                <Image style={[styles.rattingStar]} source={require('../../assets/images/star_2/star_2.png')} />
                                        }
                                    </TouchableOpacity>

                                </View>
                                <View style={[styles.input_block]}>
                                    <Input
                                        inputContainerStyle={{
                                            borderBottomWidth: 0,
                                            backgroundColor: "#F1F2F5",
                                            borderRadius: 10,
                                            height: 200,
                                        }}
                                        containerStyle={{
                                            padding: 0,
                                            paddingHorizontal: 0,
                                        }}
                                        inputStyle={{
                                            fontSize: 15,
                                            fontWeight: "700",
                                            color: "black",
                                            margin: 15,
                                            padding: 0,
                                        }}
                                        style={{

                                        }}
                                        placeholderTextColor="#c5c6d3"
                                        placeholder='write your valuable here.'
                                        numberOfLines={10}
                                        showsVerticalScrollIndicator={true}
                                        textAlignVertical={'top'}
                                        scrollEnabled={true}
                                        multiline={true} />
                                </View>

                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate("RepaymentAccount")}
                                    style={styles.submitbutton}>
                                    <Text style={[ThemeStyle.activebtntext]}>Send feedback</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </View>

            </SafeAreaView>

        );
    }
}


const styles = StyleSheet.create({
    upperbox: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#00000070",
    },
    container: {
        padding: 30,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    },
    activebutton: {
        backgroundColor: "#E8EFFF",
        padding: 12,
        marginVertical: 10,
        paddingHorizontal: 13,
        borderRadius: 9,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    button: {
        backgroundColor: "#F1F2F5",
        paddingHorizontal: 13,
        padding: 12,
        marginVertical: 8,
        borderRadius: 9,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    buttoncolor: {
        color: "#00063B"
    },
    buttonactivecolor: {
        color: "#2566FF",

    },
    submitbutton: {
        backgroundColor: "#2566FF",
        paddingVertical: 20,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
    },
    input_block: {
        marginVertical: 8
    },
    rating: {
        flex: 0,
        marginRight: 170,
        marginVertical: 10,
    },
    rattingStar: {
        width: 20,
        height: 19,
        marginRight: 7
    },

});

export default ShareFeedback;