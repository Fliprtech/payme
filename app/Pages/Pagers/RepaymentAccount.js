
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView, Clipboard, ToastAndroid } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';
import AsyncStorage from '@react-native-community/async-storage';
import UploadRepaymentScreenshot from './UploadRepaymentScreenshot';

class RepaymentAccount extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: "",
            line1: "",
            line2: "",
            line3: "",
            line4: "",
            uploadScreenshot: false,
        }
        this.copyText = this.copyText.bind(this);
    }

    componentDidMount() {
        this.loadData();
    }

    async loadData() {
        let bankDetail_string = await AsyncStorage.getItem("accountDetail");
        let bankDetail = JSON.parse(bankDetail_string);
        this.setState({
            line1: bankDetail.accountHolderName,
            line2: bankDetail.bankName,
            line3: bankDetail.accountNumber,
            line4: bankDetail.accountIFSC,
        });
    }


    copyText(string) {
        Clipboard.setString(string);
        ToastAndroid.showWithGravity("Text copied", ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>

                <View style={styles.container}>
                    <PagerTopBar percentage={100} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <View>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                <BlueLogo />
                                <View style={[ThemeStyle.text_block]}>
                                    <Text style={[ThemeStyle.processText]}>Here is the repayment details.</Text>
                                </View>

                                <View
                                    style={{
                                        marginBottom: 10,
                                        marginRight: 20,
                                        backgroundColor: '#EEF8FF',
                                        padding: 15,
                                        borderRadius: 15,
                                    }}>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'space-between',
                                            backgroundColor: '#ffffff',
                                            borderRadius: 10,
                                            padding: 5,
                                            paddingLeft: 15,
                                            marginBottom: 10,
                                        }}>
                                        <View>
                                            <Text style={[styles.inputText, ThemeStyle.bodyFontBold]}>{this.state.line1}</Text>
                                        </View>

                                        <TouchableOpacity onPress={() => this.copyText(this.state.line1)}>
                                            <View>
                                                <View style={{ width: 50, height: 50, backgroundColor: '#EEF8FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                                    <Image style={[styles.copyIcon]} source={require('../../assets/images/copy_1/copy_1.png')} />
                                                </View>
                                            </View>
                                        </TouchableOpacity>

                                    </View>
                                    <View style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'space-between',
                                        backgroundColor: '#ffffff',
                                        borderRadius: 10,
                                        padding: 5,
                                        paddingLeft: 15,
                                        marginBottom: 10

                                    }}>
                                        <View>
                                            <Text style={[styles.inputText, ThemeStyle.bodyFontBold]}>{this.state.line2}</Text>
                                        </View>

                                        <TouchableOpacity onPress={() => this.copyText(this.state.line2)}>
                                            <View>
                                                <View style={{ width: 50, height: 50, backgroundColor: '#EEF8FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                                    <Image style={[styles.copyIcon]} source={require('../../assets/images/copy_1/copy_1.png')} />
                                                </View>
                                            </View>
                                        </TouchableOpacity>

                                    </View>
                                    <View style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'space-between',
                                        backgroundColor: '#ffffff',
                                        borderRadius: 10,
                                        padding: 5,
                                        paddingLeft: 15,
                                        marginBottom: 10

                                    }}>
                                        <View>
                                            <Text style={[styles.inputText, ThemeStyle.bodyFontBold]}>{this.state.line3}</Text>
                                        </View>

                                        <TouchableOpacity onPress={() => this.copyText(this.state.line3)}>
                                            <View>
                                                <View style={{ width: 50, height: 50, backgroundColor: '#EEF8FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                                    <Image style={[styles.copyIcon]} source={require('../../assets/images/copy_1/copy_1.png')} />
                                                </View>
                                            </View>
                                        </TouchableOpacity>

                                    </View>
                                    <View style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'space-between',
                                        backgroundColor: '#ffffff',
                                        borderRadius: 10,
                                        padding: 5,
                                        paddingLeft: 15,
                                        marginBottom: 5

                                    }}>
                                        <View>
                                            <Text style={[styles.inputText, ThemeStyle.bodyFontBold]}>{this.state.line4}</Text>
                                        </View>

                                        <TouchableOpacity onPress={() => this.copyText(this.state.line4)}>
                                            <View>
                                                <View style={{ width: 50, height: 50, backgroundColor: '#EEF8FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                                    <Image style={[styles.copyIcon]} source={require('../../assets/images/copy_1/copy_1.png')} />
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                uploadScreenshot: true,
                                            })
                                        }}
                                        style={{
                                            flex: 1,
                                            marginRight: 5
                                        }}>
                                        <View style={{ paddingVertical: 15, paddingHorizontal: 20, backgroundColor: '#2566FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                            <Text style={[ThemeStyle.activebtntext]}>Upload payment</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>

                        </View>
                    </View>
                </View>
                <View style={{ height: 60 }} />
                <BottomBar {...this.props} />

                {
                    this.state.uploadScreenshot && <UploadRepaymentScreenshot {...this.props} />
                }

            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#20B77E",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    progressbar: {
        marginVertical: 20
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    container: {
        flex: 1, marginHorizontal: 25
    },
    inputText: {
        color: '#00063B',
        fontSize: 14,
        alignSelf: "flex-start",
        fontWeight: "bold",
        marginBottom: 5,
    },
    copyIcon: {
        width: 15,
        height: 15,
    }
});

export default RepaymentAccount;
