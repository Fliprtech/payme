import React, { Component } from 'react';
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native";
import { Icon } from "react-native-vector-icons/Ionicons";
import { Input } from 'react-native-elements';
import BottomBar from "../components/BottomBar";
import Barslider from "../components/Barslider";
import PagerTopBar from '../components/PagerTopBar';
import SelectAmountBar from "../components/SelectAmountBar";
import ThemeStyle from '../../assets/css/ThemeStyle';
import BlueLogo from "../components/BlueLogo";

class ShortTermLoan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            secureEmi: false,
        };
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                <View style={styles.container}>
                    <PagerTopBar percentage={93} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <View style={styles.inner_block}>
                                    <BlueLogo />
                                    <View style={styles.fullamountblock}>

                                        <View style={{
                                            flexDirection: 'row',
                                            paddingHorizontal: 15,
                                            alignItems: 'center',
                                            borderBottomWidth: 1,
                                            paddingBottom: 15,
                                            marginBottom: 5,
                                            borderBottomColor: 'white',
                                        }}>
                                            <TouchableOpacity
                                                onPress={() => this.props.navigation.goBack()}
                                                style={{
                                                    paddingHorizontal: 15,
                                                    paddingVertical: 17,
                                                    backgroundColor: 'white',
                                                    marginRight: 20,
                                                    borderRadius: 5,
                                                }}>
                                                <Image
                                                    style={{}}
                                                    source={require('../../assets/images/arrow_simple_1_blue/arrow_simple_1.png')} />
                                            </TouchableOpacity>
                                            <View style={{}}>
                                                <Text style={{ fontWeight: 'bold', color: "#2664FF", fontSize: 18, }}>Short term loan</Text>
                                            </View>
                                        </View>

                                        <View style={{}}>
                                            <SelectAmountBar maxAmount={300000} />
                                        </View>

                                        <View
                                            style={{
                                                paddingHorizontal: 15,
                                                borderTopWidth: 1,
                                                borderTopColor: 'white',
                                            }}>
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    alignItems: 'center',
                                                    justifyContent: 'space-between',
                                                    marginTop: 15,
                                                }}>
                                                <Text style={[ThemeStyle.bodyFontRegular]}>Additional amount for bharti axa</Text>
                                                <Text style={[
                                                    ThemeStyle.headingFontMontserratBold,
                                                    { fontSize: 12, color: "#2566FF" }
                                                ]}>₹ 13.18</Text>
                                            </View>

                                            <TouchableOpacity
                                                activeOpacity={0.9}
                                                onPress={() => {
                                                    this.setState({
                                                        secureEmi: !this.state.secureEmi
                                                    });
                                                }}
                                                style={{
                                                    backgroundColor: 'white',
                                                    paddingHorizontal: 10,
                                                    paddingVertical: 15,
                                                    borderRadius: 10,
                                                    marginTop: 10,
                                                    flexDirection: 'row',
                                                    alignItems: 'center',
                                                    justifyContent: 'space-between',
                                                    flexWrap: 'wrap',
                                                }}>
                                                <Text style={[
                                                    ThemeStyle.bodyFontRegular,
                                                    { fontSize: 12, }
                                                ]}>
                                                    Secure your EMI against personal accident*
                                                </Text>
                                                <View>
                                                    <View style={{
                                                        width: 14,
                                                        height: 14,
                                                        borderRadius: 7,
                                                        borderWidth: this.state.secureEmi ? 3 : 1,
                                                        borderColor: "#00063B"
                                                    }} />
                                                </View>
                                            </TouchableOpacity>
                                        </View>

                                    </View>
                                </View>
                                <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 20, }}>
                                    <TouchableOpacity
                                        onPress={() => this.props.navigation.navigate("PurposeOfLoan")}
                                        style={styles.activebutton}>
                                        <Text style={[ThemeStyle.activebtntext]}>Proceed</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.props.navigation.navigate("Welcome");
                                            // this.props.navigation.navigate("Welcome")
                                        }}
                                        style={styles.borderbutton}>
                                        <Text style={[ThemeStyle.borderbtntext]}>Not Now</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ height: 20 }} />
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 60 }} />
                <BottomBar {...this.props} />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        flex: 1
    },
    activebutton: {
        backgroundColor: "#2664FF",
        borderRadius: 10,
        flex: 1,
        paddingVertical: 20,
        paddingHorizontal: 5,
        marginRight: 5,
        borderWidth: 1.2,
        borderColor: "#2664FF",
    },
    borderbutton: {
        borderColor: "#2664FF",
        borderRadius: 10,
        paddingVertical: 20,
        paddingHorizontal: 5,
        borderWidth: 1.2,
        flex: 1,
        marginLeft: 5
    },
    fullamountblock: {
        backgroundColor: "#EEF8FF",
        paddingVertical: 15,
        borderRadius: 10,
    },
    approveamount: {
        borderBottomWidth: 1,
        borderColor: "#fff",
        padding: 10
    },

});

export default ShortTermLoan;
