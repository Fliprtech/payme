import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native";
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Input } from "react-native-elements";
import ThemeStyle from "../../assets/css/ThemeStyle";

class GoldHowitworks extends Component {
    render() {

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
                <ScrollView showsVerticalScrollIndicator={false}>

                    <View style={styles.topslider}>
                        <View style={[styles.sliderTextblock, { marginVertical: 15 }]}>
                            <Text style={[ThemeStyle.bodyFontSemiBold, ThemeStyle.profileHeadingTopText]}>your gold subscription </Text>
                            <Text style={[ThemeStyle.headingFontMontserratBold, ThemeStyle.profileHeading, { color: "#7340FB" }]}>How it actually work</Text>
                        </View>
                    </View>

                    <View style={[styles.container]}>
                        <View style={{ backgroundColor: "#F1EBFF", padding: 20, marginTop: 20, marginBottom: 10, borderRadius: 12 }}>
                            <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading]}>Select gold coin</Text>
                            <View style={{ height: 10 }} />
                            <Text style={[ThemeStyle.bodyFontSemiBold, styles.content]}>Get started by signing up and uploading some documents.</Text>
                        </View>
                        <View style={{ backgroundColor: "#DCCFFE", padding: 20, marginVertical: 10, borderRadius: 12 }}>
                            <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading]}>Pay making charges</Text>
                            <View style={{ height: 10 }} />
                            <Text style={[ThemeStyle.bodyFontSemiBold, styles.content]}>Wait for the approval and credit assigned according to CIBIL.</Text>
                        </View>
                        <View style={{ backgroundColor: "#CEBCFE", padding: 20, marginVertical: 10, borderRadius: 12 }}>
                            <Text style={[ThemeStyle.headingFontMontserratBold, styles.heading]}>Get home delivery</Text>
                            <View style={{ height: 10 }} />
                            <Text style={[ThemeStyle.bodyFontSemiBold, styles.content]}>Choose from the list of option and select that suits you.</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", margin: 15, }} >
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                            style={[styles.submitbutton, { flex: 0, backgroundColor: "white", borderWidth: 1, borderColor: "#2566FF" }]}>
                            <Icon
                                name='chevron-left'
                                color='#2566FF'
                                size={15}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate("Jewellers")}
                            style={[styles.submitbutton, { alignSelf: "stretch", backgroundColor: "#7340FB" }]}>
                            <Text style={[ThemeStyle.activebtntext]}>Proceed</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 20,
        flex: 1,
    },
    topslider: {
        flexDirection: "row",
        backgroundColor: "#F1EBFF",
        borderBottomLeftRadius: 30,
        marginBottom: 10
    },
    sliderTextblock: {
        padding: 30,
    },
    row: {
        flexDirection: "row",
        marginVertical: 10,
    },

    submitbutton: {
        paddingHorizontal: 18,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        flex: 1,
        marginHorizontal: 8
    },
    heading: {
        fontSize: 16,
        color: "#7340FB",
    },
    content: {
        fontSize: 14,
        lineHeight: 22,
        color: "#00063B"
    }

});

export default GoldHowitworks;