
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';

class CompanyType extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
        this.selectCompanyType = this.selectCompanyType.bind(this);
    }

    selectCompanyType() {
        this.props.navigation.navigate("CompanyAddress");
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={styles.container}>
                    <PagerTopBar percentage={25} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <BlueLogo />
                                <View
                                    style={[ThemeStyle.text_block]}>
                                    <Text style={[ThemeStyle.processText]}>What is the company type?</Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        flexWrap: 'wrap',
                                        justifyContent: 'space-between',
                                    }}>
                                    <TouchableOpacity style={[styles.hollowBtn, { marginRight: 10 }]} onPress={this.selectCompanyType}>
                                        <View>
                                            <Text style={[ThemeStyle.borderbtntext]}>Private Limited</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.hollowBtn]} onPress={this.selectCompanyType}>
                                        <View>
                                            <Text style={[ThemeStyle.borderbtntext]}>Government Organization</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.hollowBtn, { marginRight: 10 }]} onPress={this.selectCompanyType}>
                                        <View>
                                            <Text style={[ThemeStyle.borderbtntext]}>Incorporated Companies</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.hollowBtn]} onPress={this.selectCompanyType}>
                                        <View>
                                            <Text style={[ThemeStyle.borderbtntext]}>Public Company</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>

                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 60 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#20B77E",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    container: {
        flex: 1, marginHorizontal: 25
    },
    hollowBtn: {
        paddingHorizontal: 5,
        paddingVertical: 18,
        borderColor: "#2566FF",
        borderWidth: 1.2,
        borderRadius: 10,
        marginBottom: 10,
        flexGrow: 1,
        alignItems: 'center'
    },    
});

export default CompanyType;
