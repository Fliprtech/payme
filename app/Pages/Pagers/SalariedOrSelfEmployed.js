
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';

class SalariedOrSelfEmployed extends Component {


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={styles.container}>
                    <PagerTopBar percentage={25} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <BlueLogo />
                                <View style={[ThemeStyle.text_block]}>
                                    <Text style={[ThemeStyle.processText]}>So, are you a full-time salaried employee? Select "Others" if you are part-time / contract / self-employed professional</Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'space-between',

                                    }}>
                                    <TouchableOpacity style={styles.activebutton} onPress={() => this.props.navigation.navigate("Salary")}>
                                        <Text style={[ThemeStyle.activebtntext]}>Yes, I am</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.borderbutton} onPress={() => this.props.navigation.navigate("Salary")}>
                                        <Text style={[ThemeStyle.borderbtntext]}>Others</Text>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    activebutton: {
        backgroundColor: "#2664FF",
        borderRadius: 10,
        flex: 1,
        paddingVertical: 20,
        paddingHorizontal: 5,
        marginRight: 5,
        borderWidth: 1.2,
        borderColor: "#2664FF",
    },
    borderbutton: {
        borderColor: "#2664FF",
        borderRadius: 10,
        paddingVertical: 20,
        paddingHorizontal: 5,
        borderWidth: 1.2,
        flex: 1,
        marginLeft: 5
    },
    activebtntext: {
        color: "white",
        textAlign: "center"
    },
    borderbtntext: {
        color: "#2664FF",
        textAlign: "center"
    },
    progressbar: {
        marginVertical: 20
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    container: {
        flex: 1, marginHorizontal: 25
    }
});

export default SalariedOrSelfEmployed;
