import React, { Component } from 'react'
import {
    SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet,
    ImageBackground, ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';

export class Reference extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            textDetail_1: "Please enter the reference number of the statment.",
            placeHolder: "Enter number here",
        }
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>

                <View style={[ThemeStyle.container]}>

                    <PagerTopBar percentage={94} />

                    <View style={{
                        justifyContent: "flex-end",
                        flex: 1,
                        flexDirection: "column",
                    }}>
                        <BlueLogo />
                        <View style={[ThemeStyle.text_block]}>
                            <Text style={[ThemeStyle.processText]}>
                                {this.state.textDetail_1}
                            </Text>
                        </View>

                        <View style={[ThemeStyle.inerInputBlock]}>
                            <View style={{
                                flex: 1
                            }}>
                                <TextInput style={[ThemeStyle.inerInputStyle]}
                                    placeholder={this.state.placeHolder}
                                    value={this.state.text}
                                    onChangeText={text => {
                                        var length = text.length;
                                        if (length > 0) {
                                            this.setState({ text: text, show_next: true });
                                        } else {
                                            this.setState({ text: text, show_next: false });
                                        }
                                    }} />
                            </View>

                            <View style={{
                                flex: 0
                            }}>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate("EnterDate")}
                                    style={{ flex: 0, display: this.state.show_next ? 'flex' : 'none' }}>
                                    <View>
                                        <View style={{ width: 50, height: 50, backgroundColor: '#2566FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                            <Image source={require('../../assets/images/arrow_simple_1/arrow_simple_1.png')}></Image>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

export default Reference
