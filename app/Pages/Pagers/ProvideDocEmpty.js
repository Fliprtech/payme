
import React, { Component } from 'react';
import {
    SafeAreaView, TouchableOpacity, View, Image, Text, TextInput,
    StyleSheet, ImageBackground, ScrollView, Alert
} from 'react-native';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';
import DocumentPicker from 'react-native-document-picker';
import { Overlay, ListItem, Button, Icon } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';

class ProvideDocEmpty extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showChooser: false,
        }
        this.uploadStatement = this.uploadStatement.bind(this);
        this.uploadStateViaCamera = this.uploadStateViaCamera.bind(this);
    }

    async uploadStatement() {

        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
            });
            console.log('res : ' + JSON.stringify(res));
            console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size
            );
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }

    }

    uploadStateViaCamera() {

        const options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.launchCamera(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };
            }
        });
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={styles.container}>
                    <PagerTopBar percentage={27} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>

                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <BlueLogo />
                                <View
                                    style={[ThemeStyle.text_block]}>
                                    <Text style={[ThemeStyle.processText]}>Thanks, for choosing that now please select from the option below to proceed further.</Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'stretch',
                                        flexWrap: 'wrap'
                                    }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            Alert.alert("OOPS!", "Under Working");
                                            // this.props.navigation.navigate("SelectBank")
                                        }}
                                        style={{
                                            marginRight: 5,
                                            paddingHorizontal: 20,
                                            paddingVertical: 15,
                                            backgroundColor: '#2566FF',
                                            borderRadius: 10,
                                            flexGrow: 1,
                                        }}>

                                        <Image
                                            style={{ width: 27, height: 20, marginBottom: 5, }}
                                            source={require('../../assets/images/profile_card_2/profile_card2x.png')} />
                                        <Text style={[ThemeStyle.bodyFontSemiBold, { color: '#FFFFFF', marginTop: 10, }]}>Verify douments</Text>

                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {
                                            Alert.alert("OOPS!", "We don't know what is screen will be open. Please provide screen number in xd.");
                                        }}
                                        style={{
                                            marginLeft: 5,
                                            paddingHorizontal: 20,
                                            paddingVertical: 14,
                                            borderColor: "#2566FF",
                                            borderWidth: 1.5,
                                            borderRadius: 10,
                                            flexGrow: 1,
                                        }}>
                                        <Text style={[ThemeStyle.bodyFontSemiBold, { color: '#2566FF', fontWeight: 'bold', fontSize: 25 }]}>?</Text>
                                        <Text style={[ThemeStyle.bodyFontSemiBold, { color: '#2566FF', marginTop: 5 }]}>How it works</Text>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
                <Overlay
                    // width={'auto'}
                    height={'auto'}
                    isVisible={this.state.showChooser}>
                    <View>
                        <Text
                            style={{
                                fontSize: 18, fontWeight: '700',
                                paddingHorizontal: 10,
                                paddingVertical: 15
                            }}>
                            Choose Option
                    </Text>
                        <View>
                            <TouchableOpacity
                                onPress={() => {
                                    this.uploadStateViaCamera();
                                }}
                                style={{
                                    paddingHorizontal: 20,
                                    paddingVertical: 15,
                                    fontSize: 16,
                                    backgroundColor: '#f0f0f0',
                                    marginVertical: 10,
                                    marginHorizontal: 10,
                                }}>
                                <Text>
                                    Camera
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.uploadStatement();
                                }}
                                style={{
                                    paddingHorizontal: 20,
                                    paddingVertical: 15,
                                    fontSize: 16,
                                    backgroundColor: '#f0f0f0',
                                    marginVertical: 10,
                                    marginHorizontal: 10,
                                }}>
                                <Text>
                                    Storage
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ alignItems: 'center', paddingHorizontal: 10, paddingVertical: 20 }}>
                            <Button
                                title="Close"
                                type="solid"
                                raised={true}
                                containerStyle={{
                                }}
                                buttonStyle={{
                                    paddingHorizontal: 30,
                                }}
                                style={{
                                    paddingHorizontal: 20,
                                }}
                                onPress={() => {
                                    this.setState({
                                        showChooser: false,
                                    });
                                }} />
                        </View>
                    </View>
                </Overlay>
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, marginHorizontal: 25
    }
});

export default ProvideDocEmpty;
