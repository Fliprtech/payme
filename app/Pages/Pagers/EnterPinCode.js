
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import BottomBar from '../components/BottomBar';
import PagerTopBar from "../components/PagerTopBar";
import ThemeStyle from '../../assets/css/ThemeStyle';
import BlueLogo from '../components/BlueLogo';
import ArrowIcon from '../components/ArrowIcon';

class EnterPinCode extends Component {

    state = {
        text: '',
    };

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={styles.container}>

                    <PagerTopBar />

                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <BlueLogo />
                                <View
                                    style={[ThemeStyle.text_block]}>
                                    <Text style={[ThemeStyle.processText]}>Enter pin code of place where you stay.</Text>
                                </View>

                                <View style={[ThemeStyle.inerInputBlock]}>
                                    <View style={{ flex: 1 }}>
                                        <TextInput style={[ThemeStyle.inerInputStyle]}
                                            placeholder="Enter your pincode here."
                                            value={this.state.text}
                                            onChangeText={text => this.setState({ text })} /></View>

                                    <TouchableOpacity
                                        onPress={() => {
                                            this.props.navigation.navigate("SalariedOrSelfEmployed");
                                        }}>
                                        <View>
                                            <View style={{ width: 50, height: 50, backgroundColor: '#2566FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                                <ArrowIcon />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#20B77E",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    progressbar: {
        marginVertical: 20
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    container: {
        flex: 1, marginHorizontal: 25
    }
});

export default EnterPinCode;
