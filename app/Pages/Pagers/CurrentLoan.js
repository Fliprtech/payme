import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image } from "react-native";
import React from 'react';
import { Icon } from "react-native-vector-icons/Ionicons";
import { Input, Avatar } from 'react-native-elements';
import BottomBar from "../components/BottomBar";
import Barslider from "../components/Barslider";
import ThemeStyle from '../../assets/css/ThemeStyle';
import BlueLogo from "../components/BlueLogo";


class CurrentLoan extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                <View style={styles.container}>


                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-end' }}>
                        <View style={styles.inner_block}>
                            <BlueLogo />
                            <View style={styles.fullamountblock}>

                                <View style={styles.approveamount}>

                                    <Avatar size="small" icon={{ name: 'circle', type: 'font-awesome' }}
                                        overlayContainerStyle={{ backgroundColor: '#20B77E', borderRadius: 5 }} />

                                    <Text
                                        style={[
                                            ThemeStyle.headingFontMontserratBold,
                                            { color: "#20B77E", fontSize: 20, marginHorizontal: 15, }
                                        ]}>
                                        Current loan
                                    </Text>
                                </View>

                                <View style={styles.amountdetailsblock}>
                                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                        <View style={styles.txtdetailblock}>
                                            <Text style={styles.amoouttxth}>18</Text>
                                            <Text style={styles.amounttxtsh}>Remaining Days</Text>
                                        </View>
                                        <View style={styles.txtdetailblock}>
                                            <Text style={styles.amoouttxth}>21-08-19</Text>
                                            <Text style={styles.amounttxtsh}>Loan Start date</Text>
                                        </View>
                                        <View style={styles.txtdetailblock}>
                                            <Text style={styles.amoouttxth}>₹ 4500</Text>
                                            <Text style={styles.amounttxtsh}>Amount Due</Text>
                                        </View>
                                    </View>
                                    <TouchableOpacity
                                        style={{ backgroundColor: "#20B77E", padding: 15, marginVertical: 10, borderRadius: 5 }}>
                                        <Text style={[ThemeStyle.activebtntext]}>Pay Now</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                    </View>
                    <View style={{ alignItems: "center", marginVertical: 20, }}>
                        <Image source={require('../../assets/images/colorbar.png')} />
                    </View>
                </View>
                <View style={{ height: 60 }} />
                <BottomBar {...this.props} />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        flex: 1
    },

    inner_block: {

    },
    img_block: {
        marginVertical: 30
    },

    greendot: {
        padding: 3,
        backgroundColor: "#20b77e",
        paddingHorizontal: 5,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    orangedot: {
        backgroundColor: "#f75f00",
        padding: 3,
        paddingHorizontal: 5

    },
    reddot: {
        backgroundColor: "#ff085a",
        padding: 3,
        paddingHorizontal: 5

    },
    bluedot: {
        backgroundColor: "#7340fb",
        padding: 3,
        paddingHorizontal: 5,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    },
    bottombar: {
        padding: 15,
        marginTop: 10,
        backgroundColor: 'white',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        flexDirection: "row",
        justifyContent: "center"
    },
    fullamountblock: {
        backgroundColor: "#E8F8F2",
        padding: 10,
        borderRadius: 15
    },
    approveamount: {
        borderBottomWidth: 1,
        borderColor: "#fff",
        padding: 10,
        flexDirection: "row",
        alignItems: "center"
    },
    amountdetailsblock: {
        borderTopWidth: 1,
        borderColor: "#fff",
        padding: 10
    },
    amoouttxth: { fontSize: 16, fontWeight: "bold", color: "#20B77E" },
    amounttxtsh: {
        fontSize: 12,
        marginTop: 5
    },
    txtdetailblock: {
        paddingVertical: 8
    },
    amountbarblock: {

        padding: 10
    },
    amountBar: {
        paddingVertical: 15
    }

});

export default CurrentLoan;