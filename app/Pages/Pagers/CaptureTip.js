import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView } from 'react-native';
import ThemeStyle from '../../assets/css/ThemeStyle';


class CaptureTip extends Component {


    render() {

        return (
            <SafeAreaView style={{ flex: 1, }}>

                <View>
                    <View>
                        <Text>Capture tips</Text>
                        <Text>help you capture documents like a pro</Text>
                    </View>
                    <View>
                        <TouchableOpacity>

                        </TouchableOpacity>
                    </View>
                </View>

            </SafeAreaView>

        );
    }
}


const styles = StyleSheet.create({
    upperbox: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#00000070",
    },
    container: {
        padding: 30,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    },
    activebutton: {
        backgroundColor: "#E8EFFF",
        padding: 12,
        marginVertical: 10,
        paddingHorizontal: 13,
        borderRadius: 9,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    button: {
        backgroundColor: "#F1F2F5",
        paddingHorizontal: 13,
        padding: 12,
        marginVertical: 8,
        borderRadius: 9,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    buttoncolor: {
        color: "#00063B"
    },
    buttonactivecolor: {
        color: "#2566FF",
    },
    submitbutton: {
        backgroundColor: "#2566FF",
        paddingVertical: 20,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    input_block: {
        marginVertical: 8
    }

});

export default CaptureTip;