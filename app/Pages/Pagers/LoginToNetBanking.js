import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { RadioButton } from 'react-native-paper';
import SelectBank from './SelectBank';
import ThemeStyle from '../../assets/css/ThemeStyle';



class LoginToNetBanking extends Component {


    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <View style={{ flex: 1 }}>
                    <SelectBank />
                </View>

                <View style={[styles.upperbox, { position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }]} >
                    <View>
                        <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                            <View style={styles.container} >
                                <Text style={[ThemeStyle.overlayHeading,ThemeStyle.headingFontMontserratBold]} >Login to your account</Text>
                                <Text style={[ThemeStyle.overlaySubHeading, ThemeStyle.bodyFontRegular]}>please enter your details of net banking</Text>
                                <View style={styles.input_block}>
                                    <Input
                                        inputContainerStyle={{
                                            borderBottomWidth: 0,
                                            backgroundColor: "#F1F2F5",
                                            borderRadius: 10
                                        }}
                                        containerStyle={{
                                            padding: 0,
                                            width: '100%',
                                            alignItems: 'stretch',
                                            paddingHorizontal: 0,
                                        }}
                                        inputStyle={{
                                            fontSize: 13,
                                            fontWeight: "500",
                                            color: "black",
                                            marginLeft: 14,
                                            paddingVertical: 15
                                        }}

                                        placeholderTextColor="#c5c6d3"
                                        placeholder='Enter your username'
                                        leftIcon={
                                            <Image style={{
                                                width: 14,
                                                height: 16
                                            }}
                                                source={require('../../assets/images/profile_close_1/profile_close2.png')} />
                                        }
                                    />
                                </View>

                                <View style={styles.input_block}>
                                    <Input
                                        inputContainerStyle={{
                                            borderBottomWidth: 0,
                                            backgroundColor: "#F1F2F5",
                                            borderRadius: 10
                                        }}
                                        containerStyle={{
                                            padding: 0,
                                            width: '100%',
                                            alignItems: 'stretch',
                                            paddingHorizontal: 0,
                                        }}
                                        inputStyle={{
                                            fontSize: 13,
                                            fontWeight: "500",
                                            color: "black",
                                            marginLeft: 14,
                                            paddingVertical: 15
                                        }}
                                        secureTextEntry={true}
                                        placeholderTextColor="#c5c6d3"
                                        placeholder='Enter your password'
                                        leftIcon={
                                            <Image
                                                style={{
                                                    width: 16,
                                                    height: 16,
                                                }} source={require('../../assets/images/lock_1/lock2.png')} />
                                        } />
                                </View>
                                <View
                                    style={{
                                        flexDirection: 'row'
                                    }}>
                                    <Text style={{ fontSize: 8, fontWeight: 'bold' }}>Note: </Text>
                                    <Text style={{ fontSize: 8 }}>Your credentials are never stored! We analyze your transaction for last 4 months.</Text>
                                </View>

                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate("ProvideDocument")}
                                    style={styles.submitbutton}>
                                    <Text style={[ThemeStyle.activebtntext]}>Proceed</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </View>

            </SafeAreaView>

        );
    }
}


const styles = StyleSheet.create({
    upperbox: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#00000070",
    },    
    container: {
        padding: 30,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    activebutton: {
        backgroundColor: "#E8EFFF",
        padding: 12,
        marginVertical: 10,
        paddingHorizontal: 13,
        borderRadius: 9,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    button: {
        backgroundColor: "#F1F2F5",
        paddingHorizontal: 13,
        padding: 12,
        marginVertical: 8,
        borderRadius: 9,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    buttoncolor: {
        color: "#00063B"
    },
    buttonactivecolor: {
        color: "#2566FF",

    },
    submitbutton: {
        backgroundColor: "#2566FF",
        paddingHorizontal: 13,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
    },   
    input_block: {
        marginVertical: 8
    }

});

export default LoginToNetBanking;