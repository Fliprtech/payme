
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';

class EnterAccountNumber extends Component {

    constructor(props) {
        super(props)
        this.state = {
            text: "",
            bankDetail: null,
        }
    }

    componentDidMount() {
        this.state.bankDetail = this.props.navigation.state.params.bankdetail;
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={[ThemeStyle.container]}>

                    <PagerTopBar percentage={94} />

                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <BlueLogo />
                        <View style={[ThemeStyle.text_block]}>
                            <Text style={[ThemeStyle.processText]}>Please enter your account number.</Text>
                        </View>

                        <View style={[ThemeStyle.inerInputBlock]}>
                            <View style={{
                                flex: 1
                            }}>
                                <TextInput style={[ThemeStyle.inerInputStyle]}
                                    placeholder="Enter account number here."
                                    value={this.state.text}
                                    keyboardType={'number-pad'}
                                    onChangeText={text => {
                                        var length = text.length;
                                        this.state.bankDetail.accountNumber = text;
                                        if (length > 0) {
                                            this.setState({ text: text, show_next: true });
                                        } else {
                                            this.setState({ text: text, show_next: false });
                                        }

                                    }} />
                            </View>

                            <View style={{
                                flex: 0
                            }}>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate("EnterIFSCcode", { bankdetail: this.state.bankDetail })}
                                    style={{ flex: 0, display: this.state.show_next ? 'flex' : 'none' }}>
                                    <View>
                                        <View style={{ width: 50, height: 50, backgroundColor: '#2566FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                            <Image source={require('../../assets/images/arrow_simple_1/arrow_simple_1.png')}></Image>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#20B77E",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    progressbar: {
        marginVertical: 20
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    container: {
        flex: 1, marginHorizontal: 25
    }
});

export default EnterAccountNumber;
