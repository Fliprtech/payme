
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import BottomBar from '../components/BottomBar';
import PagerTopBar from "../components/PagerTopBar";
import ThemeStyle from '../../assets/css/ThemeStyle';
import { Input, Icon } from 'react-native-elements';
import BlueLogo from '../components/BlueLogo';
import ArrowIcon from '../components/ArrowIcon';


class EnterReferalCode extends Component {

    state = {
        text: '',
        show_next: false,
    };

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={[styles.container]}>
                    <PagerTopBar percentage={6} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <BlueLogo />
                                <View
                                    style={[ThemeStyle.text_block]}>
                                    <Text style={[ThemeStyle.processText]}>
                                        Enter the referral code below to proceed further,
                                    </Text>
                                </View>
                                <View
                                    style={[ThemeStyle.inerInputBlock]}>
                                    <View style={{ flex: 1 }}>
                                        <TextInput
                                            inputContainerStyle={{
                                                borderBottomWidth: 0,
                                            }}
                                            style={[ThemeStyle.inerInputStyle]}
                                            placeholder="Enter referral code here."
                                            value={this.state.text}
                                            onChangeText={text => {
                                                var length = text.length;
                                                if (length > 0) {
                                                    this.setState({ text: text, show_next: true });
                                                } else {
                                                    this.setState({ text: text, show_next: false });
                                                }

                                            }} />
                                    </View>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.props.navigation.navigate('SalariedOrSelfEmployed')
                                        }}
                                        style={{ flex: 0, display: this.state.show_next ? 'flex' : 'none' }}>
                                        <View>
                                            <View style={{ width: 50, height: 50, backgroundColor: '#2566FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                                <ArrowIcon />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#20B77E",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    progressbar: {
        marginVertical: 20
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    container: {
        flex: 1, marginHorizontal: 25
    }
});

export default EnterReferalCode;
