
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';


class LoanDirbursed extends Component {


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>

                <View style={styles.container}>

                    <PagerTopBar percentage={100} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <BlueLogo />
                        <View style={[ThemeStyle.text_block]}>
                            <Text style={[ThemeStyle.processText]}>Congratulations!Your loan has been disburse to your account.</Text>
                        </View>

                        <View style={{
                            flexDirection: 'column',
                        }}>
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',

                            }}>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate("ShareFeedback")}
                                    style={{
                                        flex: 1,
                                        marginRight: 5
                                    }}>
                                    <View>
                                        <View style={{ paddingVertical: 15, paddingHorizontal: 20, backgroundColor: '#2566FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                            <Text style={[ThemeStyle.activebtntext]}>Repay</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate("ShareFeedback")}
                                    style={{
                                        flex: 1
                                    }}>
                                    <View>
                                        <View style={{ paddingVertical: 15, paddingHorizontal: 20, borderColor: "#2566FF", borderWidth: 1, borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                            <Text style={[ThemeStyle.borderbtntext]}>Go Back</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>

                            </View>

                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#20B77E",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    progressbar: {
        marginVertical: 20
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    container: {
        flex: 1, marginHorizontal: 25
    }
});

export default LoanDirbursed;
