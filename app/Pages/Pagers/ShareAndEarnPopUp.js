import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, ScrollView, TouchableOpacity, Clipboard, ToastAndroid } from 'react-native'
import ThemeStyle from '../../assets/css/ThemeStyle'
import { SafeAreaView } from 'react-native-safe-area-context'
import Icon from 'react-native-vector-icons/FontAwesome5';

export class ShareAndEarnPopUp extends Component {

    constructor(props) {
        super(props)
        this.state = {
            profile: [],
            link: "https://www.paymeindia.in/corporate",
        }
    }


    render() {

        this.state.profile = [
            {
                id: 1,
            },
            {
                id: 2,
            },
            {
                id: 3,
            },
            {
                id: 4,
            },
            {
                id: 5,
            },
            {
                id: 6,
            },
            {
                id: 7,
            },
            {
                id: 8,
            }
        ];

        return (
            <View style={[ThemeStyle.overFlowBox, { position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }]} >
                <View style={[ThemeStyle.overFlowContainer]} >

                    <ScrollView>

                        <Text style={[ThemeStyle.overlayHeading, ThemeStyle.headingFontMontserratBold]}>
                            {"Share and earn"}
                        </Text>
                        <Text style={[ThemeStyle.overlaySubHeading, ThemeStyle.bodyFontRegular]}>
                            {"select from the contacts below"}
                        </Text>

                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            {
                                this.state.profile.map((item, index) => {
                                    return (
                                        <View key={index}>
                                            <Image
                                                style={{
                                                    width: 60,
                                                    height: 60,
                                                    resizeMode: 'cover',
                                                    marginRight: 10,
                                                    borderRadius: 10,
                                                    marginBottom: 20,
                                                }}
                                                source={require("../../assets/images/share-profile.png")} />
                                        </View>
                                    );
                                })
                            }
                        </ScrollView>

                        <View style={{ height: 2, backgroundColor: '#efefef', marginBottom: 20, }} />

                        <Text style={[ThemeStyle.overlayHeading, ThemeStyle.headingFontMontserratBold]}>
                            {"Share and earn"}
                        </Text>
                        <Text style={[ThemeStyle.overlaySubHeading, ThemeStyle.bodyFontRegular]}>
                            {"select from the contacts below"}
                        </Text>

                        <View>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

                                <TouchableOpacity style={[styles.shareSocialBox, { backgroundColor: '#54CF61' }]}>
                                    <Image style={[styles.socialIcon]}
                                        source={require("../../assets/images/iconmonstr-whatsapp-1/iconmonstr-whatsapp-1.png")} />
                                </TouchableOpacity>

                                <TouchableOpacity style={[styles.shareSocialBox, { backgroundColor: '#385898' }]}>
                                    <Image style={[styles.socialIcon]}
                                        source={require("../../assets/images/facebook-logo/facebook-logo.png")} />
                                </TouchableOpacity>

                                <TouchableOpacity style={[styles.shareSocialBox, { backgroundColor: '#2977C9' }]}>
                                    <Image style={[styles.socialIcon]}
                                        source={require("../../assets/images/linkedin-letters/linkedin-letters.png")} />
                                </TouchableOpacity>

                                <TouchableOpacity style={[styles.shareSocialBox, { backgroundColor: '#333333' }]}>
                                    <Image style={[styles.socialIcon]}
                                        source={require("../../assets/images/iconmonstr-instagram-11/iconmonstr-instagram-11.png")} />
                                </TouchableOpacity>
                            </ScrollView>
                        </View>

                        <View style={{ marginBottom: 20, }}>
                            <TouchableOpacity
                                onPress={() => {
                                    Clipboard.setString(this.state.link);
                                    ToastAndroid.showWithGravity("Link copied", ToastAndroid.LONG, ToastAndroid.BOTTOM);
                                }}
                                style={{
                                    paddingHorizontal: 15,
                                    paddingVertical: 20,
                                    borderWidth: 1,
                                    borderColor: "#EFEFEF",
                                    borderRadius: 10,
                                }}>
                                <Text style={[ThemeStyle.bodyFontBold]}>
                                    {this.state.link}
                                </Text>
                            </TouchableOpacity>
                        </View>

                        <View>
                            <View style={{ flexDirection: "row", display: 'flex', flex: 1, }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        Clipboard.setString(this.state.link);
                                        ToastAndroid.showWithGravity("Link copied", ToastAndroid.LONG, ToastAndroid.BOTTOM);
                                    }}
                                    style={[styles.submitbutton, { flex: 1, alignSelf: "stretch" }]}>
                                    <Text style={[ThemeStyle.activebtntext]}>Copy link and share</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.close();
                                    }}
                                    style={[styles.submitbutton, { flex: 0, backgroundColor: "white", borderWidth: 1, borderColor: "#2566FF" }]}>
                                    <Icon
                                        name='times'
                                        color='#2566FF'
                                        size={15} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    shareSocialBox: {
        marginRight: 10,
        borderRadius: 10,
        marginBottom: 20,
        width: 60,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
    },
    socialIcon: {
        width: 20,
        height: 20,
        resizeMode: "contain",
    },
    submitbutton: {
        backgroundColor: "#2566FF",
        paddingHorizontal: 18,
        padding: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 8
    },
});

export default ShareAndEarnPopUp
