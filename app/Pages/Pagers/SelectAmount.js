import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native";
import React from 'react';
import { Icon } from "react-native-vector-icons/Ionicons";
import { Input } from 'react-native-elements';
import BottomBar from "../components/BottomBar";
import Barslider from "../components/Barslider";
import PagerTopBar from '../components/PagerTopBar';
import SelectAmountBar from "../components/SelectAmountBar";
import ThemeStyle from '../../assets/css/ThemeStyle';
import BlueLogo from "../components/BlueLogo";


class SelectAmount extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                <View style={styles.container}>
                    <PagerTopBar percentage={93} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <View style={styles.inner_block}>
                                    <BlueLogo />
                                    <View style={styles.fullamountblock}>

                                        <View style={styles.approveamount}>
                                            <Text style={[ThemeStyle.headingFontMontserratBold, { color: "#2566FF", fontSize: 20, }]}>₹ 4500</Text>
                                            <Text style={[ThemeStyle.bodyFontSemiBold, { color: "#00063B", fontSize: 14, marginTop: 2 }]}>Approved limit</Text>
                                        </View>

                                        <View style={{}}>
                                            <SelectAmountBar maxAmount={300000} />
                                        </View>

                                    </View>
                                </View>
                                <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 20, }}>
                                    <TouchableOpacity
                                        onPress={() => this.props.navigation.navigate("PurposeOfLoan")}
                                        style={styles.activebutton}>
                                        <Text style={[ThemeStyle.activebtntext]}>Proceed</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.props.navigation.navigate("Welcome");
                                            // this.props.navigation.navigate("Welcome")
                                        }}
                                        style={styles.borderbutton}>
                                        <Text style={[ThemeStyle.borderbtntext]}>Not Now</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ height: 20 }} />
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 60 }} />
                <BottomBar {...this.props} />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        flex: 1
    },
    activebutton: {
        backgroundColor: "#2664FF",
        borderRadius: 10,
        flex: 1,
        paddingVertical: 20,
        paddingHorizontal: 5,
        marginRight: 5,
        borderWidth: 1.2,
        borderColor: "#2664FF",
    },
    borderbutton: {
        borderColor: "#2664FF",
        borderRadius: 10,
        paddingVertical: 20,
        paddingHorizontal: 5,
        borderWidth: 1.2,
        flex: 1,
        marginLeft: 5
    },
    fullamountblock: {
        backgroundColor: "#EEF8FF",
        paddingVertical: 15,
    },
    approveamount: {
        borderBottomWidth: 1,
        borderColor: "#fff",
        padding: 10
    },

});

export default SelectAmount;