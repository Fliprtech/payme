
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import ThemeStyle from '../../assets/css/ThemeStyle';
import BlueLogo from '../components/BlueLogo';

class AccountForDisbursal extends Component {

    constructor(props) {
        super(props)

        this.state = {
            bankDetail: {
                accountHolderName: "",
                accountNumber: "",
                accountIFSC: "",
                bankName: "",
            }
        }
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={styles.container}>

                    <PagerTopBar percentage={94} />

                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <BlueLogo />
                        <View style={[ThemeStyle.text_block]}>
                            <Text style={[ThemeStyle.processText]}>Please enter your detail name on bank account you want your money to be disbursed.</Text>
                        </View>

                        <View style={{
                            flexDirection: 'column',
                        }}>
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',

                            }}>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate("NameOnBank", { bankdetail: this.state.bankDetail })}
                                    style={{
                                        flex: 1,
                                        marginRight: 5
                                    }}>
                                    <View>
                                        <View style={{ paddingVertical: 15, backgroundColor: '#2566FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                            <Text style={[ThemeStyle.activebtntext]}>Old account</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate("NameOnBank", { bankdetail: this.state.bankDetail })}
                                    style={{
                                        flex: 1
                                    }}>
                                    <View>
                                        <View style={{ paddingVertical: 15, borderColor: "#2566FF", borderWidth: 1, borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                            <Text style={[ThemeStyle.borderbtntext]}>New account</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#20B77E",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    progressbar: {
        marginVertical: 20
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    container: {
        flex: 1, marginHorizontal: 25
    }
});

export default AccountForDisbursal;
