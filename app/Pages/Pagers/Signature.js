import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView, Animated, Dimensions } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { RadioButton } from 'react-native-paper';
import ThemeStyle from '../../assets/css/ThemeStyle';


class Signature extends Component {

    constructor(props) {
        super(props);
        this.state = {
            itemPosition: new Animated.Value(Dimensions.get('window').height + 100),
        }
        this.move_up = this.move_up.bind(this);

    }

    move_up() {
        Animated.timing(this.state.itemPosition, {
            toValue: 0,
            duration: 300
        }).start(() => {
            // this.props.privacyAnimationCallback();
        });
    }

    render() {

        this.move_up();

        return (
            <View style={[styles.upperbox, { position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }]} >
                <Animated.View
                    style={[styles.container,
                    {
                        transform: [
                            {
                                translateY: this.state.itemPosition
                            }
                        ]
                    }]} >
                    <Text style={[ThemeStyle.overlayHeading, ThemeStyle.headingFontMontserratBold]} >Sign here</Text>
                    <Text style={[ThemeStyle.overlaySubHeading, ThemeStyle.bodyFontRegular]}>draw your signature using finger below</Text>

                    <View style={styles.signatureblock}>
                        <Text> </Text>
                    </View>
                    <Text style={[ThemeStyle.bodyFontRegular, { fontSize: 10 }]}>Note: Make sure it resembles of your original signature on PAN Card</Text>
                    <View style={{ flexDirection: "row" }}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate("PreApprovedLimit")}
                            style={[styles.submitbutton, { alignSelf: "stretch" }]}>
                            <Text style={[ThemeStyle.activebtntext]}>Save and continue</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.submitbutton, { flex: 0, backgroundColor: "white", borderWidth: 1, borderColor: "#2566FF" }]}>
                            <Text style={[ThemeStyle.borderbtntext, { color: "#2566FF" }]}>Clear</Text>
                        </TouchableOpacity>
                    </View>
                </Animated.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    upperbox: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#00000070",
    },
    container: {
        padding: 30,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    },

    submitbutton: {
        backgroundColor: "#2566FF",
        paddingHorizontal: 18,
        paddingVertical: 15,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        flex: 1,
        marginHorizontal: 10
    },
    signatureblock: {
        backgroundColor: "#F1F2F5",
        padding: 20,
        marginVertical: 10,
        paddingVertical: 30,
        borderRadius: 10,
        height: 150
    }

});

export default Signature;