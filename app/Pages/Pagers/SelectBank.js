
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';

class SelectBank extends Component {

    constructor(props) {
        super(props);
        this.state = {}
        this.openLogin = this.openLogin.bind(this);
    }

    openLogin() {
        this.props.navigation.navigate("LoginToNetBanking");
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={styles.container}>
                    <PagerTopBar percentage={28} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <BlueLogo />
                                <View
                                    style={[ThemeStyle.text_block]}>
                                    <Text style={[ThemeStyle.processText]}>Choose your bank from the list of bank below</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'flex-start',
                                            alignContent: 'stretch',
                                            // alignItems: 'stretch',                                        
                                            flexWrap: 'wrap',
                                            width: '100%',
                                        }}>
                                        <View style={[styles.IconBox, { backgroundColor: '#003399', }]}>
                                            <TouchableOpacity
                                                onPress={this.openLogin}
                                                style={{
                                                    marginVertical: 10,
                                                    marginHorizontal: 3,
                                                }}>
                                                <Image source={require('../../assets/images/Bank/HDFC-bank-logo.png')}
                                                    style={{
                                                        width: 30, height: 30,
                                                    }}></Image>
                                                <Text style={[ThemeStyle.headingFont,styles.btnText]}>HDFC Bank</Text>
                                            </TouchableOpacity>
                                        </View>

                                        <View style={[styles.IconBox, { backgroundColor: '#F01318', }]}>
                                            <TouchableOpacity
                                                onPress={this.openLogin}
                                                style={{
                                                    marginVertical: 10,
                                                    marginHorizontal: 3
                                                }}>
                                                <Image source={require('../../assets/images/Bank/kotak-logo.png')}
                                                    style={{
                                                        width: 30, height: 30,
                                                    }}></Image>
                                                <Text style={[ThemeStyle.headingFont,styles.btnText]}>Kotak Bank</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={[styles.IconBox, { backgroundColor: '#0062A8', }]}>
                                            <TouchableOpacity
                                                onPress={this.openLogin}
                                                style={{
                                                    marginVertical: 10,
                                                    marginHorizontal: 3
                                                }}>
                                                <Image source={require('../../assets/images/Bank/yes-bank-logo.png')}
                                                    style={{
                                                        width: 30, height: 30,
                                                    }}></Image>
                                                <Text style={[ThemeStyle.headingFont,styles.btnText]}>Yash Bank</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row' }}>
                                    <View style={[styles.IconBox, { backgroundColor: '#00BDF2', }]}>
                                        <TouchableOpacity
                                            onPress={this.openLogin}
                                            style={{
                                                marginVertical: 10,
                                                marginHorizontal: 5
                                            }}>
                                            <Image source={require('../../assets/images/Bank/Citi.png')}
                                                style={{
                                                    width: 45, height: 30,
                                                }}></Image>
                                            <Text style={[ThemeStyle.headingFont,styles.btnText]}>CITI Bank</Text>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={[styles.IconBox, { backgroundColor: '#fe4d01', }]}>
                                        <TouchableOpacity
                                            onPress={this.openLogin}
                                            style={{
                                                marginVertical: 10,
                                                marginHorizontal: 5
                                            }}>
                                            <Image source={require('../../assets/images/Bank/BOB.jpg')}
                                                style={{
                                                    width: 30, height: 30,
                                                }}></Image>
                                            <Text style={[ThemeStyle.headingFont,styles.btnText]}>Bank of Baroda</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                <TouchableOpacity
                                    style={{
                                        marginTop: 20,
                                    }}>
                                    <View style={{ paddingVertical: 20, paddingHorizontal: 15, width: "100%", borderColor: "#2566FF", borderWidth: 1.5, borderRadius: 10, }}>
                                        <Text style={[ThemeStyle.borderbtntext]}>Other Bank</Text>
                                    </View>
                                </TouchableOpacity>
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 60 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, marginHorizontal: 25
    },
    IconBox: {
        paddingHorizontal: 15,
        paddingVertical: 0,
        borderRadius: 10,
        marginHorizontal: 3,
        marginVertical: 5,
        flex: 1,
    },
    btnText:{
        color: '#ffffff', 
        marginTop: 10,
        fontSize:12,
    }
});

export default SelectBank;
