import React, { Component } from 'react'
import { Text, View, Image, TextInput } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import BottomBar from '../components/BottomBar'
import ThemeStyle from '../../assets/css/ThemeStyle'
import PagerTopBar from '../components/PagerTopBar'
import BlueLogo from '../components/BlueLogo'
import { TouchableOpacity } from 'react-native-gesture-handler'

export class NameOfYourBank extends Component {

    constructor(props) {
        super(props)
        this.state = {
            text: "",
            bankDetail: null,
        }
    }

    componentDidMount() {
        this.state.bankDetail = this.props.navigation.state.params.bankdetail;
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={[ThemeStyle.container]}>
                    <PagerTopBar percentage={94} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <BlueLogo />
                        <View style={[ThemeStyle.text_block]}>
                            <Text style={[ThemeStyle.processText]}>
                                Please enter name of your bank here.
                            </Text>
                        </View>

                        <View style={[ThemeStyle.inerInputBlock]}>
                            <View style={{
                                flex: 1
                            }}>
                                <TextInput style={[ThemeStyle.inerInputStyle]}
                                    placeholder="Enter bank name here"
                                    value={this.state.text}
                                    onChangeText={text => {
                                        var length = text.length;
                                        this.state.bankDetail.bankName = text;
                                        if (length > 0) {
                                            this.setState({ text: text, show_next: true });
                                        } else {
                                            this.setState({ text: text, show_next: false });
                                        }
                                    }} />
                            </View>

                            <View
                                style={{
                                    flex: 0
                                }}>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate("CheckDetails", { bankdetail: this.state.bankDetail })}
                                    style={{ flex: 0, display: this.state.show_next ? 'flex' : 'none' }}>
                                    <View>
                                        <View style={{ width: 50, height: 50, backgroundColor: '#2566FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                            <Image source={require('../../assets/images/arrow_simple_1/arrow_simple_1.png')} />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView>
        )
    }
}

export default NameOfYourBank



