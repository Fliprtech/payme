
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import BottomBar from '../components/BottomBar';
import PagerTopBar from "../components/PagerTopBar";
import ThemeStyle from '../../assets/css/ThemeStyle';
import BlueLogo from '../components/BlueLogo';

class DoYouHaveReferalCode extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={styles.container}>

                    <PagerTopBar percentage={6} />

                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>

                        <View>
                            <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>
                                <BlueLogo />
                                <View
                                    style={[ThemeStyle.text_block]}>
                                    <Text style={[ThemeStyle.processText]}>Hi {this.props.navigation.state.params.name},</Text>
                                    <Text style={[ThemeStyle.processText]}>Do you have any referral code?</Text>
                                </View>

                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'space-between',
                                    }}>
                                    <TouchableOpacity
                                        style={styles.activebutton}
                                        onPress={() => {
                                            this.props.navigation.navigate("EnterReferalCode");
                                        }}>
                                        <Text style={[ThemeStyle.activebtntext]}>Yes, I have</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={styles.borderbutton}
                                        onPress={() => {
                                            this.props.navigation.navigate('SalariedOrSelfEmployed');
                                        }}>
                                        <Text style={[ThemeStyle.borderbtntext]}>No, I don't</Text>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({

    progressbar: {
        marginVertical: 20
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    container: {
        flex: 1, marginHorizontal: 25
    },
    activebutton: {
        backgroundColor: "#2664FF",
        borderRadius: 10,
        flex: 1,
        paddingVertical: 20,
        paddingHorizontal: 5,
        marginRight: 5,
        borderWidth: 1.2,
        borderColor: "#2664FF",
    },
    borderbutton: {
        borderColor: "#2664FF",
        borderRadius: 10,
        paddingVertical: 20,
        paddingHorizontal: 5,
        borderWidth: 1.2,
        flex: 1,
        marginLeft: 5
    }  

});

export default DoYouHaveReferalCode;
