import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { RadioButton } from 'react-native-paper';
import OptionForMandateAndSign from './OptionForMandateAndSign';
import ThemeStyle from '../../assets/css/ThemeStyle';


class PhysicalNach extends Component {

    render() {

        return (
            <SafeAreaView style={{ flex: 1, }}>

                <View style={{ flex: 1 }}>
                    <OptionForMandateAndSign />
                </View>

                <View style={[styles.upperbox, { position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }]} >
                    <View style={styles.container} >
                        <Text style={[ThemeStyle.overlayHeading,ThemeStyle.headingFontMontserratBold]} >Send physical nach</Text>
                        <Text style={[ThemeStyle.overlaySubHeading,ThemeStyle.bodyFontRegular]}>you need to send following documents via courier</Text>

                        <View>
                            <View style={styles.listitem}>
                                <View style={[styles.listCircle]}>
                                    <Text style={styles.listnumber}>1</Text>
                                </View>
                                <Text style={[styles.listitemtext,ThemeStyle.bodyFontRegular]}>Signed loan agreement copy</Text>
                            </View>
                            <View style={styles.listitem}>
                                <View style={[styles.listCircle]}>
                                    <Text style={styles.listnumber}>2</Text>
                                </View>
                                <Text style={[styles.listitemtext,ThemeStyle.bodyFontRegular]}>Two signed accounts payee checks</Text>
                            </View>
                            <View style={styles.listitem}>
                                <View style={[styles.listCircle]}>
                                    <Text style={styles.listnumber}>3</Text>
                                </View>
                                <Text style={[styles.listitemtext,ThemeStyle.bodyFontRegular]}>Signed NACH copy</Text>
                            </View>
                        </View>
                        <Text style={{ fontSize: 11 }}>Note: Please reach out to support@paymeindia.in for more</Text>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate("EMandate")}
                            style={styles.submitbutton}>
                            <Text style={[ThemeStyle.activebtntext]}>Go back</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}


const styles = StyleSheet.create({
    upperbox: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#00000070",
    },    
    container: {
        padding: 30,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    },
    submitbutton: {
        backgroundColor: "#2566FF",
        paddingHorizontal: 13,
        padding: 15,
        marginTop: 15,
        borderRadius: 9,
    },    
    input_block: {
        marginVertical: 8
    },   
    listCircle:{
        backgroundColor: "#00063B", 
        borderRadius: 30, 
        width: 20, 
        height: 20,
        alignItems:'center',
        justifyContent:'center',
    },
    listnumber: {
        color: "#ffffff",
        textAlign: "center",
        paddingVertical: 2,
        fontSize:12,
    },
    listitem: {
        flexDirection: "row",
        alignContent: "center",
        marginVertical: 10
    },
    listitemtext: {
        fontSize: 14,        
        marginLeft: 15,
        color: "#00063B"
    }

});

export default PhysicalNach;