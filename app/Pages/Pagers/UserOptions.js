import { Component } from "react";
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native";
import React from 'react';
import { Input, Avatar, Icon } from 'react-native-elements';
import BottomBar from "../components/BottomBar";
import ThemeStyle from '../../assets/css/ThemeStyle';
import BlueLogo from "../components/BlueLogo";


class UserOptions extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>


                <View style={[styles.container, { justifyContent: 'flex-end', flexDirection: 'column', }]}>
                    <View style={{}}>

                        <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false}>

                            <View style={styles.inner_block}>
                                <BlueLogo />
                                <View>
                                    <TouchableOpacity>
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                justifyContent: "space-between",
                                                backgroundColor: '#C6E5FF',
                                                borderRadius: 10,
                                                paddingLeft: 20,
                                                marginBottom: 5,
                                                overflow: "hidden",
                                            }}>
                                            <View
                                                style={{
                                                    justifyContent: "center",
                                                }}>
                                                <Text
                                                    style={[
                                                        ThemeStyle.headingFontMontserratBold,
                                                        styles.heading,
                                                        {
                                                            color: "#1C99FF",
                                                        }
                                                    ]}>Buy Gold</Text>
                                                <Text
                                                    style={{
                                                        color: '#00063B',
                                                        fontSize: 12,
                                                    }}>gold from our partner</Text>
                                            </View>
                                            <Image source={require('../../assets/images/gold.png')} style={{
                                                height: 100,
                                                maxWidth: 150,
                                            }} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>


                            <View>
                                <View style={{ flexDirection: "row", flexWrap: "nowrap", justifyContent: "space-between", marginVertical: 10, }}>
                                    <TouchableOpacity
                                        onPress={() => this.props.navigation.navigate("CurrentLoan")}
                                        style={{ flex: 1, backgroundColor: "#D2F1E5", marginHorizontal: 5, padding: 10, borderRadius: 10, paddingVertical: 20 }}>

                                        <Avatar size="small" icon={{ name: 'circle', type: 'font-awesome' }}
                                            overlayContainerStyle={{ backgroundColor: '#20B77E', borderRadius: 5 }}
                                        />

                                        <Text
                                            style={[
                                                ThemeStyle.headingFontMontserratBold,
                                                styles.heading,
                                                {
                                                    color: "#20B77E",
                                                }
                                            ]}>
                                            Current loan
                                        </Text>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>status of your loan</Text>

                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => this.props.navigation.navigate("LoanHistory")}
                                        style={{ flex: 1, backgroundColor: "#FEDFCC", marginHorizontal: 5, padding: 10, borderRadius: 10, paddingVertical: 20 }}>

                                        <Avatar size="small" icon={{ name: 'square', type: 'font-awesome' }}
                                            overlayContainerStyle={{ backgroundColor: '#F75F00', borderRadius: 5 }}
                                        />

                                        <Text
                                            style={[
                                                ThemeStyle.headingFontMontserratBold,
                                                styles.heading,
                                                {
                                                    color: "#F75F00",
                                                }
                                            ]}>
                                            Loan history
                                        </Text>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>history of all the loans</Text>

                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: "row", justifyContent: "space-between", }}>

                                    <TouchableOpacity
                                        onPress={() => this.props.navigation.navigate("PaymeScore")}
                                        style={{ flex: 1, backgroundColor: "#FFCDDE", marginHorizontal: 5, padding: 10, borderRadius: 10, paddingVertical: 20 }}>

                                        <Avatar size="small" icon={{ name: 'circle', type: 'font-awesome' }}
                                            overlayContainerStyle={{ backgroundColor: '#FF085A', borderRadius: 5 }}
                                        />

                                        <Text
                                            style={[
                                                ThemeStyle.headingFontMontserratBold,
                                                styles.heading,
                                                {
                                                    color: "#FF085A",
                                                }
                                            ]}>
                                            PayMe score
                                         </Text>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>check your level</Text>

                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => this.props.navigation.navigate("EMICalculator")}
                                        style={{ flex: 1, backgroundColor: "#E3D9FE", marginHorizontal: 5, padding: 10, borderRadius: 10, paddingVertical: 20 }}>

                                        <Avatar size="small" icon={{ name: 'square', type: 'font-awesome' }}
                                            overlayContainerStyle={{ backgroundColor: '#7340FB', borderRadius: 5 }} />
                                        <Text
                                            style={[
                                                ThemeStyle.headingFontMontserratBold,
                                                styles.heading,
                                                {
                                                    color: "#7340FB",
                                                }
                                            ]}>
                                            EMI Calculator
                                        </Text>
                                        <Text style={[ThemeStyle.bodyFontRegular, styles.subHeading]}>calculate your EMI</Text>

                                    </TouchableOpacity>
                                </View>

                            </View>

                            <TouchableOpacity
                                onPress={() => {
                                    this.props.closeOptions();
                                }}
                                style={{ alignItems: "center", marginTop: 10 }}>
                                <Image style={{ width: 70, height: 70 }} source={require('../../assets/images/cross.png')}></Image>
                            </TouchableOpacity>

                            <View style={{ height: 80 }} />

                        </ScrollView>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 12,
        flex: 1
    },


    inner_block: {

    },
    img_block: {
        marginVertical: 0
    },

    greendot: {
        padding: 3,
        backgroundColor: "#20b77e",
        paddingHorizontal: 5,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    orangedot: {
        backgroundColor: "#f75f00",
        padding: 3,
        paddingHorizontal: 5

    },
    reddot: {
        backgroundColor: "#ff085a",
        padding: 3,
        paddingHorizontal: 5

    },
    bluedot: {
        backgroundColor: "#7340fb",
        padding: 3,
        paddingHorizontal: 5,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    },
    bottombar: {
        padding: 15,
        marginTop: 10,
        backgroundColor: 'white',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        flexDirection: "row",
        justifyContent: "center"
    },
    heading: {
        fontSize: 14,
        marginVertical: 10
    },
    subHeading: {
        fontSize: 12,
        color: "#00063B",
    }

});

export default UserOptions;