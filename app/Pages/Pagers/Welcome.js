
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import BottomBar from '../components/BottomBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';
import UserOptions from './UserOptions';

class Welcome extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showOptions: false,
        }
        this.closeOptions = this.closeOptions.bind(this);
    }

    closeOptions() {
        this.setState({
            showOptions: false,
        });
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={styles.container}>
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <BlueLogo />
                        <View style={[ThemeStyle.text_block]}>
                            <Text style={[ThemeStyle.processText]}>Hi Shailesh Bakshi, Welcome back What do you want to explore today?</Text>
                        </View>
                        <TouchableOpacity
                            onPress={() => this.setState({ showOptions: true })}
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'center'
                            }}>
                            <Image source={require('../../assets/images/colorbar.png')} />
                        </TouchableOpacity>
                    </View>
                </View>

                {
                    this.state.showOptions ?
                        <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, backgroundColor: 'white', zIndex: 1 }}>
                            <UserOptions {...this.props} closeOptions={this.closeOptions} />
                        </View>
                        :
                        <View />
                }

                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />

            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#20B77E",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    progressbar: {
        marginVertical: 20
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    container: {
        flex: 1, marginHorizontal: 25
    }

});

export default Welcome;
