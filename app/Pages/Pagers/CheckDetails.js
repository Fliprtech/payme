
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import BottomBar from '../components/BottomBar';
import PagerTopBar from '../components/PagerTopBar';
import BlueLogo from '../components/BlueLogo';
import ThemeStyle from '../../assets/css/ThemeStyle';

class CheckDetails extends Component {

    constructor(props) {
        super(props)
        this.state = {
            text: "",
            bankDetail: null,
        }
    }

    componentDidMount() {
        this.state.bankDetail = this.props.navigation.state.params.bankdetail;
        console.log(this.state.bankDetail);
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={styles.container}>

                    <PagerTopBar percentage={94} />
                    <View
                        style={{
                            justifyContent: "flex-end",
                            flex: 1,
                            flexDirection: "column",
                        }}>
                        <BlueLogo />
                        <View style={[ThemeStyle.text_block]}>
                            <Text style={[ThemeStyle.processText]}>Would you like to re check the details you entered?</Text>
                        </View>

                        <View style={{
                            flexDirection: 'column',
                        }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    flexWrap: "wrap"

                                }}>
                                <TouchableOpacity
                                    style={{ flex: 1, margin: 5, paddingHorizontal: 5, paddingVertical: 15, backgroundColor: '#2566FF', borderColor: "#2566FF", borderRadius: 10, borderWidth: 1, }}
                                    onPress={() => this.props.navigation.navigate("EditAndConfirm", { bankdetail: this.state.bankDetail })}>
                                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                                        <Text style={[ThemeStyle.activebtntext]}>Yes, let me check</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={{ flex: 1, margin: 5, paddingHorizontal: 5, paddingVertical: 15, borderColor: "#2566FF", borderRadius: 10, borderWidth: 1, }}
                                    onPress={() => this.props.navigation.navigate("EditAndConfirm", { bankdetail: this.state.bankDetail })}>
                                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                                        <Text style={[ThemeStyle.borderbtntext]}>No, go ahead</Text>
                                    </View>
                                </TouchableOpacity>

                            </View>

                        </View>
                    </View>
                </View>
                <View style={{ height: 80 }} />
                <BottomBar {...this.props} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#20B77E",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    },
    progressbar: {
        marginVertical: 20
    },
    bardot: {
        backgroundColor: "white",
        padding: 3,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: "#DDF0FF"
    },
    container: {
        flex: 1, marginHorizontal: 25
    }
});

export default CheckDetails;
