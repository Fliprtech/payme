
import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Image, Text, TextInput, StyleSheet, ImageBackground, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import BottomBar from '../components/BottomBar';
import ProfileTab from './profileTabs/ProfileTab';
import KycDetail from './profileTabs/KycDetail';
import ThemeStyle from '../../assets/css/ThemeStyle';
import ProfileTabTopBar from '../components/ProfileTabTopBar';

class Profile extends Component {

    state = {
        text: '',
    };

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <ScrollView>
                    <View style={{}}>
                        <ImageBackground source={require('../../assets/images/ProfilePath/ProfilePath.png')} style={{ width: '100%', height: 300 }}>
                            <View style={{
                                flexDirection: "column",
                                justifyContent: "space-around",
                            }}>
                                <Image source={require('../../assets/images/darkimage/darkimage.png')} style={{
                                    width: "100%",
                                    height: 330,
                                    borderBottomLeftRadius: 18
                                }}></Image>
                                <View style={{
                                    padding: 20
                                }}>
                                    <Icon
                                        reverse
                                        name='chevron-left'
                                        type='font-awesome'
                                        size={25}
                                        onPress={() => this.props.navigation.goBack()}
                                        color='#ffffff' />
                                </View>
                            </View>
                        </ImageBackground>

                        <View
                            style={{
                                alignItems: 'center',
                                padding: 20,
                                backgroundColor: "#E7F5FF",
                            }}>
                            <Text
                                style={[
                                    ThemeStyle.headingFontMontserratBold,
                                    {
                                        fontSize: 20,
                                        marginBottom: 10
                                    }
                                ]}>
                                Shailesh Bakshi
                        </Text>
                            <Text
                                style={[
                                    ThemeStyle.bodyFontRegular,
                                    {
                                        color: "#00063B",
                                        fontSize: 14,
                                    }
                                ]}>
                                click to explore more
                            </Text>
                        </View>
                    </View>

                    <View style={{
                        marginVertical: 20,
                        marginHorizontal: 20
                    }}>
                        <View >
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate("SetMpin")}
                                style={{ marginTop: 20 }}>
                                <View style={{ paddingHorizontal: 20, paddingRight: 30, paddingVertical: 20, backgroundColor: '#2566FF', borderRadius: 10, }}>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            alignItems: 'center'
                                        }}>
                                        <View
                                            style={{
                                                flexDirection: "row",
                                            }}>
                                            <Image source={require('../../assets/images/setmpin/setmpin.png')}
                                                style={{
                                                    marginRight: 10
                                                }}></Image>
                                            <Text style={[
                                                ThemeStyle.bodyFontBold,
                                                { color: '#ffffff', }
                                            ]}>Set MPIN </Text>
                                        </View>

                                        <Image
                                            source={require('../../assets/images/arrow_simple_1/arrow_simple_1.png')}
                                            style={{
                                                alignItems: 'center'
                                            }} />

                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <ProfileTab />
                        <View >
                            <TouchableOpacity style={{ marginTop: 20 }}>
                                <View style={{ backgroundColor: '#FFEFE5', borderRadius: 10, paddingBottom: 0 }}>
                                    <ProfileTabTopBar {...this.props} barColor={'#F75F00'} progress={50} />
                                    <View style={[ThemeStyle.profileTabMainRow]}>
                                        <View style={[ThemeStyle.profileTabTextRow]}>
                                            <Image
                                                source={require('../../assets/images/home_3_2/home_3_2.png')}
                                                style={[ThemeStyle.profileTabIcon]} />
                                            <Text style={[ThemeStyle.profileHeadings,]}>Residential details </Text>
                                        </View>
                                        <Icon
                                            reverse
                                            name='plus'
                                            type='font-awesome'
                                            size={15}
                                            color='#000000' />
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View >
                            <TouchableOpacity style={{ marginTop: 20 }}>
                                <View style={{ backgroundColor: '#FFE5EE', borderRadius: 10, paddingBottom: 0 }}>
                                    <ProfileTabTopBar {...this.props} barColor={'#FF085A'} progress={80} />
                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        paddingHorizontal: 20
                                    }}>
                                        <View style={{
                                            flexDirection: "row",
                                            paddingVertical: 20
                                        }}>
                                            <Image
                                                source={require('../../assets/images/layers_3/layers_3.png')}
                                                style={[ThemeStyle.profileTabIcon]} />
                                            <Text style={[ThemeStyle.profileHeadings,]}>Bank details </Text>
                                        </View>
                                        <Icon
                                            reverse
                                            name='plus'
                                            type='font-awesome'
                                            size={15}
                                            color='#000000' />
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <KycDetail />

                    </View>

                </ScrollView>

            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    submitbutton: {
        backgroundColor: "#20B77E",
        paddingHorizontal: 13,
        padding: 20,
        marginBottom: 10,
        marginTop: 15,
        borderRadius: 9,
        marginHorizontal: 20
    },
    submitbuttontxt: {
        color: "white",
        fontWeight: "700",
        fontSize: 16,
        textAlign: "center"
    }, greendot: {
        padding: 3,
        backgroundColor: "#20b77e",
        paddingHorizontal: 7,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    orangedot: {
        backgroundColor: "#f75f00",
        padding: 3,
        paddingHorizontal: 7

    },
    reddot: {
        backgroundColor: "#ff085a",
        padding: 3,
        paddingHorizontal: 7

    },
    bluedot: {
        backgroundColor: "#7340fb",
        padding: 3,
        paddingHorizontal: 7,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    },
    bottombar: {
        padding: 25,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        flexDirection: "row",
        justifyContent: "center"
    },
    progressbar: {
        marginBottom: 20,
        paddingRight: 10
    },
});

export default Profile;
