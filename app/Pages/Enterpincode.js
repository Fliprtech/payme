import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView, StatusBar, ScrollView } from 'react-native';
import { Input, Icon } from 'react-native-elements';
import ThemeStyle from '../assets/css/ThemeStyle';
import { ThemeColors } from 'react-navigation';

class Enterpincode extends Component {

    constructor(props) {
        super(props);
        this.state = {
            show_submit: false,
            pincode: "",
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <StatusBar backgroundColor="#f1f1f1" barStyle="dark-content" />
                <View style={[styles.container,]}>
                    <View>
                        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingVertical: 40 }}>
                            <Image style={styles.image} source={require('../assets/images/logo-blue/logo-blue3x.png')}></Image>
                            <Text style={[ThemeStyle.headingLarge, ThemeStyle.colorBlue, ThemeStyle.MarginVSmall]}>Enter pincode</Text>
                            <Text style={[ThemeStyle.sub_heading, ThemeStyle.colorDarkBlue]}>give us your pin code so that we can go further</Text>
                            <View style={styles.input_block}>
                                <Input
                                    keyboardType={"number-pad"}
                                    inputContainerStyle={{
                                        borderBottomWidth: 0,
                                    }}
                                    containerStyle={{
                                        padding: 0,
                                        width: '100%',
                                        alignItems: 'stretch',
                                        paddingHorizontal: 0,
                                    }}
                                    inputStyle={[ThemeStyle.inputStyle1, ThemeStyle.colorDarkBlue]}
                                    placeholderTextColor="#00063B36"
                                    placeholder='enter your pincode here'
                                    leftIconContainerStyle={{
                                        marginRight: 15,
                                        marginLeft: 0,
                                    }}
                                    value={this.state.pincode}
                                    onChangeText={pincode => {
                                        var length = pincode.length;
                                        if (length == 6) {
                                            this.setState({ pincode: pincode, show_submit: true });
                                        } else {
                                            this.setState({ pincode: pincode, show_submit: false });
                                        }
                                    }}
                                    leftIcon={
                                        <Image
                                            style={styles.icon_img}
                                            source={require('../assets/images/compas/compas.png')} />
                                    }
                                />
                            </View>
                        </ScrollView>
                    </View>
                </View>

                <View style={{ flex: 0 }}>
                    {
                        this.state.show_submit ?
                            <TouchableOpacity
                                activeOpacity={0.5}
                                onPress={() => {
                                    this.props.navigation.navigate("Chooselanguage");
                                }}
                                style={{
                                    padding: 25,
                                    backgroundColor: '#2566FF',
                                    borderTopLeftRadius: 15,
                                    borderTopRightRadius: 15,
                                    alignItems: 'center'
                                }}>
                                <Text style={{ fontWeight: "700", color: "white", fontSize: 18 }}>proceed</Text>
                            </TouchableOpacity>
                            :
                            <View />
                    }
                </View>


            </SafeAreaView >

        );
    }
}


const styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
    },
    image: {
        width: 80,
        height: "auto",
        aspectRatio: 77 / 84

    },
    container: {
        paddingHorizontal: 50,
        flex: 1,
        justifyContent: "center"
    },
    input_block: {
        marginTop: 50
    },
    icon_img: {
        width: 25,
        height: 25,
    },
    inputComponent: {
        borderWidth: 4
    }
});

export default Enterpincode;