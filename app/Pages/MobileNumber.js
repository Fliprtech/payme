import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, Alert, TouchableOpacity, SafeAreaView, StatusBar, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Input, Icon } from 'react-native-elements';
import GlobalFont from 'react-native-global-font'
import ThemeStyle from '../assets/css/ThemeStyle';


class MobileNumber extends Component {

    constructor(props) {
        super(props);
        this.state = {
            show_submit: false,
            number: "",
        }
    }

    componentDidMount() {
        let fontName = 'Nunito-Regular';
        GlobalFont.applyGlobal(fontName);
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <StatusBar backgroundColor="#2566FF" barStyle="light-content" />
                <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 0.5, y: 1 }}
                    locations={[0.0, 0.35, 0.99]}
                    colors={['#2566FF', '#246BFF', '#1C99FF']}
                    style={styles.linearGradient}>
                    <View style={[styles.container]}>
                        <View>
                            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingVertical: 40 }}>
                                <Image style={styles.image} source={require('../assets/images/logo-white/logo-white3x.png')}></Image>
                                <Text style={[ThemeStyle.headingLarge, ThemeStyle.colorLight, ThemeStyle.MarginVSmall]}>Howdy mate</Text>
                                <Text style={[ThemeStyle.sub_heading, ThemeStyle.colorLight]}>give us your mobile number that is linked with bank account.</Text>
                                <View style={styles.input_block}>
                                    <Input
                                        keyboardType={"number-pad"}
                                        inputContainerStyle={{
                                            borderBottomWidth: 0,
                                        }}
                                        containerStyle={{
                                            padding: 0,
                                            width: '100%',
                                            alignItems: 'stretch',
                                            paddingHorizontal: 0,
                                        }}
                                        inputStyle={[ThemeStyle.inputStyle1, ThemeStyle.colorLight]}
                                        placeholderTextColor="#ffffff5c"
                                        placeholder='enter your number here'
                                        leftIconContainerStyle={{
                                            marginRight: 15,
                                            marginLeft: 0,
                                        }}
                                        value={this.state.number}
                                        onChangeText={number => {
                                            var length = number.length;
                                            if (length == 10) {
                                                this.setState({ number: number, show_submit: true });
                                            } else {
                                                this.setState({ number: number, show_submit: false });
                                            }
                                        }}
                                        leftIcon={
                                            <Image
                                                style={styles.icon_img}
                                                source={require('../assets/images/phone/phone3x.png')} />
                                        }
                                    />
                                </View>
                            </ScrollView>
                        </View>
                    </View>

                    <View style={{ flex: 0 }}>
                        {
                            this.state.show_submit ?
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    onPress={() => {
                                        this.props.navigation.navigate('EnterPincode');
                                        // Alert.alert("Data", "You Pressed Button");
                                    }}
                                    style={{
                                        padding: 25,
                                        backgroundColor: 'white',
                                        borderTopLeftRadius: 15,
                                        borderTopRightRadius: 15,
                                        alignItems: 'center'
                                    }}>
                                    <Text style={{ color: "#2566FF", fontWeight: 'bold', fontSize: 16 }}>proceed</Text>
                                </TouchableOpacity>
                                :
                                <View />
                        }
                    </View>
                </LinearGradient>
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
    },
    image: {
        width: 80,
        height: "auto",
        aspectRatio: 77 / 84

    },
    container: {
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 30,
    },
    input_block: {
        marginTop: 50
    },
    icon_img: {
        width: 25,
        height: 25,
    },
    inputComponent: {
        borderWidth: 4
    }
});

export default MobileNumber;