import React, { Component } from 'react';
import { SafeAreaView, View, Text, ImageBackground, Image, TouchableOpacity, StatusBar } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import ThemeStyle from '../../assets/css/ThemeStyle';

class Intro2 extends Component {

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <StatusBar backgroundColor="#2566FF" barStyle="light-content" />
                <View style={{ flex: 1, }}>
                    <View style={[ThemeStyle.intoUpperBlock]}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <Image
                                style={[ThemeStyle.introMainImage]}
                                source={require('../../assets/images/intro-screens/intro_2.png')} />
                            <View style={[ThemeStyle.introUpperTextBlock]}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("GettingStarted");
                                    }}
                                    style={{
                                        paddingHorizontal: 20,
                                        paddingVertical: 0,
                                        alignSelf: "flex-end",
                                    }}>
                                    <Text
                                        style={{
                                            alignSelf: "flex-end",
                                            color: "#ffffff"
                                        }}>
                                        Skip
                                    </Text>
                                </TouchableOpacity>
                                <Text style={[ThemeStyle.introHeading]}>Get swift approval.</Text>
                                <Text style={[ThemeStyle.introSubHeading]}>
                                    You will never be in queue again.
                                </Text>
                            </View>
                        </ScrollView>
                    </View>
                    <View style={[ThemeStyle.introBottomRow]}>
                        <View>
                            <View
                                style={{
                                    flexDirection: "row",
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                }}>
                                <View style={{}}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ width: 13, marginRight: 5, height: 15, backgroundColor: '#2566FF', borderRadius: 3 }}></View>
                                        <View style={{ width: 45, marginRight: 5, height: 15, backgroundColor: '#2566FF', borderRadius: 3 }}></View>
                                        <View style={{ width: 13, marginRight: 5, height: 15, backgroundColor: '#D3E0FF', borderRadius: 3 }}></View>
                                    </View>
                                </View>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("Intro3");
                                    }}>
                                    <View>
                                        <View style={[ThemeStyle.introNextIcon]}>
                                            <Image source={require('../../assets/images/arrow_simple_1/arrow_simple_1.png')} />
                                        </View>
                                    </View>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </View>
                </View>
            </SafeAreaView>

        );
    }

}

export default Intro2;