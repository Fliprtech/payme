import React, { Component } from 'react';
import { SafeAreaView, TouchableOpacity, View, Text, Image, ImageBackground, ScrollView } from 'react-native';

class WhatDoiNeed extends Component {

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>

                <View
                    style={{
                        padding: 35,
                        backgroundColor: '#E7F5FF',
                        borderBottomLeftRadius: 15,
                        alignItems: 'center'
                    }}>
                    <Text
                        style={{
                            color: '#040B4D',
                            fontSize: 18,
                            marginBottom: 8,
                            alignSelf: "flex-start"
                        }}>
                        Keep these handy
                                        </Text>
                    <Text style={{
                        color: '#2566FF',
                        fontSize: 20,
                        alignSelf: "flex-start",
                        fontWeight: "bold",
                    }}>
                        That you need to go further
                                        </Text>
                </View>

                <ScrollView>

                    <View
                        style={{
                            marginLeft: 20,
                            marginRight: 20,
                            marginTop: 40,
                            marginBottom: 20,
                            backgroundColor: '#E8F8F2',
                            padding: 30,
                            borderRadius: 15,
                        }}>
                        <Text style={{
                            color: '#20B77E',
                            fontSize: 20,
                            alignSelf: "flex-start",
                            fontWeight: "bold",
                            marginBottom: 8,
                        }}>
                            Aadhar Card
                                    </Text>
                        <Text style={{
                            color: '#00063B',
                            fontSize: 18,
                            alignSelf: "flex-start",
                        }}>
                            This will let us know your address proof, bate of birth and other details.
                                    </Text>
                    </View>

                    <View style={{
                        marginLeft: 20,
                        marginRight: 20,
                        marginBottom: 20,
                        backgroundColor: '#FFEFE5',
                        padding: 30,
                        borderRadius: 15,
                    }}>
                        <Text style={{
                            color: '#F75F00',
                            fontSize: 20,
                            alignSelf: "flex-start",
                            fontWeight: "bold",
                            marginBottom: 8,
                        }}>
                            PAN Card
                                    </Text>
                        <Text style={{
                            color: '#00063B',
                            fontSize: 18,
                            alignSelf: "flex-start",
                        }}>
                            This will let us know your address proof, bate of birth and other details.
                                    </Text>
                    </View>

                    <View style={{
                        marginLeft: 20,
                        marginRight: 20,
                        marginBottom: 20,
                        backgroundColor: '#FFE5EE',
                        padding: 30,
                        borderRadius: 15,
                    }}>
                        <Text style={{
                            color: '#FF085A',
                            fontSize: 20,
                            alignSelf: "flex-start",
                            fontWeight: "bold",
                            marginBottom: 8,
                        }}>
                            Bank statement
                                    </Text>
                        <Text style={{
                            color: '#00063B',
                            fontSize: 18,
                            alignSelf: "flex-start",
                        }}>
                            This will let us know your address proof, bate of birth and other details.
                                    </Text>
                    </View>

                    <View style={{
                        marginLeft: 20,
                        marginRight: 20,
                        marginBottom: 20,
                        backgroundColor: '#F1EBFF',
                        padding: 30,
                        borderRadius: 15,
                    }}>
                        <Text style={{
                            color: '#FF085A',
                            fontSize: 20,
                            alignSelf: "flex-start",
                            fontWeight: "bold",
                            marginBottom: 8,
                        }}>
                            Salary Slip
                                    </Text>
                        <Text style={{
                            color: '#00063B',
                            fontSize: 18,
                            alignSelf: "flex-start",
                        }}>
                            This will let us know your address proof, bate of birth and other details.
                                    </Text>
                    </View>

                    <View
                        style={{
                            marginLeft: 20,
                            marginRight: 20,
                            marginBottom: 20,
                            backgroundColor: '#FFE5EE',
                            padding: 30,
                            borderRadius: 15,
                        }}>
                        <Text
                            style={{
                                color: '#7340FB',
                                fontSize: 20,
                                alignSelf: "flex-start",
                                fontWeight: "bold",
                                marginBottom: 8,
                            }}>
                            Salary Slip
                        </Text>
                        <Text
                            style={{
                                color: '#00063B',
                                fontSize: 18,
                                alignSelf: "flex-start",
                            }}>
                            This will let us know your address proof, bate of birth and other details.
                        </Text>
                    </View>

                </ScrollView>

                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate("GettingStarted");
                    }}
                    style={{
                        padding: 25,
                        marginTop: 10,
                        backgroundColor: '#2566FF',
                        borderTopLeftRadius: 15,
                        borderTopRightRadius: 15,
                        alignItems: 'center'
                    }}>
                    <Text style={{ fontWeight: "700", color: "#ffffff", fontSize: 18 }}>Let's begin</Text>
                </TouchableOpacity>
            </SafeAreaView >
        );
    }
}

export default WhatDoiNeed;
