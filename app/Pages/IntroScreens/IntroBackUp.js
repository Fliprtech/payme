import React, { Component } from 'react';
import { SafeAreaView, View, Text, ImageBackground, Image, TouchableOpacity, StatusBar } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

class Intro3 extends Component {

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#2566FF' }}>
                <StatusBar backgroundColor="#2566FF" barStyle="light-content" />
                <View
                    style={{
                        justifyContent: "flex-end",
                        flex: 1,
                        flexDirection: "column",
                    }}>
                    <View>
                        <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false} style={{}} contentContainerStyle={{}} >


                            <View style={{ flex: 1 }}>
                                <ImageBackground
                                    source={require('../../assets/images/intro-screens/intro_3.png')}
                                    resizeMode={'cover'}
                                    style={{ width: '100%', height: 'auto', aspectRatio: 376 / 537 }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.props.navigation.navigate("GettingStarted");
                                        }}
                                        style={{
                                            paddingHorizontal: 20,
                                            paddingVertical: 20,
                                            alignSelf: "flex-end",
                                        }}>
                                        <Text
                                            style={{
                                                alignSelf: "flex-end",
                                                color: "#ffffff"
                                            }}>
                                            Skip
                        </Text>
                                    </TouchableOpacity>
                                    <Text
                                        style={{
                                            marginTop: 30,
                                            marginHorizontal: 20,
                                            marginBottom: 20,
                                            color: "#ffffff",
                                            fontSize: 24,
                                            fontWeight: 'bold'
                                        }}>
                                        Get accurate assessment.
                        </Text>
                                    <Text
                                        style={{
                                            marginHorizontal: 20,
                                            color: "#ffffff",
                                            fontSize: 18,
                                            fontWeight: "300"
                                        }}>
                                        You are valuable. Know your true worth..
                        </Text>
                                </ImageBackground>
                            </View>


                            <View style={{ flex: 0, justifyContent: 'center', padding: 20, backgroundColor: 'white' }}>
                                <View>
                                    <View style={{
                                        flexDirection: "row",
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                    }}>

                                        <View style={{}}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ width: 13, marginRight: 5, height: 15, backgroundColor: '#2566FF', borderRadius: 3 }}></View>
                                                <View style={{ width: 13, marginRight: 5, height: 15, backgroundColor: '#2566FF', borderRadius: 3 }}></View>
                                                <View style={{ width: 45, marginRight: 5, height: 15, backgroundColor: '#2566FF', borderRadius: 3 }}></View>
                                            </View>
                                        </View>

                                        <TouchableOpacity
                                            onPress={() => {
                                                this.props.navigation.navigate("Intro4");
                                            }}>
                                            <View>
                                                <View style={{ width: 45, height: 45, backgroundColor: '#2566FF', borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                                                    <Image source={require('../../assets/images/arrow_simple_1/arrow_simple_1.png')}></Image>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </View>



            </SafeAreaView>

        );
    }

}

export default Intro3;