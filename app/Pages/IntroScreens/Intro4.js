import React, { Component } from 'react';
import { SafeAreaView, StyleSheet, TouchableOpacity, View, Text, StatusBar, Image, ImageBackground } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import ThemeStyle from '../../assets/css/ThemeStyle';

class Intro4 extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#2566FF' }}>

                <StatusBar backgroundColor="#2566FF" barStyle="light-content" />
                <View
                    style={{
                        justifyContent: "flex-end",
                        flex: 1,
                        flexDirection: "column",
                    }}>
                    <View>
                        <ScrollView keyboardShouldPersistTaps={'always'} showsVerticalScrollIndicator={false} style={{}} contentContainerStyle={{}} >


                            <View style={{ flex: 1 }}>
                                <ImageBackground
                                    source={require('../../assets/images/intro-screens/intro_4.png')}
                                    resizeMode={'cover'}
                                    style={{ width: '100%', height: 'auto', aspectRatio: 376 / 537 }}>
                                    <Text style={[ThemeStyle.introHeading]}>Instant cash disbursed.</Text>
                                    <Text style={[ThemeStyle.introSubHeading]}>
                                        Walk a mile in few steps with unique mobile app based login process.
                                    </Text>
                                </ImageBackground>
                            </View>
                            <View style={{ flex: 0, justifyContent: 'center', backgroundColor: 'white' }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("WhatDoiNeed");
                                    }}
                                    style={{
                                        padding: 25,
                                        backgroundColor: '#E7F5FF',
                                        borderTopLeftRadius: 15,
                                        borderTopRightRadius: 15,
                                    }}>
                                    <Text style={{ fontWeight: "700", color: "#2664FF", fontSize: 18, textAlign: 'center' }}>
                                        What do I need?
                        </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("GettingStarted");
                                    }}
                                    style={{
                                        padding: 25,
                                        backgroundColor: '#2566FF',
                                        borderTopLeftRadius: 15,
                                        borderTopRightRadius: 15,
                                    }}>
                                    <Text style={{ fontWeight: "700", color: "#ffffff", fontSize: 18, textAlign: 'center' }}>
                                        Let's begin
                        </Text>
                                </TouchableOpacity>
                            </View>

                        </ScrollView>
                    </View>
                </View>
            </SafeAreaView >

        );
    }
}

export default Intro4;