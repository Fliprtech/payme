import { StyleSheet } from "react-native"

export default StyleSheet.create({

    container: {
        flex: 1, marginHorizontal: 25
    },
    overFlowContainer: {
        padding: 30,
        backgroundColor: "#ffffff",
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    },
    overFlowBox: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#00000070",
    },
    progressContainer: {
        flex: 1,
        marginHorizontal: 25
    },
    processText: {
        color: "#00063B",
        fontSize: 18,
        // fontWeight: "bold",
        letterSpacing: 0.5,
        fontFamily: 'Montserrat-Bold',
        lineHeight: 24,
        letterSpacing: 0,
    },
    text_block: {
        maxWidth: "80%",
        backgroundColor: "#EEF8FF",
        paddingLeft: 20,
        paddingRight: 25,
        paddingVertical: 22,
        borderTopEndRadius: 10,
        borderBottomEndRadius: 10,
        borderBottomLeftRadius: 10,
        marginBottom: 30,
    },
    inerInputStyle: {
        color: '#00063B',
        fontSize: 14,
        fontFamily: 'Nunito-Bold',
        paddingVertical: 10,
        paddingLeft: 20
    },
    inerInputBlock: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#F1F2F5',
        borderRadius: 10,
        padding: 5,
    },
    imageblock: {
        marginVertical: 30
    },
    MarginVSmall: {
        marginVertical: 20,
    },
    headingLarge: {
        fontFamily: 'Vollkorn-Bold',
        fontSize: 33,
    },
    colorLight: {
        color: '#ffffff',
    },
    colorBlue: {
        color: '#2566FF',
    },
    colorDarkBlue: {
        color: "#00063B",
    },
    sub_heading: {
        fontSize: 18,
        fontWeight: "300",
        lineHeight: 30,
        fontFamily: 'Nunito-Regular',
    },
    blueBtn: {
        paddingVertical: 15,
        backgroundColor: '#2566FF',
        borderRadius: 10,
        paddingHorizontal: 10,
        borderColor: "#2566FF",
        borderWidth: 1,
    },
    whiteBtn: {
        paddingVertical: 15,
        backgroundColor: 'white',
        borderRadius: 10,
        borderColor: "#2566FF",
        borderWidth: 1,
        paddingHorizontal: 10,
    },
    activebtntext: {
        color: "white",
        textAlign: "center",
        fontFamily: 'Nunito-SemiBold',
    },
    borderbtntext: {
        color: "#2664FF",
        textAlign: "center",
        fontFamily: 'Nunito-SemiBold',
    },
    overlayHeading: {
        color: "#040B4D",
        fontSize: 18,
        fontWeight: 'bold'
    },
    overlaySubHeading: {
        color: "#00063B",
        fontSize: 13,
        marginVertical: 6,
        marginBottom: 15,
    },
    aggrementTextHeading: {
        color: "#00063B",
        fontSize: 16,
        alignSelf: 'flex-start',
        fontFamily: 'Vollkorn-Bold',
        marginBottom: 15
    },
    aggrementText: {
        color: "#00063B",
        fontSize: 13,
        alignSelf: 'flex-start',
        marginBottom: 15,
        fontFamily: 'Nunito-Regular',
    },
    inputLavelText: {
        color: '#00063B',
        fontSize: 14,
        alignSelf: "flex-start",
        marginBottom: 10,
        fontFamily: 'Montserrat-Bold',
    },
    inputStyle1: {
        fontSize: 20,
        fontFamily: 'Nunito-Bold',
    },
    profileHeadings: {
        fontFamily: 'Nunito-Bold',
        color: '#00063B',
        fontSize: 14,
    },
    profileTabIcon: {
        width: 17,
        height: 18,
        marginRight: 15,
        resizeMode: 'contain'
    },
    profileTabTextRow: {
        flexDirection: "row",
        paddingVertical: 20,
        alignItems: 'center'
    },
    profileTabMainRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    profileHeadingTopText: {
        fontSize: 14
    },
    profileHeading: {
        fontSize: 22,
        lineHeight: 40,
    },
    headingFont: {
        fontFamily: 'Vollkorn-Bold',
    },
    headingFontMontserratBold: {
        fontFamily: 'Montserrat-Bold',
    },
    headingFontMontserratMedium: {
        fontFamily: 'Montserrat-Medium',
    },
    bodyFontRegular: {
        fontFamily: 'Nunito-Regular',
    },
    bodyFontBlack: {
        fontFamily: 'Nunito-Black',
    },
    bodyFontExtraBlack: {
        fontFamily: 'Nunito-ExtraBold',
    },
    bodyFontBold: {
        fontFamily: 'Nunito-Bold',
    },
    bodyFontSemiBold: {
        fontFamily: 'Nunito-SemiBold',
    },

    // Intro Screen
    introMainImage: {
        width: 'auto',
        height: 'auto',
        aspectRatio: 376 / 537,
        resizeMode: 'contain'
    },
    introBottomRow: {
        justifyContent: 'center',
        paddingVertical: 5,
        paddingHorizontal: 20,
        backgroundColor: 'white',
        flexGrow: 1,
    },
    intoUpperBlock: {
        position: 'relative',
    },
    introUpperTextBlock: {
        position: 'absolute',
        paddingVertical: 20,
        left: 0,
        right: 0,
    },
    introHeading: {
        marginTop: 30,
        marginHorizontal: 20,
        marginBottom: 20,
        color: "#ffffff",
        fontSize: 22,
        fontFamily: 'Montserrat-Bold',
    },
    introSubHeading: {
        marginHorizontal: 20,
        color: "#ffffff",
        fontSize: 17,
        fontFamily: 'Nunito-Regular',
    },
    introNextIcon: {
        width: 55,
        height: 55,
        backgroundColor: '#2566FF',
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
    },
    uploadstatecontainer: {
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',

    },
    uploadstatesingleblock: {
        backgroundColor: '#E8F8F2',
        flex: 1,
        padding: 10,
        marginHorizontal: 4,
        borderRadius: 8,
        paddingVertical: 18,
        paddingTop: 25

    },
    uploadStateImage: {
        width: 20,
        height: 18
    },
    uploadStateText: {
        fontSize: 15,
        marginTop: 8,

    }


})