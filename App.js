import React from 'react';
import {
  SafeAreaView, StyleSheet,
  ScrollView, View,
  Text, StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import MobileNumber from './app/Pages/MobileNumber';
import Home from './app/Pages/Home';
import Enterpincode from './app/Pages/Enterpincode';
import Chooselanguage from './app/Pages/Chooselanguage';
import Intro1 from './app/Pages/IntroScreens/Intro1';
import Intro2 from './app/Pages/IntroScreens/Intro2';
import Intro3 from './app/Pages/IntroScreens/Intro3';
import Intro4 from './app/Pages/IntroScreens/Intro4';
import WhatDoiNeed from './app/Pages/IntroScreens/WhatDoiNeed';
import GettingStarted from './app/Pages/GettingStarted';
import Allowaccess from './app/Pages/Allowaccess';
import SelectAccount from './app/Pages/Pagers/SelectAccount';
import ReferalCode from './app/Pages/Pagers/ReferalCode';
import DoYouHaveReferalCode from './app/Pages/Pagers/DoYouHaveReferalCode';
import EnterReferalCode from './app/Pages/Pagers/EnterReferalCode';
import EnterPinCode from './app/Pages/Pagers/EnterPinCode';
import SalariedOrSelfEmployed from './app/Pages/Pagers/SalariedOrSelfEmployed';
import Salary from './app/Pages/Pagers/Salary';
import CompanyName from './app/Pages/Pagers/CompanyName';
import CompanyType from './app/Pages/Pagers/CompanyType';
import CompanyAddress from './app/Pages/Pagers/CompanyAddress';
import OptionForBank from './app/Pages/Pagers/OptionForBank';
import SelectBank from './app/Pages/Pagers/SelectBank';
import LoginToNetBanking from './app/Pages/Pagers/LoginToNetBanking';
import EnterPassword from './app/Pages/Pagers/EnterPassword';
import ProvideDocument from './app/Pages/Pagers/ProvideDocument';
import OptionForMandateAndSign from './app/Pages/Pagers/OptionForMandateAndSign';
import PhysicalNach from './app/Pages/Pagers/PhysicalNach';
import EMandate from './app/Pages/Pagers/EMandate';
import Esign from './app/Pages/Pagers/ESign';
import LoanAgreement1 from './app/Pages/Pagers/LoanAgreement1';
import PreApprovedLimit from './app/Pages/Pagers/PreApprovedLimit';
import SelectAmount from './app/Pages/Pagers/SelectAmount';
import PurposeOfLoan from './app/Pages/Pagers/PurposeOfLoan';
import EnterOtp from './app/Pages/Pagers/EnterOtp';
import AccountForDisbursal from './app/Pages/Pagers/AccountForDisbursal';
import EnterAccountNumber from './app/Pages/Pagers/EnterAccountNumber';
import CheckDetails from './app/Pages/Pagers/CheckDetails';
import EditAndConfirm from './app/Pages/Pagers/EditAndConfirm';
import LoanDirbursed from './app/Pages/Pagers/LoanDirbursed';
import ShareFeedback from './app/Pages/Pagers/ShareFeedback';
import RepaymentAccount from './app/Pages/Pagers/RepaymentAccount';
import OpctionsSidemenu from './app/Pages/Pagers/OptionsSidemenu';
import Profile from './app/Pages/Pagers/Profile';
import SetMpin from './app/Pages/Pagers/SetMpin';
import ContactUs from './app/Pages/Pagers/ContactUs';
import Repayment from './app/Pages/Pagers/Repayment';
import BuyGold from './app/Pages/Pagers/BuyGold';
import SellMeGold from './app/Pages/Pagers/SellMeGold';
import GiftGold from './app/Pages/Pagers/GiftGold';
import GetDelivery from './app/Pages/Pagers/GetDelivery';
import GoldHowitworks from './app/Pages/Pagers/GoldHowItWorks';
import Jewellers from './app/Pages/Pagers/Jewellers';
import EnterMPIN from './app/Pages/Pagers/EnterMPIN';
import Welcome from './app/Pages/Pagers/Welcome';
import UserOptions from './app/Pages/Pagers/UserOptions';
import EMICalculator from './app/Pages/Pagers/EMICalculator';
import PaymeScore from './app/Pages/Pagers/PaymeScore';
import LoanHistory from './app/Pages/Pagers/LoanHistory';
import CurrentLoan from './app/Pages/Pagers/CurrentLoan';
import UploadBankState from './app/Pages/Pagers/UploadBankState';
import EnterDocPassword from './app/Pages/Pagers/EnterDocPassword';
import ProvideDocEmpty from './app/Pages/Pagers/ProvideDocEmpty';
import CaptureTip from './app/Pages/Pagers/CaptureTip';
import SelectEMILoanOption from './app/Pages/Pagers/SelectEMILoanOption';
import ShortTermLoan from './app/Pages/Pagers/ShortTermLoan';
import NameOnBank from './app/Pages/Pagers/NameOnBank';
import EnterIFSCcode from './app/Pages/Pagers/EnterIFSCcode';
import NameOfYourBank from './app/Pages/Pagers/NameOfYourBank';
import AmountPaid from './app/Pages/Pagers/AmountPaid';
import Reference from './app/Pages/Pagers/Reference';
import EnterDate from './app/Pages/Pagers/EnterDate';
import ReferAndEarn from './app/Pages/Pagers/ReferAndEarn';
import ShareRules from './app/Pages/Pagers/ShareRules';

const AppStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        headerTitle: 'Home',
        header: null,
      },
    },
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#ffffff',
        elevation: 0,
        shadowOpacity: 0,
      },
      headerTintColor: '#000000',
      headerTitleStyle: {
        textAlign: 'center',
        fontWeight: 'bold',
        alignSelf: 'center',
      },
      // headerTransparent: true
    },
  },
);

const AuthStack = createStackNavigator(
  {
    MobileNumber: MobileNumber,
    EnterPincode: Enterpincode,
    Chooselanguage: Chooselanguage,
  },
  {
    initialRouteName: 'MobileNumber',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

const IntroStack = createStackNavigator(
  {
    Intro1: Intro1,
    Intro2: Intro2,
    Intro3: Intro3,
    Intro4: Intro4,
  },
  {
    initialRouteName: 'Intro1',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

const InfoStack = createStackNavigator(
  {
    WhatDoiNeed: WhatDoiNeed,
    GettingStarted: GettingStarted,
  },
  {
    initialRouteName: 'WhatDoiNeed',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

const AppStartStack = createStackNavigator(
  {
    Allowaccess: Allowaccess,
    SelectAccount: SelectAccount,
    ReferalCode: ReferalCode,
    DoYouHaveReferalCode: DoYouHaveReferalCode,
    EnterReferalCode: EnterReferalCode,
    EnterPinCode: EnterPinCode,
    SalariedOrSelfEmployed: SalariedOrSelfEmployed,
    Salary: Salary,
    CompanyName: CompanyName,
    CompanyType: CompanyType,
    CompanyAddress: CompanyAddress,
    OptionForBank: OptionForBank,
    SelectBank: SelectBank,
    LoginToNetBanking: LoginToNetBanking,
    EnterPassword: EnterPassword,
    ProvideDocument: ProvideDocument,
    OptionForMandateAndSign: OptionForMandateAndSign,
    PhysicalNach: PhysicalNach,
    EMandate: EMandate,
    ESign: Esign,
    LoanAgreement1: LoanAgreement1,
    PreApprovedLimit: PreApprovedLimit,
    SelectAmount: SelectAmount,
    PurposeOfLoan: PurposeOfLoan,
    EnterOtp: EnterOtp,
    AccountForDisbursal: AccountForDisbursal,
    EnterAccountNumber: EnterAccountNumber,
    CheckDetails: CheckDetails,
    EditAndConfirm: EditAndConfirm,
    LoanDirbursed: LoanDirbursed,
    ShareFeedback: ShareFeedback,
    RepaymentAccount: RepaymentAccount,
    OpctionsSidemenu: OpctionsSidemenu,
    Profile: Profile,
    SetMpin: SetMpin,
    ContactUs: ContactUs,
    Repayment: Repayment,
    BuyGold: BuyGold,
    SellMeGold: SellMeGold,
    GiftGold: GiftGold,
    GetDelivery: GetDelivery,
    GoldHowitworks: GoldHowitworks,
    Jewellers: Jewellers,
    EnterMPIN: EnterMPIN,
    Welcome: Welcome,
    UserOptions: UserOptions,
    EMICalculator: EMICalculator,
    PaymeScore: PaymeScore,
    LoanHistory: LoanHistory,
    CurrentLoan: CurrentLoan,
    /*********************************/
    UploadBankState: UploadBankState,
    EnterDocPassword: EnterDocPassword,
    ProvideDocEmpty: ProvideDocEmpty,
    SelectEMILoanOption: SelectEMILoanOption,
    ShortTermLoan: ShortTermLoan,
    NameOnBank: NameOnBank,
    EnterIFSCcode: EnterIFSCcode,
    NameOfYourBank: NameOfYourBank,
    AmountPaid: AmountPaid,
    Reference: Reference,
    EnterDate: EnterDate,
    ReferAndEarn: ReferAndEarn,
    ShareRules: ShareRules,
  },
  {
    initialRouteName: 'Allowaccess',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

const AppContainer = createAppContainer(
  createSwitchNavigator(
    {
      App: AppStack,
      Auth: AuthStack,
      Intro: IntroStack,
      Info: InfoStack,
      AppStart: AppStartStack,
    },
    {
      initialRouteName: 'Auth',
    },
  ),
);

const App: () => React$Node = () => {
  return (
    <AppContainer />
    // <CaptureTip />
  );
};

export default App;

// https://xd.adobe.com/view/b83a16fb-a5e9-41b8-561d-8ea78d272625-ce0b/screen/8ee23982-0f37-4c93-b6a5-7e822f010131/intro-1
